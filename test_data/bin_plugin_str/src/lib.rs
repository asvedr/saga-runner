use std::ffi::{c_char, c_uint, CStr, CString};

#[no_mangle]
pub extern "C" fn name() -> *const u8 {
    "strops\0".as_ptr()
}

#[no_mangle]
pub extern "C" fn version() -> *const u8 {
    "0.1.0\0".as_ptr()
}

#[no_mangle]
pub extern "C" fn schema() -> *const u8 {
    concat!(
    // "raise_err() -> s\n",
    "add(s, s) -> s\0",
    ).as_ptr()
}

static mut RESULT: Option<CString> = None;

#[no_mangle]
pub extern "C" fn init(_: c_uint) {}

#[no_mangle]
pub extern "C" fn finalize(_: c_uint) {}

#[no_mangle]
pub unsafe extern "C" fn add(
    _lua_id: c_uint,
    args: *const *const c_char,
    result: *mut *const c_char,
) -> c_char {
    let a = CStr::from_ptr(*args).to_str().unwrap();
    let b = CStr::from_ptr(*(args.add(1))).to_str().unwrap();
    let joined = format!("{}{}", a, b);
    RESULT = Some(CString::new(joined).unwrap());
    match RESULT {
        Some(ref cstr) => *result = cstr.as_ptr(),
        _ => unreachable!(),
    }
    1
}

/*
#[no_mangle]
pub unsafe extern "C" fn raise_err(
    _lua_id: c_uint,
    _args: *const *const c_char,
    result: *mut *const c_char,
) -> c_char {
    RESULT = Some(CString::new("oh now!").unwrap());
    match RESULT {
        Some(ref cstr) => *result = cstr.as_ptr(),
        _ => unreachable!(),
    }
    0
}
*/