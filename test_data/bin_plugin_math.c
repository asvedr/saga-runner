// gcc -fPIC -shared
char* name() {
    return "mathops\0";
}

char* version() {
    return "0.1.0\0";
}

char* schema() {
    return "add(i, i) -> i\nsub(i, i) -> i\0";
}

void init(unsigned int _) {}

void finalize(unsigned int _) {}

char add(
    unsigned int _lua_id,
    void** args,
    void** result
) {
    static int RES;
    int a = *(int*)(*args);
    int b = *(int*)(*(++args));
    RES = a + b;
    *result = &RES;
    return 1;
}

char sub(
    unsigned int _lua_id,
    void** args,
    void** result
) {
    static int RES;
    int a = *(int*)(*args);
    int b = *(int*)(*(++args));
    RES = a - b;
    *result = &RES;
    return 1;
}

