if saga.payload.err_type == 'N' then
    std.err.wait(saga.payload.err_data)
end
if saga.payload.err_type == 'R' then
    std.err.retry(saga.payload.err_data)
end
if saga.payload.err_type == 'T' then
    std.err.terminal(saga.payload.err_data)
end
result = {
    saga=saga['val'],
    previous=required_steps['step1']
}
print('>> step2 >>')