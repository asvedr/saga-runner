if saga.payload.err_type == 'N' then
    if saga.payload.err_data then
        std.err.wait(saga.payload.err_data)
    else
        std.err.wait()
    end
end
if saga.payload.err_type == 'R' then
    if saga.payload.err_data then
        std.err.retry(saga.payload.err_data)
    else
        std.err.retry()
    end
end
if saga.payload.err_type == 'T' then
    if saga.payload.err_data then
        std.err.terminal(saga.payload.err_data)
    else
        std.err.terminal()
    end
end
result = 'step1_data'
print('>> step1 >>')