use std::ffi::{c_char, c_uint, c_int};

#[no_mangle]
pub extern "C" fn name() -> *const u8 {
    "mathops\0".as_ptr()
}

#[no_mangle]
pub extern "C" fn version() -> *const u8 {
    "0.1.0\0".as_ptr()
}

#[no_mangle]
pub extern "C" fn schema() -> *const u8 {
    concat!(
        "add(i, i) -> i\n",
        "sub(i, i) -> i\0"
    ).as_ptr()
}

static mut RESULT: c_int = 0;

#[no_mangle]
pub extern "C" fn init(_: c_uint) {}

#[no_mangle]
pub extern "C" fn finalize(_: c_uint) {}

#[no_mangle]
pub unsafe extern "C" fn add(
    _lua_id: c_uint,
    args: *const *const c_int,
    result: *mut *const c_int,
) -> c_char {
    let a = **args;
    let b = **(args.add(1));
    RESULT = a + b;
    *result = &RESULT;
    1
}

#[no_mangle]
pub unsafe extern "C" fn sub(
    _lua_id: c_uint,
    args: *const *const c_int,
    result: *mut *const c_int,
) -> c_char {
    let a = **args;
    let b = **(args.add(1));
    RESULT = a - b;
    *result = &RESULT;
    1
}
