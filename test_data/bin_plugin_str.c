// gcc -fPIC -shared
#include <string.h>

char* name() {
    return "strops\0";
}

char* version() {
    return "0.1.0\0";
}

char* schema() {
    return "add(s, s) -> s\nraise_err() -> s\0";
}

void init(unsigned int _) {}

void finalize(unsigned int _) {}

static char result[100];

char add(
	unsigned int lua_id,
    	void** args,
    	void** res
) {
	char* a = *args;
	char* b = *(++args);
	int a_len = strlen(a);
	int b_len = strlen(b);
	for(int i=0; i<a_len; ++i) {
		result[i] = a[i];
	}
	for(int i=0; i<b_len; ++i) {
		result[i+a_len] = b[i];
	}
	result[a_len + b_len] = 0;
	*res = &result[0];
	return 1;
}

char raise_err(
        unsigned int lua_id,
        void** args,
        void** res
) {
	*res = "oh no!";
	return 0;
}

