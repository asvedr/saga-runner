local m = load('math')

module['gte'] = function(a, b)
    return m.sub(a, b) > 0
end
