module['add'] = function (x, y)
    return x + y + 1
end

module['sub'] = function (x, y)
    return x - y
end

module['fun_with_err'] = function ()
    local x = nil
    return x.foo()
end

function _gcd(a, b)
    while (b ~= 0)
    do
        t = b
        b = a % b
        a = t
    end
    return a
end

module['gcd'] = _gcd
