#!/bin/sh

set -e

export PLUGIN_MATH="test_data/bin_plugin_math.so"
export PLUGIN_STR="test_data/bin_plugin_str.so"
export POSTGRES_DB="saga_runner"
export POSTGRES_USER="saga_runner"
export POSTGRES_PASSWORD="abc123"
export POSTGRES_PORT=5432

if [[ "$OSTYPE" == "linux"* ]]; then
  export POSTGRES_HOST=127.0.0.1
  export OS_LIB_SUFFIX="so"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  export POSTGRES_HOST=0.0.0.0
  export OS_LIB_SUFFIX="dylib"
else
	echo "unsupported OS"
	exit 1
fi
sh ./test_data/build_lib.sh bin_plugin_math $PLUGIN_MATH
sh ./test_data/build_lib.sh bin_plugin_str $PLUGIN_STR
cargo test --all-features $1 # -- --nocapture

