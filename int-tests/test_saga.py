import json
import time
from uuid import UUID
from typing import Dict

from saga_runner import SagaRunner


def test_simple_saga(db, saga_runner: SagaRunner):
    result = saga_runner.create_saga('one_empty_step', '')
    saga_id = result['id']
    time.sleep(0.1)
    status = saga_runner.saga_status(saga_id)
    assert status['name'] == 'one_empty_step'
    assert status['step'] == 'first'
    assert status['status'] == 'Completed'
    assert status['retried'] == 0
    assert status['execute_after']
    assert status['created']
    status = saga_runner.steps_status(saga_id)
    assert status == [{'name': 'first', 'order': 1, 'status': 'Completed'}]


def test_create_saga_bad_schema(db, saga_runner: SagaRunner):
    result = saga_runner.create_saga('unknown_type', '')
    assert result == 'Err: Custom { kind: Other, error: "SchemaNotFound" }'


def test_custom_retry_exec_ok(db, saga_runner: SagaRunner):
    saga_id = saga_runner.create_saga('custom_retry_err', {})['id']
    time.sleep(0.1)
    status = saga_runner.saga_status(saga_id)
    assert status['status'] == 'Completed'


def test_custom_retry_rb_on_step2_ok(db, saga_runner: SagaRunner):
    payload = json.dumps({'step2_e': True})
    saga_id = saga_runner.create_saga('custom_retry_err', payload)['id']
    time.sleep(0.1)
    status = saga_runner.saga_status(saga_id)
    assert status['status'] == 'RolledBack'
    status = saga_runner.steps_status(saga_id)
    assert status == [
        {'name': 'step1', 'order': 1, 'status': 'RolledBack'},
        {'name': 'step2', 'order': 2, 'status': 'RolledBack'},
        {'name': 'step3', 'order': 3, 'status': 'InProgress'},
    ]


def test_custom_retry_rb_on_step3_break_step2(db, saga_runner: SagaRunner):
    payload = json.dumps({'step2_r': True, 'step3_e': True})
    saga_id = saga_runner.create_saga('custom_retry_err', payload)['id']
    time.sleep(0.1)
    status = saga_runner.saga_status(saga_id)
    assert status['status'] == 'Broken'
    status = saga_runner.steps_status(saga_id)
    assert status == [
        {'name': 'step1', 'order': 1, 'status': 'Completed'},
        {'name': 'step2', 'order': 2, 'status': 'Broken'},
        {'name': 'step3', 'order': 3, 'status': 'RolledBack'},
    ]


def test_stats_empty(db, saga_runner: SagaRunner):
    stats = saga_runner.get_stats()
    # one active is periodic
    assert stats == {
        "active": 1, "rolled_back": 0, "broken": 0
    }


def test_stats_completed(db, saga_runner: SagaRunner):
    saga_id = saga_runner.create_saga('one_empty_step', '')['id']
    time.sleep(0.1)
    status = saga_runner.saga_status(saga_id)
    assert status['status'] == 'Completed'
    stats = saga_runner.get_stats()
    # one active is periodic
    assert stats == {"active": 1, "rolled_back": 0, "broken": 0}


def test_stats_rb(db, saga_runner: SagaRunner):
    payload = json.dumps({'step3_e': True})
    saga_id = saga_runner.create_saga('custom_retry_err', payload)['id']
    time.sleep(0.1)
    status = saga_runner.saga_status(saga_id)
    assert status['status'] == 'RolledBack'
    stats = saga_runner.get_stats()
    # one active is periodic
    assert stats == {"active": 1, "rolled_back": 1, "broken": 0}


def test_stats_broken(db, saga_runner: SagaRunner):
    payload = json.dumps({'step3_e': True, 'step1_r': True})
    saga_id = saga_runner.create_saga('custom_retry_err', payload)['id']
    time.sleep(0.1)
    status = saga_runner.saga_status(saga_id)
    assert status['status'] == 'Broken'
    stats = saga_runner.get_stats()
    # one active is periodic
    assert stats == {"active": 1, "rolled_back": 0, "broken": 1}


def test_periodic(db, saga_runner: SagaRunner):
    stats = saga_runner.get_stats()
    assert stats["active"] == 1
    start = time.time()
    while True:
        time.sleep(0.1)
        stats = saga_runner.get_stats()
        assert stats["active"] in (0, 1)
        if stats["active"] == 0:
            break
    end = time.time()
    assert (end - start) < 5.0
