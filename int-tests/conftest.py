import time
from os import environ
import psycopg2

import pytest

from saga_runner import SagaRunner

DB_TYPE = environ['DB_TYPE']
PG_HOST = environ.get('PG_HOST', None)
PG_PORT = environ.get('PG_PORT', None)
PG_USER = environ.get('PG_USER', None)
PG_NAME = environ.get('PG_NAME', None)
PG_PASS = environ.get('PG_PASS', None)
SLEEP_TIME = float(environ.get('SLEEP_TIME', 0.1))


def init_sqlite():
    return None


def drop_sqlite(_):
    return


def init_pg():
    cnn = psycopg2.connect(
        dbname=PG_NAME,
        user=PG_USER,
        password=PG_PASS,
        host=PG_HOST,
        port=PG_PORT,
    )
    cnn.execute('DROP TABLE IF EXISTS saga')
    cnn.execute('DROP TABLE IF EXISTS saga_step')
    cnn.execute('DROP TABLE IF EXISTS saga_log')
    return cnn


def drop_pg(cnn):
    cnn.close()


DB_FUN_MAP = {
    'sqlite': (init_sqlite, drop_sqlite),
    'pgsql': (init_pg, drop_pg),
}


@pytest.fixture
def db():
    init, drop = DB_FUN_MAP[DB_TYPE]
    db = init()
    yield db
    drop(db)


@pytest.fixture
def saga_runner(db) -> SagaRunner:
    with SagaRunner() as sr:
        time.sleep(SLEEP_TIME)
        yield sr
