import json
from contextlib import contextmanager
from datetime import datetime
import socket
import struct
import time
from typing import List, Union
from os import environ
import subprocess as sp
from signal import SIGTERM

PORT = int(environ['SAGA_RUNNER_TCP_PORT'])
BIN_PATH = environ.get('SAGA_RUNNER', './saga-runner')


@contextmanager
def _socket(port: int) -> socket.socket:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(('127.0.0.1', port))
    yield sock
    sock.close()


class SagaRunner:

    def __init__(self):
        self.handler = None

    def __enter__(self) -> 'SagaRunner':
        self.handler = sp.Popen([BIN_PATH, 'run'])
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.handler.send_signal(SIGTERM)
        self.handler.wait(3)

    def create_saga(self, name, data) -> Union[str, dict]:
        params = {'cmd': 'CRS', 'name': str(name)}
        if data is not None:
            params['body'] = data
        return self._send_message(params)

    def saga_status(self, saga_id) -> Union[str, dict]:
        return self._send_message({'cmd': 'SAS', 'id': str(saga_id)})

    def steps_status(self, saga_id) -> Union[str, list]:
        return self._send_message({'cmd': 'STS', 'id': str(saga_id)})

    def get_stats(self) -> Union[str, dict]:
        return self._send_message({'cmd': 'GST'})

    def get_errs(self, ts: datetime, limit: int) -> Union[str, list]:
        return self._send_message({'cmd': 'GER', 'timestamp': ts, 'limit': limit})
#         msg = 'GER'.encode()
#         ts = int(ts.timestamp())
#         msg += struct.pack('<li', ts, limit)
#         with _socket(PORT) as sock:
#             return self._send_recv(sock, msg)

    @classmethod
    def _send_message(cls, params: dict) -> Union[str, list, dict]:
        msg = cls._ser_msg(params)
        with _socket(PORT) as sock:
            return cls._send_recv(sock, msg)

    @staticmethod
    def _ser_msg(params: dict) -> bytes:
        body = json.dumps(params).encode()
        return struct.pack('<i', len(body)) + body

    @staticmethod
    def _send_recv(sock: socket.socket, msg: bytes) -> Union[str, list, dict]:
        sock.send(msg)
        time.sleep(1.0)
        bts = sock.recv(4)
        (bts_len,) = struct.unpack('<i', bts)
        bts = sock.recv(bts_len)
        msg = bts.decode().strip()
        if msg.startswith('Err'):
            return msg
        else:
            return json.loads(msg)
