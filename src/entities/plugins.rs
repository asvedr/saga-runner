use mlua::AsChunk;
use std::borrow::Cow;
use std::io;

pub const MODULE_PREFIX: &str = "__module__";

pub struct CodeBlock<'a, 'b> {
    pub source: &'a str,
    pub name: &'b str,
    pub shift: isize,
}

impl<'a, 'b, 'lua> AsChunk<'lua> for CodeBlock<'a, 'b> {
    fn source(&self) -> io::Result<Cow<[u8]>> {
        Ok(Cow::Borrowed(self.source.as_ref()))
    }

    fn name(&self) -> Option<String> {
        if self.shift == 0 {
            Some(format!("@{}", self.name))
        } else {
            Some(format!("@[[SHIFT {}]]{}", self.shift, self.name))
        }
    }
}
