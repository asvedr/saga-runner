use std::fmt::{Debug, Display, Formatter};
use std::io;
use std::str::FromStr;

#[cfg(feature = "sqlite")]
use easy_sqlite::errors::DbError;
use mlua::{Error as LuaError, Error};
#[cfg(feature = "pgsql")]
use postgres::Error as PGError;
#[cfg(feature = "sqlite")]
use rusqlite::Error as SQLiteError;

use crate::entities::saga::SagaStatus;

#[derive(Debug)]
pub enum PluginError {
    FileNotFound(String),
    CanNotReadFile,
    LuaError(LuaError),
    NotFound,
    BrokenMutex(String),
    BinaryPluginIsInvalid(String),
    BinaryPluginMethodNotFound(String),
    IO(io::Error),
    // BinaryPluginInitFailed(String),
}

#[derive(Debug)]
pub enum LuaCustomError {
    NeedToWait(Option<f64>),
    Retry(Option<String>, Option<f64>),
    Terminal(Option<String>),
}

#[derive(Debug)]
#[allow(dead_code)]
pub enum SagaError {
    LuaError(LuaError),
    LuaCustomError(LuaCustomError),
    // NeedToWait(String),
    // RetryableError(String),
    // TerminalError(String),
    DBError(String),
    UnexpectedStepStatus(SagaStatus),
    NotFound,
    SchemaNotFound,
    ExecutorJoinFailed,
    InvalidPayload(String),
    CanNotAcquireRepo,
    CanNotAcquireLua,
}

impl SagaError {
    pub fn can_not_acquire(&self) -> bool {
        matches!(self, Self::CanNotAcquireRepo | Self::CanNotAcquireLua)
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum SagaSchemaError {
    InvalidToml(String),
    InvalidSection {
        section: &'static str,
        expected_type: &'static str,
    },
    SectionRequired(&'static str),
    SagaMustHaveAtLeastOneStep,
    StepOrdersMustBeUnique,
    StepWithRollbackCanNotBeAfterStepWithNoRollback,
}

#[derive(Debug)]
pub enum SagaBuilderError {
    SchemaError(SagaSchemaError),
    SagaDirNotFound,
    IO(io::Error),
}

impl PartialEq for SagaError {
    fn eq(&self, other: &Self) -> bool {
        format!("{:?}", self) == format!("{:?}", other)
    }
}

impl Eq for SagaError {}

pub type PluginResult<T> = Result<T, PluginError>;
pub type SagaResult<T> = Result<T, SagaError>;
pub type SagaSchemaResult<T> = Result<T, SagaSchemaError>;
pub type SagaBuilderResult<T> = Result<T, SagaBuilderError>;

impl From<io::Error> for PluginError {
    fn from(value: io::Error) -> Self {
        Self::IO(value)
    }
}

impl From<LuaError> for PluginError {
    fn from(value: LuaError) -> Self {
        Self::LuaError(value)
    }
}

fn get_ext_root_cause(err: &LuaError) -> Option<String> {
    match err {
        Error::CallbackError { cause, .. } => get_ext_root_cause(cause),
        Error::ExternalError(err) => Some(format!("{}", err)),
        _ => None,
    }
}

impl From<LuaError> for SagaError {
    fn from(err: LuaError) -> Self {
        let ext_err = match get_ext_root_cause(&err) {
            None => return Self::LuaError(err),
            Some(val) => val,
        };
        match LuaCustomError::from_str(&ext_err) {
            Ok(ext) => Self::LuaCustomError(ext),
            _ => Self::LuaError(err),
        }
    }
}

#[cfg(feature = "sqlite")]
impl From<SQLiteError> for SagaError {
    fn from(value: SQLiteError) -> Self {
        Self::DBError(format!("{:?}", value))
    }
}

#[cfg(feature = "pgsql")]
impl From<PGError> for SagaError {
    fn from(value: PGError) -> Self {
        let msg = value.to_string();
        if msg == "query returned an unexpected number of rows" {
            return Self::NotFound;
        }
        Self::DBError(msg)
    }
}

#[cfg(feature = "sqlite")]
impl From<DbError> for SagaError {
    fn from(value: DbError) -> Self {
        match value {
            DbError::NotFound(_) => Self::NotFound,
            other => Self::DBError(format!("{:?}", other)),
        }
    }
}

#[derive(Debug)]
pub struct CanNotConvertIntToStatus;

impl Display for CanNotConvertIntToStatus {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for CanNotConvertIntToStatus {}

impl From<toml::de::Error> for SagaSchemaError {
    fn from(value: toml::de::Error) -> Self {
        Self::InvalidToml(value.to_string())
    }
}

impl From<SagaSchemaError> for SagaBuilderError {
    fn from(value: SagaSchemaError) -> Self {
        Self::SchemaError(value)
    }
}

impl From<io::Error> for SagaBuilderError {
    fn from(value: io::Error) -> Self {
        Self::IO(value)
    }
}

fn write_token<T: Display>(f: &mut Formatter, opt_t: &Option<T>) -> std::fmt::Result {
    if let Some(val) = opt_t {
        write!(f, "{}", val)
    } else {
        Ok(())
    }
}

impl Display for LuaCustomError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "LuaCustomError::")?;
        match self {
            LuaCustomError::NeedToWait(msg) => {
                write!(f, "N|")?;
                write_token(f, msg)
            }
            LuaCustomError::Retry(msg, time) => {
                write!(f, "R|")?;
                write_token(f, msg)?;
                write!(f, "|")?;
                write_token(f, time)
            }
            LuaCustomError::Terminal(msg) => {
                write!(f, "T|")?;
                write_token(f, msg)
            }
        }
    }
}

impl FromStr for LuaCustomError {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if !s.starts_with("LuaCustomError::") {
            return Err(());
        }
        let tokens = s
            .trim_start_matches("LuaCustomError::")
            .split('|')
            .collect::<Vec<_>>();
        match tokens[0] {
            "N" => {
                if tokens.len() < 2 {
                    return Err(());
                }
                let tm = match (tokens[1], f64::from_str(tokens[1])) {
                    ("", _) => None,
                    (_, Ok(val)) => Some(val),
                    _ => return Err(()),
                };
                Ok(Self::NeedToWait(tm))
            }
            "R" => {
                if tokens.len() < 3 {
                    return Err(());
                }
                let msg = match tokens[1] {
                    "" => None,
                    other => Some(other.to_string()),
                };
                let tm = match (tokens[2], f64::from_str(tokens[2])) {
                    ("", _) => None,
                    (_, Ok(val)) => Some(val),
                    _ => return Err(()),
                };
                Ok(Self::Retry(msg, tm))
            }
            "T" => {
                if tokens.len() < 2 {
                    return Err(());
                }
                let msg = match tokens[1] {
                    "" => None,
                    other => Some(other.to_string()),
                };
                Ok(Self::Terminal(msg))
            }
            _ => Err(()),
        }
    }
}

impl std::error::Error for LuaCustomError {}
