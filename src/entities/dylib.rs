use std::ffi::{c_char, c_uint, c_void};
use std::str::FromStr;

use libloading::Library;
use mlua::prelude::LuaResult;
use mlua::{Lua, MultiValue, Value};

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum DyValType {
    Void,
    Bool,
    Int,
    Real,
    Str,
    Json,
}

const DY_VAL_STR_MAP: &[(DyValType, &str)] = &[
    (DyValType::Void, "v"),
    (DyValType::Bool, "b"),
    (DyValType::Int, "i"),
    (DyValType::Real, "f"),
    (DyValType::Str, "s"),
    (DyValType::Json, "j"),
];

impl FromStr for DyValType {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        for (val, key) in DY_VAL_STR_MAP {
            if *key == s {
                return Ok(*val);
            }
        }
        Err(())
    }
}

impl ToString for DyValType {
    fn to_string(&self) -> String {
        for (key, val) in DY_VAL_STR_MAP {
            if key == self {
                return val.to_string();
            }
        }
        unreachable!()
    }
}

pub type ExFun = extern "C" fn(c_uint, *const *const c_void, *mut *const c_void) -> c_char;

pub type WrappedExFun<'a> = Box<dyn Fn(&'a Lua, MultiValue) -> LuaResult<Value<'a>>>;

pub struct FuncSchema {
    pub name: String,
    pub params: Vec<DyValType>,
    pub result: DyValType,
}

/**
   dylib must implement next functions,
   - name() -> str
   - version() -> str
   - schema() -> str
   - init(c_uint)
   - finalize(c_uint)
*/
pub struct DyLibCtx {
    pub lib: Box<Library>,
    pub name: String,
    pub funcs: Vec<FuncSchema>,
    pub init: Box<dyn Fn(c_uint)>,
    pub finalize: Box<dyn Fn(c_uint)>,
}
