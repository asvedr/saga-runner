#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Cmd {
    CreateSaga,
    SagaStatus,
    StepStatus,
    GetStats,
    GetErrs,
}

impl Cmd {
    const CODES: &'static [(&'static str, Self)] = &[
        ("CRS", Self::CreateSaga),
        ("SAS", Self::SagaStatus),
        ("STS", Self::StepStatus),
        ("GST", Self::GetStats),
        ("GER", Self::GetErrs),
    ];
    pub const CODE_LEN: usize = 3;

    #[allow(dead_code)]
    pub fn as_code(&self) -> &str {
        for (code, val) in Self::CODES {
            if self == val {
                return code;
            }
        }
        unreachable!()
    }

    pub fn from_str(src: &str) -> Option<Self> {
        for (code, val) in Self::CODES {
            if *code == src {
                return Some(*val);
            }
        }
        None
    }
}
