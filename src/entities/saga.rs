use uuid::Uuid;

use crate::entities::errors::CanNotConvertIntToStatus;

pub const DEFAULT_MAX_RETRIES: usize = 20;
pub const DEFAULT_RETRY_TIMEOUT: f64 = 3.0;
pub const DEFAULT_WAIT_TIMEOUT: f64 = 5.0;

pub type SagaId = Uuid;
pub type SagaStepId = u64;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum SagaStatus {
    NotReady,
    InProgress,
    Completed,
    RollingBack,
    RolledBack,
    Broken,
    Terminated,
    Skipped,
}

impl Default for SagaStatus {
    fn default() -> Self {
        Self::NotReady
    }
}

impl SagaStatus {
    const PAIRS: &'static [(Self, i32)] = &[
        (Self::NotReady, -1),
        (Self::InProgress, 1),
        (Self::Completed, 2),
        (Self::RollingBack, 3),
        (Self::RolledBack, 4),
        (Self::Broken, 5),
        (Self::Terminated, 6),
        (Self::Skipped, 7),
    ];

    pub const ACTIVE: &'static [Self] = &[Self::InProgress, Self::RollingBack];

    pub const TERMINAL: &'static [Self] = &[
        Self::Completed,
        Self::RolledBack,
        Self::Broken,
        Self::Terminated,
        Self::Skipped,
    ];

    pub fn as_i32(&self) -> i32 {
        for (k, v) in Self::PAIRS {
            if self == k {
                return *v;
            }
        }
        unreachable!()
    }

    #[allow(dead_code)]
    pub fn as_i32_ptr(&self) -> &'static i32 {
        for (k, v) in Self::PAIRS {
            if self == k {
                return v;
            }
        }
        unreachable!()
    }

    pub fn from_i32(val: i32) -> Result<Self, CanNotConvertIntToStatus> {
        for (v, k) in Self::PAIRS {
            if val == *k {
                return Ok(*v);
            }
        }
        Err(CanNotConvertIntToStatus)
    }

    pub fn is_forward(&self) -> bool {
        matches!(self, Self::NotReady | Self::InProgress | Self::Completed)
    }

    pub fn is_in_terminal_state(&self) -> bool {
        Self::TERMINAL.contains(self)
    }
}

#[derive(Default, Debug, Eq, PartialEq, Clone)]
pub struct Saga {
    pub id: SagaId,
    pub name: String,
    pub payload: Option<String>,
    pub step_name: String,
    pub status: SagaStatus,
    pub retried: u32,
    pub execute_after: Option<i64>,
    pub created_at: i64,
    pub updated_at: i64,
}

#[derive(Clone)]
pub struct NewSagaStep {
    // pub saga_id: SagaId,
    pub name: String,
    pub step_order: i32,
    pub status: SagaStatus,
}

pub struct SagaStep {
    pub id: SagaStepId,
    pub payload: Option<String>,
    pub updated_at: i64,

    pub saga_id: SagaId,
    pub name: String,
    pub step_order: u32,
    pub status: SagaStatus,
}

pub struct SagaLog {
    pub saga_id: SagaId,
    pub step_name: String,
    pub timestamp: i64,
    pub status: SagaStatus,
    pub error: Option<String>,
}

#[derive(Debug, Clone)]
pub enum PeriodicSchema {
    Daytime(i64),
    Every(i64),
}

#[derive(Default, Debug)]
pub struct SagaSchema {
    pub max_retries: usize,
    pub retry_timeout: f64,
    pub wait_timeout: f64,
    pub steps: Vec<SagaStepSchema>,
    pub periodic: Option<PeriodicSchema>,
}

#[derive(Default, Debug)]
pub struct SagaStepSchema {
    pub name: String,
    pub step_order: u32,
    pub required_payloads: Vec<String>,
    pub script_forward: String,
    pub script_backward: Option<String>,
    pub max_retries: Option<usize>,
    pub retry_timeout: Option<f64>,
    pub wait_timeout: Option<f64>,
}

#[cfg(test)]
impl PartialEq for SagaSchema {
    fn eq(&self, other: &Self) -> bool {
        format!("{:?}", self) == format!("{:?}", other)
    }
}

#[cfg(test)]
impl Eq for SagaSchema {}

#[cfg(test)]
impl PartialEq for SagaStepSchema {
    fn eq(&self, other: &Self) -> bool {
        format!("{:?}", self) == format!("{:?}", other)
    }
}

#[cfg(test)]
impl Eq for SagaStepSchema {}
