use std::fmt;
use std::path::PathBuf;

use mddd::macros::IConfig;
use mddd::traits::IParser;

#[derive(IConfig)]
#[allow(clippy::manual_non_exhaustive)]
pub struct MainConfig {
    #[description("root dir for saga schemas")]
    #[default(PathBuf::from("."))]
    pub schema_root: PathBuf,
    #[description("dirs with plugins")]
    #[default(Vec::new())]
    #[parser(StrListParser)]
    pub include: Vec<String>,
    #[default(dylib_ext())]
    #[parser(StrListParser)]
    #[description("extension of binary plugin")]
    pub dylib_ext: Vec<String>,
    #[description("db connect string")]
    #[default(":memory:".to_string())]
    pub db: String,
    #[default(1)]
    pub max_threads: usize,
    #[description("tcp port if tcp io required")]
    #[default(None)]
    pub tcp_port: Option<u16>,
    #[description("path to shell interpreter")]
    #[default("/bin/sh".to_string())]
    pub shell_int: String,
    #[prefix("SAGA_RUNNER_")]
    __meta__: (),
}

#[allow(unreachable_code)]
#[allow(clippy::needless_return)]
fn dylib_ext() -> Vec<String> {
    #[cfg(target_os = "macos")]
    return vec!["dylib".to_string(), "so".to_string()];
    #[cfg(target_os = "linux")]
    return vec!["so".to_string()];
    #[cfg(target_os = "windows")]
    return vec!["dll".to_string()];
    return vec!["so".to_string()];
}

struct StrListParser;

impl IParser<Vec<String>> for StrListParser {
    fn parse(src: &str) -> Result<Vec<String>, String> {
        Ok(src.split(',').map(|s| s.to_string()).collect())
    }
}

impl fmt::Display for MainConfig {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let sr = self.schema_root.to_str().unwrap_or_default();
        writeln!(f, "  schema_root = {}", sr)?;
        writeln!(f, "  include = {:?}", self.include)?;
        writeln!(f, "  dylib_ext = {:?}", self.dylib_ext)?;
        writeln!(f, "  db = {:?}", self.db)?;
        writeln!(f, "  tcp_port = {:?}", self.tcp_port)?;
        write!(f, "  max_threads = {}", self.max_threads)
    }
}
