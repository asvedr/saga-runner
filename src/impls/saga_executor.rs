use chrono::Utc;
use json::object::Object;
use json::JsonValue;
use uuid::Uuid;

use crate::entities::errors::LuaCustomError as LCE;
use crate::entities::errors::{SagaError, SagaResult};
use crate::entities::saga::{
    NewSagaStep, Saga, SagaId, SagaLog, SagaSchema, SagaStatus, SagaStep, SagaStepSchema,
};
use crate::proto::lua::ILuaExecutor;
use crate::proto::saga::{ISagaExecutor, ISagaRepo, ISagaSchemaManager, ITransaction};
use crate::utils::lua::format_lua_error;

const SAGA_VAR: &str = "saga";
const REQUIRED_STEPS_PAYLOAD_VAR: &str = "required_steps";
const ACTIVE_STEP_PAYLOAD_VAR: &str = "current_step";
const RESULT_VAR: &str = "result";

pub struct SagaExecutor<R: ISagaRepo, SM: ISagaSchemaManager, E: ILuaExecutor> {
    repo: R,
    schema_manager: SM,
    lua: E,
}

impl<R: ISagaRepo, SM: ISagaSchemaManager, E: ILuaExecutor> SagaExecutor<R, SM, E> {
    pub fn new(repo: R, schema_manager: SM, lua: E) -> Self {
        Self {
            repo,
            schema_manager,
            lua,
        }
    }

    pub fn get_saga_steps(
        &self,
        saga: &Saga,
        step_schema: &SagaStepSchema,
    ) -> SagaResult<Vec<SagaStep>> {
        let mut steps_to_request: Vec<&str> = vec![&saga.step_name];
        for payload in step_schema.required_payloads.iter() {
            steps_to_request.push(payload);
        }
        let steps = self.repo.get_saga_steps(&saga.id, &steps_to_request)?;
        if steps.len() != steps_to_request.len() {
            return Err(SagaError::NotFound);
        }
        Ok(steps)
    }

    fn fill_script_context(
        &self,
        saga: &Saga,
        required_payloads: &[SagaStep],
        active_step_payload: &Option<String>,
    ) -> SagaResult<()> {
        let saga_payload = match saga.payload {
            Some(ref val) => Self::loads_payload(val),
            _ => JsonValue::Null,
        };
        let active_step_payload = match active_step_payload {
            Some(val) => Self::loads_payload(val),
            _ => JsonValue::Null,
        };
        let mut steps_payload = Object::new();
        for step in required_payloads.iter() {
            let payload = match step.payload {
                None => JsonValue::Null,
                Some(ref val) => Self::loads_payload(val),
            };
            steps_payload.insert(&step.name, payload);
        }
        let mut saga_obj = Object::new();
        saga_obj.insert("payload", saga_payload);
        saga_obj.insert("id", saga.id.to_string().into());
        saga_obj.insert("retried", saga.retried.into());
        if let Some(val) = saga.execute_after {
            saga_obj.insert("execute_after", val.into());
        }
        saga_obj.insert("created_at", saga.created_at.into());
        saga_obj.insert("updated_at", saga.updated_at.into());
        self.lua.set_globals(&[
            (SAGA_VAR, JsonValue::Object(saga_obj)),
            (REQUIRED_STEPS_PAYLOAD_VAR, JsonValue::Object(steps_payload)),
            (ACTIVE_STEP_PAYLOAD_VAR, active_step_payload),
            (RESULT_VAR, JsonValue::Null),
        ])?;
        Ok(())
    }

    pub fn execute_script(&self, name: &str, code: &str) -> SagaResult<Option<String>> {
        match self.lua.exec_n_get_js(name, code, RESULT_VAR) {
            Ok(JsonValue::Null) => Ok(None),
            Ok(val) => Ok(Some(val.to_string())),
            Err(err) => {
                let err_txt = format_lua_error(&err);
                eprintln!("Lua error:\n{}", err_txt);
                Err(err.into())
            }
        }
    }

    fn change_saga_status(
        &self,
        saga: Saga,
        step: SagaStep,
        schema: &SagaSchema,
        result: SagaResult<Option<String>>,
    ) -> SagaResult<()> {
        let err = match result {
            Ok(payload) => {
                if saga.status.is_forward() {
                    return self.step_forward(saga, step, schema, payload);
                }
                return self.step_backward(saga, step, schema);
            }
            Err(err) => err,
        };
        let str_err = Self::serialize_error_for_log(&err);
        let timeout = match err {
            SagaError::LuaCustomError(LCE::NeedToWait(tm)) => {
                return self.need_to_wait(saga, schema, tm, str_err)
            }
            SagaError::LuaCustomError(LCE::Terminal(_)) => {
                return self.rollback_or_break(saga, step, schema, str_err)
            }
            SagaError::LuaCustomError(LCE::Retry(_, tm)) => tm,
            _ => None,
        };
        let (_, step_schema) = Self::get_step_schema(&saga, schema)?;
        let max_retries = step_schema.max_retries.unwrap_or(schema.max_retries);
        if saga.retried as usize >= max_retries {
            self.rollback_or_break(saga, step, schema, str_err)
        } else {
            self.inc_retries(saga, schema, timeout, str_err)
        }
    }

    fn step_forward(
        &self,
        mut saga: Saga,
        mut step: SagaStep,
        schema: &SagaSchema,
        payload: Option<String>,
    ) -> SagaResult<()> {
        step.payload = payload;
        step.status = SagaStatus::Completed;
        self.repo.update_step(&step, &["payload", "status"])?;
        let (mut index, _) = Self::get_step_schema(&saga, schema)?;
        index += 1;
        if index >= schema.steps.len() {
            saga.status = SagaStatus::Completed;
            return self.repo.update_saga(&saga, &["status"]);
        }
        saga.retried = 0;
        saga.step_name = schema.steps[index].name.clone();
        self.repo.update_saga(&saga, &["retried", "step_name"])?;
        self.repo.log(SagaLog {
            saga_id: saga.id,
            step_name: step.name,
            timestamp: Utc::now().timestamp(),
            status: SagaStatus::Completed,
            error: None,
        })
    }

    fn step_backward(
        &self,
        mut saga: Saga,
        mut step: SagaStep,
        schema: &SagaSchema,
    ) -> SagaResult<()> {
        step.status = SagaStatus::RolledBack;
        self.repo.update_step(&step, &["status"])?;
        let (mut index, _) = Self::get_step_schema(&saga, schema)?;
        if index == 0 {
            saga.status = SagaStatus::RolledBack;
            return self.repo.update_saga(&saga, &["status"]);
        }
        index -= 1;
        saga.step_name = schema.steps[index].name.clone();
        saga.retried = 0;
        self.repo.update_saga(&saga, &["retried", "step_name"])?;
        self.repo.log(SagaLog {
            saga_id: saga.id,
            step_name: step.name,
            timestamp: Utc::now().timestamp(),
            status: SagaStatus::RolledBack,
            error: None,
        })
    }

    fn rollback_or_break(
        &self,
        saga: Saga,
        step: SagaStep,
        schema: &SagaSchema,
        msg: String,
    ) -> SagaResult<()> {
        let (_, step_schema) = Self::get_step_schema(&saga, schema)?;
        if saga.status.is_forward() && step_schema.script_backward.is_some() {
            return self.set_rolling_back(saga, step, schema, msg);
        }
        self.set_broken(saga, step, msg)
    }

    fn need_to_wait(
        &self,
        mut saga: Saga,
        schema: &SagaSchema,
        timeout: Option<f64>,
        msg: String,
    ) -> SagaResult<()> {
        let (_, step_schema) = Self::get_step_schema(&saga, schema)?;
        let timeout = timeout.unwrap_or(step_schema.wait_timeout.unwrap_or(schema.wait_timeout));
        let now = Utc::now().timestamp();
        let execute_after = now + (timeout as i64);
        saga.execute_after = Some(execute_after);
        saga.retried = 0;
        self.repo
            .update_saga(&saga, &["execute_after", "retried"])?;
        self.repo.log(SagaLog {
            saga_id: saga.id,
            step_name: saga.step_name.clone(),
            timestamp: now,
            status: saga.status,
            error: Some(msg),
        })
    }

    fn inc_retries(
        &self,
        mut saga: Saga,
        schema: &SagaSchema,
        timeout: Option<f64>,
        msg: String,
    ) -> SagaResult<()> {
        let (_, step_schema) = Self::get_step_schema(&saga, schema)?;
        let timeout = timeout.unwrap_or(step_schema.retry_timeout.unwrap_or(schema.retry_timeout));
        let now = Utc::now().timestamp();
        let execute_after = now + (timeout as i64);
        saga.execute_after = Some(execute_after);
        saga.retried += 1;
        self.repo
            .update_saga(&saga, &["execute_after", "retried"])?;
        self.repo.log(SagaLog {
            saga_id: saga.id,
            step_name: saga.step_name,
            timestamp: now,
            status: saga.status,
            error: Some(msg),
        })
    }

    fn set_rolled_back(&self, mut saga: Saga, mut step: SagaStep) -> SagaResult<()> {
        saga.status = SagaStatus::RolledBack;
        self.repo.update_saga(&saga, &["status"])?;
        step.status = SagaStatus::RolledBack;
        self.repo.update_step(&step, &["status"])
    }

    fn set_broken(&self, mut saga: Saga, mut step: SagaStep, msg: String) -> SagaResult<()> {
        saga.status = SagaStatus::Broken;
        self.repo.update_saga(&saga, &["status"])?;
        step.status = SagaStatus::Broken;
        self.repo.update_step(&step, &["status"])?;
        self.repo.log(SagaLog {
            saga_id: saga.id,
            step_name: step.name,
            timestamp: Utc::now().timestamp(),
            status: SagaStatus::Broken,
            error: Some(msg),
        })
    }

    fn set_rolling_back(
        &self,
        mut saga: Saga,
        mut step: SagaStep,
        schema: &SagaSchema,
        msg: String,
    ) -> SagaResult<()> {
        let (index, _) = Self::get_step_schema(&saga, schema)?;
        if index == 0 {
            return self.set_rolled_back(saga, step);
        }
        saga.status = SagaStatus::RollingBack;
        saga.retried = 0;
        saga.step_name = schema.steps[index - 1].name.clone();
        self.repo
            .update_saga(&saga, &["status", "retried", "step_name"])?;
        step.status = SagaStatus::RolledBack;
        self.repo.update_step(&step, &["status"])?;
        self.repo.log(SagaLog {
            saga_id: saga.id,
            step_name: step.name,
            timestamp: Utc::now().timestamp(),
            status: SagaStatus::RolledBack,
            error: Some(msg),
        })
    }

    pub fn get_schema(&self, saga: &Saga) -> SagaResult<(&SagaSchema, &SagaStepSchema)> {
        let schema = match self.schema_manager.get_schema(&saga.name) {
            None => return Err(SagaError::SchemaNotFound),
            Some(val) => val,
        };
        let (_, step_schema) = Self::get_step_schema(saga, schema)?;
        Ok((schema, step_schema))
    }

    pub fn prepare_lua<'x>(
        &self,
        saga: &Saga,
        steps: &[SagaStep],
        active_step: &SagaStep,
        step_schema: &'x SagaStepSchema,
    ) -> SagaResult<(String, &'x str)> {
        self.fill_script_context(saga, steps, &active_step.payload)?;
        let (name_suff, script) = match saga.status {
            SagaStatus::InProgress => ("execute", &step_schema.script_forward),
            SagaStatus::RollingBack => {
                let code = match step_schema.script_backward {
                    Some(ref val) => val,
                    None => return Err(SagaError::UnexpectedStepStatus(SagaStatus::RollingBack)),
                };
                ("rollback", code)
            }
            other => return Err(SagaError::UnexpectedStepStatus(other)),
        };
        let name = format!("{}:{}", saga.step_name, name_suff);
        Ok((name, script))
    }

    fn loads_payload(src: &str) -> JsonValue {
        if src.is_empty() {
            return JsonValue::Null;
        }
        match json::parse(src) {
            Ok(val) => val,
            _ => JsonValue::String(src.to_string()),
        }
    }

    fn get_step_schema<'d>(
        saga: &Saga,
        schema: &'d SagaSchema,
    ) -> SagaResult<(usize, &'d SagaStepSchema)> {
        for (i, step) in schema.steps.iter().enumerate() {
            if step.name == saga.step_name {
                return Ok((i, step));
            }
        }
        Err(SagaError::SchemaNotFound)
    }

    fn serialize_error_for_log(err: &SagaError) -> String {
        match err {
            SagaError::LuaError(le) => le.to_string(),
            SagaError::LuaCustomError(lce) => lce.to_string(),
            SagaError::DBError(dbe) => dbe.clone(),
            other => format!("{:?}", other),
        }
    }
}

impl<R: ISagaRepo, SM: ISagaSchemaManager, E: ILuaExecutor> ISagaExecutor
    for SagaExecutor<R, SM, E>
{
    fn create_saga(&self, name: String, payload: Option<String>) -> SagaResult<SagaId> {
        let schema = match self.schema_manager.get_schema(&name) {
            None => return Err(SagaError::SchemaNotFound),
            Some(val) => val,
        };
        let id = Uuid::new_v4();
        let new_saga = Saga {
            id,
            name,
            payload,
            ..Default::default()
        };
        let new_steps = schema
            .steps
            .iter()
            .map(|schema| NewSagaStep {
                name: schema.name.clone(),
                step_order: schema.step_order as i32,
                status: SagaStatus::InProgress,
            })
            .collect();
        let trx = self.repo.transaction()?;
        trx.process_result_dbg(self.repo.create(new_saga, new_steps))?;
        Ok(id)
    }

    fn execute_saga(&self, saga_id: &SagaId) -> SagaResult<()> {
        let saga = self.repo.get_saga(saga_id)?;
        let (schema, step_schema) = self.get_schema(&saga)?;
        let mut steps = self.get_saga_steps(&saga, step_schema)?;
        let active_step = steps.pop().unwrap();
        let (name, code) = self.prepare_lua(&saga, &steps, &active_step, step_schema)?;
        let result = self.execute_script(&name, code);
        let trx = self.repo.transaction()?;
        let result = self.change_saga_status(saga, active_step, schema, result);
        trx.process_result_dbg(result)
    }
}
