use chrono::{Timelike, Utc};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::Duration;
use std::{mem, thread};

use crate::entities::config::MainConfig;
use crate::entities::errors::{SagaError, SagaResult};
use crate::entities::saga::{PeriodicSchema, SagaId};
use crate::proto::saga::{
    ISagaExecutor, ISagaExecutorFactory, ISagaRepo, ISagaRepoPool, ISagaRunner, ISagaSchemaManager,
};

pub struct SagaRunner<F: ISagaExecutorFactory + 'static, R: ISagaRepoPool> {
    executor_factory: F,
    repo_pool: R,
    max_threads: usize,
    periodics: Vec<(String, PeriodicSchema)>,
}

struct Ctx<E: ISagaExecutor + 'static> {
    executor: E,
}

unsafe impl<E: ISagaExecutor + 'static> Send for Ctx<E> {}
unsafe impl<E: ISagaExecutor + 'static> Sync for Ctx<E> {}

impl<E: ISagaExecutor + 'static> Ctx<E> {
    fn execute(&self, saga_id: SagaId) -> SagaResult<()> {
        self.executor.execute_saga(&saga_id)
    }
}

impl<F: ISagaExecutorFactory + 'static, R: ISagaRepoPool> SagaRunner<F, R> {
    pub fn new<SSM: ISagaSchemaManager>(
        saga_executor_factory: F,
        repo_pool: R,
        schema_manager: SSM,
        config: &MainConfig,
    ) -> Self {
        Self {
            executor_factory: saga_executor_factory,
            repo_pool,
            max_threads: config.max_threads,
            periodics: schema_manager.get_periodics(),
        }
    }

    fn get_ready_ids(&self, count: usize) -> SagaResult<Vec<SagaId>> {
        let repo = match self.repo_pool.acquire_repo() {
            None => return Ok(Vec::new()),
            Some(val) => val,
        };
        repo.get_ready_ids(count)
    }
}

impl<F: ISagaExecutorFactory + 'static, R: ISagaRepoPool> SagaRunner<F, R> {
    pub fn iteration(&self) -> SagaResult<()> {
        let ids = self.get_ready_ids(self.max_threads)?;
        let mut handlers = Vec::new();
        for id in ids.into_iter() {
            let executor = match self.executor_factory.produce() {
                Ok(val) => val,
                Err(err) if err.can_not_acquire() => {
                    eprintln!("Saga process skipped: {:?}", err);
                    break;
                }
                Err(other) => return Err(other),
            };
            let ctx = Ctx { executor };
            handlers.push((id, thread::spawn(move || ctx.execute(id))));
        }
        let mut critical_err = false;
        for (saga_id, handler) in handlers {
            match handler.join() {
                Err(_) => critical_err = true,
                Ok(Ok(_)) => (),
                Ok(Err(err)) => eprintln!("saga {} failed: {:?}", saga_id, err),
            }
        }
        if critical_err {
            Err(SagaError::ExecutorJoinFailed)
        } else {
            Ok(())
        }
    }

    fn start_periodics(&self) -> SagaResult<()> {
        let repo = match self.repo_pool.acquire_repo() {
            None => return Err(SagaError::CanNotAcquireRepo),
            Some(val) => val,
        };
        let mut to_create = Vec::new();
        let now = Utc::now();
        let day_begin = now
            .with_hour(0)
            .unwrap()
            .with_minute(0)
            .unwrap()
            .with_second(0)
            .unwrap()
            .timestamp();
        let time_since_day_begin = now.timestamp() - day_begin;
        for (name, schema) in self.periodics.iter() {
            let saga = match repo.get_last_saga_of_name(name) {
                Ok(val) => val,
                Err(SagaError::NotFound) => {
                    to_create.push(name.clone());
                    continue;
                }
                Err(other) => return Err(other),
            };
            if !saga.status.is_in_terminal_state() {
                continue;
            }
            let ready = match schema {
                PeriodicSchema::Daytime(ref tm) => time_since_day_begin > *tm,
                PeriodicSchema::Every(ref tm) => tm - saga.updated_at > 0,
            };
            if ready {
                to_create.push(name.clone())
            }
        }
        mem::drop(repo);
        for name in to_create {
            self.create_saga(name, None)?;
        }
        Ok(())
    }
}

impl<F: ISagaExecutorFactory + 'static, R: ISagaRepoPool> ISagaRunner for SagaRunner<F, R> {
    fn create_saga(&self, name: String, payload: Option<String>) -> SagaResult<SagaId> {
        let executor = self.executor_factory.produce()?;
        executor.create_saga(name, payload)
    }

    fn run(&self, term_flag: Arc<AtomicBool>) -> SagaResult<()> {
        while !term_flag.load(Ordering::Relaxed) {
            self.iteration()?;
            self.start_periodics()?;
            thread::yield_now();
        }
        Ok(())
    }
}
