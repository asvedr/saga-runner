use std::mem;
use std::net::TcpStream;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, Mutex};
use std::thread::JoinHandle;
use std::{io, thread};

use crate::entities::config::MainConfig;
use crate::impls::tcp_io_inner::TcpIOInner;
use crate::proto::common::IIO;
use crate::proto::saga::{ISagaRepoPool, ISagaRunner};

pub struct TcpIO<R: ISagaRepoPool + Clone + 'static, SR: ISagaRunner + Clone + 'static> {
    port: u16,
    handler: Mutex<Option<JoinHandle<()>>>,
    term_flag: Arc<AtomicBool>,
    saga_repo_pool: R,
    saga_runner: SR,
}

impl<R: ISagaRepoPool + Clone + 'static, SR: ISagaRunner + Clone + 'static> TcpIO<R, SR> {
    pub fn new(saga_repo_pool: R, saga_runner: SR, config: &MainConfig) -> Self {
        Self {
            port: config.tcp_port.unwrap(),
            handler: Mutex::new(None),
            term_flag: Arc::new(AtomicBool::new(false)),
            saga_repo_pool,
            saga_runner,
        }
    }
}

impl<R: ISagaRepoPool + Clone + 'static, SR: ISagaRunner + Clone + 'static> IIO for TcpIO<R, SR> {
    fn run(&self) -> io::Result<()> {
        // let mut lock_c = self
        //     .saga_ctx
        //     .lock()
        //     .map_err(|_| io::Error::new(io::ErrorKind::Other, "poisoned mutex"))?;
        // let (repo, exec) = match mem::replace(&mut *lock_c, None) {
        //     Some(val) => val,
        //     _ => panic!("tcp already run"),
        // };
        let listener = TcpIOInner::new(
            self.port,
            self.saga_repo_pool.clone(),
            self.saga_runner.clone(),
            self.term_flag.clone(),
        )?;
        let port = self.port;
        let handler = thread::spawn(move || {
            println!("tcp started on port: {}", port);
            let _ = listener.run();
        });
        let mut lock_h = self
            .handler
            .lock()
            .map_err(|_| io::Error::new(io::ErrorKind::Other, "poisoned mutex"))?;
        if lock_h.is_some() {
            panic!("tcp already run");
        }
        *lock_h = Some(handler);
        Ok(())
    }

    fn stop(&self) -> io::Result<()> {
        self.term_flag.store(true, Ordering::SeqCst);
        thread::yield_now();
        TcpStream::connect(format!("localhost:{}", self.port))?;
        let mut guard = self
            .handler
            .lock()
            .map_err(|_| io::Error::new(io::ErrorKind::Other, "mutex poisoned"))?;
        let handler = match mem::replace(&mut *guard, None) {
            None => panic!("handler expected"),
            Some(val) => val,
        };
        let _ = handler.join();
        println!("tcp stopped");
        Ok(())
    }
}
