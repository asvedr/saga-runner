use std::fs;
use std::path::{Path, PathBuf};

use mlua::prelude::LuaResult;
use mlua::{Error, FromLua, Lua, MultiValue, Table};

use crate::proto::lua::IStdMod;

pub struct Mod;

fn f_list_dir(_: &Lua, path: String) -> LuaResult<Vec<String>> {
    let entry = fs::read_dir(path).map_err(Error::external)?;
    let mut result = Vec::new();
    for res_item in entry {
        let item = res_item.map_err(Error::external)?;
        let name = match item.path().file_name() {
            None => "".to_string(),
            Some(val) => val.to_str().unwrap_or_default().to_string(),
        };
        result.push(name)
    }
    Ok(result)
}

fn f_type(_: &Lua, path: String) -> LuaResult<Option<String>> {
    let path = Path::new(&path);
    if path.is_file() {
        return Ok(Some("file".to_string()));
    }
    if path.is_dir() {
        return Ok(Some("dir".to_string()));
    }
    Ok(None)
}

fn f_join(lua: &Lua, args: MultiValue) -> LuaResult<String> {
    let mut items = args.into_iter();
    let first = match items.next() {
        Some(val) => val,
        _ => return Ok(String::default()),
    };
    let mut result = PathBuf::from(String::from_lua(first, lua)?);
    for item in items {
        let item = String::from_lua(item, lua)?;
        result.push(item);
    }
    match result.to_str() {
        None => Err(Error::RuntimeError("std.fs.path.join failed".to_string())),
        Some(val) => Ok(val.to_string()),
    }
}

impl IStdMod for Mod {
    fn path(&self) -> &[&str] {
        &["fs", "path"]
    }

    fn init<'lua>(&self, _: usize, lua: &'lua Lua, table: &'lua Table) -> LuaResult<()> {
        let fun = lua.create_function(f_list_dir)?;
        table.set("list_dir", fun)?;
        let fun = lua.create_function(f_join)?;
        table.set("join", fun)?;
        let fun = lua.create_function(f_type)?;
        table.set("type", fun)
    }
}
