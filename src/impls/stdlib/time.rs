use chrono::{DateTime, Datelike, LocalResult, TimeZone, Timelike, Utc};
use mlua::prelude::LuaResult;
use mlua::{Error, Lua, Table};

use crate::proto::lua::IStdMod;

pub struct Mod;

fn f_ts_to_time(lua: &Lua, secs: i64) -> LuaResult<Table> {
    let dt: DateTime<Utc> = match Utc.timestamp_opt(secs, 0) {
        LocalResult::Single(val) => val,
        _ => return Err(Error::RuntimeError("invalid timestamp".to_string())),
    };
    let tbl = lua.create_table()?;
    tbl.set("second", dt.second())?;
    tbl.set("minute", dt.minute())?;
    tbl.set("hour", dt.hour())?;
    tbl.set("day", dt.day())?;
    tbl.set("month", dt.month())?;
    tbl.set("year", dt.year())?;
    Ok(tbl)
}

fn f_time_to_ts(_: &Lua, time: Table) -> LuaResult<i64> {
    let sec = time.get("second")?;
    let min = time.get("minute")?;
    let hour = time.get("hour")?;
    let day = time.get("day")?;
    let mon = time.get("month")?;
    let year = time.get("year")?;
    let utc: DateTime<Utc> = match Utc.with_ymd_and_hms(year, mon, day, hour, min, sec) {
        LocalResult::Single(val) => val,
        _ => return Err(Error::RuntimeError("invalid time".to_string())),
    };
    Ok(utc.timestamp())
}

impl IStdMod for Mod {
    fn path(&self) -> &[&str] {
        &["time"]
    }

    fn init<'lua>(&self, _: usize, lua: &'lua Lua, table: &'lua Table) -> LuaResult<()> {
        let fun = lua.create_function(|_, ()| Ok(Utc::now().timestamp()))?;
        table.set("timestamp", fun)?;
        let fun = lua.create_function(f_ts_to_time)?;
        table.set("timestamp_to_time", fun)?;
        let fun = lua.create_function(f_time_to_ts)?;
        table.set("time_to_timestamp", fun)
    }
}
