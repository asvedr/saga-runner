use std::process;
use std::process::Stdio;

use mlua::prelude::{LuaError, LuaResult};
use mlua::{Lua, Table};

use crate::entities::config::MainConfig;
use crate::proto::lua::IStdMod;

pub struct Mod {
    shell: String,
}

impl Mod {
    pub fn new(config: &MainConfig) -> Self {
        Self {
            shell: config.shell_int.clone(),
        }
    }
}

fn f_call<'a>(sh: &str, lua: &'a Lua, cmd: String) -> LuaResult<Table<'a>> {
    let map_err = |err| {
        let msg = format!("running {:?} failed: {:?}", cmd, err,);
        LuaError::RuntimeError(msg)
    };
    let output = process::Command::new(sh)
        .arg("-c")
        .arg(&cmd)
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .map_err(&map_err)?
        .wait_with_output()
        .map_err(&map_err)?;
    let table = lua.create_table()?;
    table.set("status", output.status.code().unwrap())?;
    if let Ok(val) = String::from_utf8(output.stdout) {
        table.set("stdout", val)?;
    } else {
        table.set("stdout", "<bin>".to_string())?;
    }
    if let Ok(val) = String::from_utf8(output.stderr) {
        table.set("stderr", val)?;
    } else {
        table.set("stderr", "<bin>".to_string())?;
    }
    Ok(table)
}

impl IStdMod for Mod {
    fn path(&self) -> &[&str] {
        &["proc"]
    }

    fn init<'lua>(&self, _: usize, lua: &'lua Lua, table: &'lua Table) -> LuaResult<()> {
        let sh = self.shell.clone();
        let call = lua.create_function(move |lua, cmd| f_call(&sh, lua, cmd))?;
        table.set("call", call)
    }
}
