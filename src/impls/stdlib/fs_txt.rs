use std::fs;
use std::io::{Read, Write};

use mlua::prelude::LuaResult;
use mlua::{Error, Lua, Table};

use crate::proto::lua::IStdMod;

pub struct Mod;

fn f_read(_: &Lua, path: String) -> LuaResult<String> {
    let mut file = fs::File::open(path).map_err(Error::external)?;
    let mut text = String::new();
    file.read_to_string(&mut text).map_err(Error::external)?;
    Ok(text)
}

fn f_write(_: &Lua, args: (String, String)) -> LuaResult<()> {
    let (path, data) = args;
    let mut file = fs::File::create(path).map_err(Error::external)?;
    file.write_all(data.as_bytes()).map_err(Error::external)?;
    Ok(())
}

impl IStdMod for Mod {
    fn path(&self) -> &[&str] {
        &["fs", "txt"]
    }

    fn init<'lua>(&self, _: usize, lua: &'lua Lua, table: &'lua Table) -> LuaResult<()> {
        let fun = lua.create_function(f_read)?;
        table.set("read", fun)?;
        let fun = lua.create_function(f_write)?;
        table.set("write", fun)
    }
}
