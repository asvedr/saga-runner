use mlua::prelude::LuaResult;
use mlua::{Lua, Table, Value};

use crate::impls::plugin_loader::dylib_tools::js_parser::parse_json;
use crate::impls::plugin_loader::dylib_tools::js_render::render_json;

use crate::proto::lua::IStdMod;

pub struct Mod;

fn f_dumps(_: &Lua, value: Value) -> LuaResult<String> {
    render_json(value)
}

fn f_loads(lua: &Lua, value: String) -> LuaResult<Value> {
    parse_json(lua, &value)
}

impl IStdMod for Mod {
    fn path(&self) -> &[&str] {
        &["json"]
    }

    fn init<'lua>(&self, _: usize, lua: &'lua Lua, table: &'lua Table) -> LuaResult<()> {
        let fun = lua.create_function(f_loads)?;
        table.set("loads", fun)?;
        let fun = lua.create_function(f_dumps)?;
        table.set("dumps", fun)
    }
}
