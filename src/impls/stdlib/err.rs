use mlua::prelude::LuaResult;
use mlua::{Error, Lua, Table};

use crate::entities::errors::LuaCustomError;
use crate::proto::lua::IStdMod;

pub struct Mod;

const MSG_KEY: &str = "msg";
const TIMEOUT_KEY: &str = "timeout";

fn f_raise_retry(_: &Lua, tbl: Option<Table>) -> LuaResult<()> {
    let tbl = match tbl {
        Some(val) => val,
        _ => return Err(Error::external(LuaCustomError::Retry(None, None))),
    };
    let msg: Option<String> = if tbl.contains_key(MSG_KEY)? {
        tbl.get(MSG_KEY)?
    } else {
        None
    };
    let timeout: Option<f64> = if tbl.contains_key(TIMEOUT_KEY)? {
        tbl.get(TIMEOUT_KEY)?
    } else {
        None
    };
    let err = LuaCustomError::Retry(msg, timeout);
    Err(Error::external(err))
}

fn f_raise_terminal(_: &Lua, tbl: Option<Table>) -> LuaResult<()> {
    let tbl = match tbl {
        Some(val) => val,
        _ => return Err(Error::external(LuaCustomError::Terminal(None))),
    };
    let msg: Option<String> = if tbl.contains_key(MSG_KEY)? {
        tbl.get(MSG_KEY)?
    } else {
        None
    };
    let err = LuaCustomError::Terminal(msg);
    Err(Error::external(err))
}

fn f_raise_need_to_wait(_: &Lua, tbl: Option<Table>) -> LuaResult<()> {
    let tbl = match tbl {
        Some(val) => val,
        _ => return Err(Error::external(LuaCustomError::NeedToWait(None))),
    };
    let timeout: Option<f64> = if tbl.contains_key(TIMEOUT_KEY)? {
        tbl.get(TIMEOUT_KEY)?
    } else {
        None
    };
    let err = LuaCustomError::NeedToWait(timeout);
    Err(Error::external(err))
}

impl IStdMod for Mod {
    fn path(&self) -> &[&str] {
        &["err"]
    }

    fn init<'lua>(&self, _: usize, lua: &'lua Lua, table: &'lua Table) -> LuaResult<()> {
        let fun = lua.create_function(f_raise_retry)?;
        table.set("retry", fun)?;
        let fun = lua.create_function(f_raise_terminal)?;
        table.set("terminal", fun)?;
        let fun = lua.create_function(f_raise_need_to_wait)?;
        table.set("wait", fun)
    }
}
