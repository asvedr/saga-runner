use crate::entities::config::MainConfig;
use crate::proto::lua::IStdMod;

mod err;
mod fs_txt;
mod json;
mod os_path;
mod proc;
mod time;

pub fn all_mods(config: &MainConfig) -> Vec<Box<dyn IStdMod>> {
    vec![
        Box::new(json::Mod),
        Box::new(fs_txt::Mod),
        Box::new(proc::Mod::new(config)),
        Box::new(os_path::Mod),
        Box::new(err::Mod),
        Box::new(time::Mod),
    ]
}
