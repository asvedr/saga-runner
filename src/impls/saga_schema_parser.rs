use std::fs;
use std::io::Read;
use std::path::Path;
use std::str::FromStr;
use toml::{Table, Value};

use crate::entities::errors::{SagaResult, SagaSchemaError, SagaSchemaResult};
use crate::entities::saga::{
    PeriodicSchema, SagaSchema, SagaStepSchema, DEFAULT_MAX_RETRIES, DEFAULT_RETRY_TIMEOUT,
    DEFAULT_WAIT_TIMEOUT,
};
use crate::proto::saga::ISagaSchemaParser;

const KEY_SAGA: &str = "saga";
const KEY_STEPS: &str = "steps";
const KEY_MAX_RETRIES: &str = "max_retries";
const KEY_RETRY_TIMEOUT: &str = "retry_timeout";
const KEY_WAIT_TIMEOUT: &str = "wait_timeout";
const KEY_FORWARD_CODE: &str = "exec_code";
const KEY_FORWARD_FILE: &str = "exec_file";
const KEY_BACKWARD_CODE: &str = "rollback_code";
const KEY_BACKWARD_FILE: &str = "rollback_file";
const KEY_STEP_ORDER: &str = "order";
const KEY_REQUIRED_PAYLOADS: &str = "required_payloads";
const KEY_PERIODIC: &str = "periodic";

const KEY_EVERY: &str = "every";

pub struct SagaSchemaParser;

fn to_usize(key: &'static str, val: &Value) -> SagaSchemaResult<usize> {
    match val {
        Value::Integer(val) if *val >= 0 => Ok(*val as usize),
        _ => Err(SagaSchemaError::InvalidSection {
            section: key,
            expected_type: "non negative int",
        }),
    }
}

fn to_u32(key: &'static str, val: &Value) -> SagaSchemaResult<u32> {
    to_usize(key, val).map(|val| val as u32)
}

fn to_pos_f64(key: &'static str, val: &Value) -> SagaSchemaResult<f64> {
    match val {
        Value::Integer(val) if *val >= 0 => Ok(*val as f64),
        Value::Float(val) if *val >= 0.0 => Ok(*val),
        _ => Err(SagaSchemaError::InvalidSection {
            section: key,
            expected_type: "non negative float",
        }),
    }
}

fn to_str(section: &'static str, val: &Value) -> SagaSchemaResult<String> {
    match val.as_str() {
        None => Err(SagaSchemaError::InvalidSection {
            section,
            expected_type: "string",
        }),
        Some(val) => Ok(val.to_string()),
    }
}

fn parse_time(txt: &str) -> SagaSchemaResult<i64> {
    let err = SagaSchemaError::InvalidSection {
        section: "periodic",
        expected_type: "'h:m:s' or 'every h:m:s'",
    };
    let err_copy = err.clone();
    let split = txt
        .split(':')
        .map(i64::from_str)
        .collect::<Result<Vec<_>, _>>()
        .map_err(|_| err_copy)?;
    if split.len() != 3 {
        return Err(err);
    }
    Ok(split[0] * 60 * 60 + split[1] * 60 + split[2])
}

fn parse_periodic(txt: &str) -> SagaSchemaResult<PeriodicSchema> {
    let txt = txt.trim();
    if txt.starts_with(KEY_EVERY) {
        let secs = parse_time(txt.trim_start_matches(KEY_EVERY).trim())?;
        Ok(PeriodicSchema::Every(secs))
    } else {
        let secs = parse_time(txt)?;
        Ok(PeriodicSchema::Daytime(secs))
    }
}

fn to_vec_str(section: &'static str, val: &Value) -> SagaSchemaResult<Vec<String>> {
    let expected_type = "list of string";
    let vec_val = match val {
        Value::Array(val) => val,
        _ => {
            return Err(SagaSchemaError::InvalidSection {
                section,
                expected_type,
            })
        }
    };
    let mut result = vec![];
    for item in vec_val {
        match item.as_str() {
            Some(val) => result.push(val.to_string()),
            _ => {
                return Err(SagaSchemaError::InvalidSection {
                    section,
                    expected_type,
                })
            }
        }
    }
    Ok(result)
}

fn read_file(section: &'static str, path: &Path) -> SagaSchemaResult<String> {
    let mut file = fs::File::open(path).map_err(|_| SagaSchemaError::InvalidSection {
        section,
        expected_type: "valid file path",
    })?;
    let mut text = String::new();
    file.read_to_string(&mut text)
        .map_err(|_| SagaSchemaError::InvalidSection {
            section,
            expected_type: "text file path",
        })?;
    Ok(text)
}

fn get_script(
    path: &Path,
    tbl: &Table,
    key_code: &'static str,
    key_file: &'static str,
    toml_name: Option<&str>,
) -> SagaSchemaResult<Option<String>> {
    match tbl.get(key_code) {
        None => (),
        Some(Value::String(val)) => return Ok(Some(val.clone())),
        _ => {
            return Err(SagaSchemaError::InvalidSection {
                section: key_code,
                expected_type: "string",
            })
        }
    }
    match tbl.get(key_file) {
        None => (),
        Some(Value::String(val)) => return Ok(Some(read_file(key_file, &path.join(val))?)),
        _ => {
            return Err(SagaSchemaError::InvalidSection {
                section: key_file,
                expected_type: "file path",
            })
        }
    }
    if let Some(name) = toml_name {
        let data = read_file(key_file, &path.join(format!("{}.lua", name)))?;
        Ok(Some(data))
    } else {
        Ok(None)
    }
}

impl SagaSchemaParser {
    fn de_general(tbl: &Table) -> SagaSchemaResult<SagaSchema> {
        let result = SagaSchema {
            max_retries: tbl
                .get(KEY_MAX_RETRIES)
                .map(|v| to_usize(KEY_MAX_RETRIES, v))
                .unwrap_or(Ok(DEFAULT_MAX_RETRIES))?,
            retry_timeout: tbl
                .get(KEY_RETRY_TIMEOUT)
                .map(|v| to_pos_f64(KEY_RETRY_TIMEOUT, v))
                .unwrap_or(Ok(DEFAULT_RETRY_TIMEOUT))?,
            wait_timeout: tbl
                .get(KEY_WAIT_TIMEOUT)
                .map(|v| to_pos_f64(KEY_WAIT_TIMEOUT, v))
                .unwrap_or(Ok(DEFAULT_WAIT_TIMEOUT))?,
            steps: vec![],
            periodic: match tbl.get(KEY_PERIODIC) {
                Some(v) => {
                    let as_str = to_str(KEY_PERIODIC, v)?;
                    Some(parse_periodic(&as_str)?)
                }
                _ => None,
            },
        };
        Ok(result)
    }

    fn de_step(path: &Path, name: &str, toml: &Value) -> SagaSchemaResult<SagaStepSchema> {
        let tbl = match toml {
            Value::Table(val) => val,
            _ => {
                return Err(SagaSchemaError::InvalidSection {
                    section: "step body",
                    expected_type: "table",
                })
            }
        };
        let script_forward = get_script(
            path,
            &tbl,
            KEY_FORWARD_CODE,
            KEY_FORWARD_FILE,
            Some(name),
            // Some(format!("{}.lua", name)),
        )?
        .unwrap();
        let script_backward = get_script(path, &tbl, KEY_BACKWARD_CODE, KEY_BACKWARD_FILE, None)?;
        let schema = SagaStepSchema {
            script_backward,
            // match tbl.get(KEY_ROLLBACK) {
            //     Some(Value::String(val)) => Some(val.clone()),
            //     Some(_) => {
            //         return Err(SagaSchemaError::InvalidSection {
            //             section: KEY_ROLLBACK,
            //             expected_type: "filename",
            //         })
            //     }
            //     _ => None,
            // },
            name: name.to_string(),
            step_order: match tbl.get(KEY_STEP_ORDER) {
                Some(v) => to_u32(KEY_STEP_ORDER, v)?,
                None => return Err(SagaSchemaError::SectionRequired(KEY_STEP_ORDER)),
            },
            required_payloads: match tbl.get(KEY_REQUIRED_PAYLOADS) {
                None => vec![],
                Some(val) => to_vec_str(KEY_REQUIRED_PAYLOADS, val)?,
            },
            script_forward,
            max_retries: match tbl.get(KEY_MAX_RETRIES) {
                Some(v) => Some(to_usize(KEY_MAX_RETRIES, v)?),
                None => None,
            },
            retry_timeout: match tbl.get(KEY_RETRY_TIMEOUT) {
                Some(v) => Some(to_pos_f64(KEY_RETRY_TIMEOUT, v)?),
                None => None,
            },
            wait_timeout: match tbl.get(KEY_WAIT_TIMEOUT) {
                Some(v) => Some(to_pos_f64(KEY_WAIT_TIMEOUT, v)?),
                None => None,
            },
        };
        Ok(schema)
    }

    fn validate_steps(steps: &[SagaStepSchema]) -> SagaSchemaResult<()> {
        if steps.is_empty() {
            return Err(SagaSchemaError::SagaMustHaveAtLeastOneStep);
        }
        let mut can_rollback = steps[0].script_backward.is_some();
        let mut order = steps[0].step_order;
        for step in steps.iter().skip(1) {
            if step.step_order == order {
                return Err(SagaSchemaError::StepOrdersMustBeUnique);
            }
            order = step.step_order;
            let this_can_rollback = step.script_backward.is_some();
            if this_can_rollback && !can_rollback {
                return Err(SagaSchemaError::StepWithRollbackCanNotBeAfterStepWithNoRollback);
            }
            can_rollback = this_can_rollback;
        }
        Ok(())
    }
}

impl ISagaSchemaParser for SagaSchemaParser {
    fn parse_schema(&self, path: &Path, src: &str) -> SagaSchemaResult<SagaSchema> {
        let toml = src.parse::<Table>()?;
        let mut result = match toml.get(KEY_SAGA) {
            None => Self::de_general(&Table::default())?,
            Some(Value::Table(tbl)) => Self::de_general(tbl)?,
            Some(_) => {
                return Err(SagaSchemaError::InvalidSection {
                    section: KEY_SAGA,
                    expected_type: "table",
                })
            }
        };
        let steps = match toml.get(KEY_STEPS) {
            Some(Value::Table(tbl)) => tbl,
            None => return Err(SagaSchemaError::SagaMustHaveAtLeastOneStep),
            _ => {
                return Err(SagaSchemaError::InvalidSection {
                    section: KEY_STEPS,
                    expected_type: "table",
                })
            }
        };
        for (key, data) in steps {
            result.steps.push(Self::de_step(path, key, data)?);
        }
        result.steps.sort_by_key(|s| s.step_order);
        Self::validate_steps(&result.steps)?;
        Ok(result)
    }
}
