use crate::entities::errors::{SagaError, SagaResult};
use crate::impls::saga_executor::SagaExecutor;
use crate::proto::lua::ILuaPool;
use crate::proto::saga::{ISagaExecutorFactory, ISagaRepoPool, ISagaSchemaManager};

pub struct SagaExecutorFactory<
    R: ISagaRepoPool + 'static,
    SM: ISagaSchemaManager + Clone + 'static,
    L: ILuaPool + 'static,
> {
    pub repo_pool: R,
    pub schema_manager: SM,
    pub lua_pool: L,
}

impl<
        R: ISagaRepoPool + 'static,
        SM: ISagaSchemaManager + Clone + 'static,
        L: ILuaPool + 'static,
    > ISagaExecutorFactory for SagaExecutorFactory<R, SM, L>
{
    type Executor = SagaExecutor<R::Repo, SM, L::Executor>;

    fn produce(&self) -> SagaResult<Self::Executor> {
        let lua = match self.lua_pool.acquire_lua() {
            None => return Err(SagaError::CanNotAcquireLua),
            Some(val) => val,
        };
        let repo = match self.repo_pool.acquire_repo() {
            None => return Err(SagaError::CanNotAcquireRepo),
            Some(val) => val,
        };
        Ok(SagaExecutor::new(repo, self.schema_manager.clone(), lua))
    }
}
