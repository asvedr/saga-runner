use std::fs;
use std::io;
use std::io::{Read, Write};
use std::path::Path;

use crate::proto::common::IFSManager;

pub struct FSManager;

impl IFSManager for FSManager {
    fn read_text(&self, path: &Path) -> io::Result<String> {
        let mut file = fs::File::open(path)?;
        let mut buf = String::new();
        file.read_to_string(&mut buf)?;
        Ok(buf)
    }

    fn write_text(&self, path: &Path, text: &str) -> io::Result<()> {
        let mut file = fs::File::create(path)?;
        file.write_all(text.as_bytes())
    }

    fn list_dir(&self, path: &Path) -> io::Result<Vec<String>> {
        let mut result = Vec::new();
        for item_res in fs::read_dir(path)? {
            let item = item_res?;
            if let Some(txt) = item.file_name().to_str() {
                result.push(txt.to_string())
            }
        }
        Ok(result)
    }

    fn read_bytes(&self, path: &Path) -> io::Result<Vec<u8>> {
        let mut file = fs::File::open(path)?;
        let mut buf = Vec::new();
        file.read_to_end(&mut buf)?;
        Ok(buf)
    }

    fn write_bytes(&self, path: &Path, bts: &[u8]) -> std::io::Result<()> {
        let mut file = fs::File::create(path)?;
        file.write_all(bts)
    }
}
