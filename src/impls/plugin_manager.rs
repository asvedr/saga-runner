use std::path::Path;

use crate::entities::config::MainConfig;
use crate::entities::errors::PluginResult;
use crate::proto::common::IFSManager;
use crate::proto::lua::{IPlugin, IPluginLoader, IPluginManager};

const PLUGINS_DIR: &str = "plugins";

pub struct PluginManager<'a, 'b, 'c, FS: IFSManager, LPL: IPluginLoader, BPL: IPluginLoader> {
    pub fs: &'a FS,
    pub lua_loader: &'b LPL,
    pub bin_loader: &'c BPL,
    pub plugins: Vec<Box<dyn IPlugin>>,
}

enum FType {
    Lua,
    Bin,
    Dir,
    Unknown,
}

impl<'a, 'b, 'c, FS: IFSManager, LPL: IPluginLoader, BPL: IPluginLoader>
    PluginManager<'a, 'b, 'c, FS, LPL, BPL>
{
    pub fn new(
        fs: &'a FS,
        lua_loader: &'b LPL,
        bin_loader: &'c BPL,
        config: &MainConfig,
    ) -> PluginResult<Self> {
        let mut manager = Self {
            fs,
            lua_loader,
            bin_loader,
            plugins: vec![],
        };
        manager.try_root_include(&config.schema_root, &config.dylib_ext)?;
        for dir in config.include.iter() {
            manager.process_inc_dir(Path::new(&dir), "", &config.dylib_ext)?;
        }
        Ok(manager)
    }

    fn try_root_include(&mut self, src: &Path, dylib_exts: &[String]) -> PluginResult<()> {
        for subd in self.fs.list_dir(src)? {
            if subd == PLUGINS_DIR {
                return self.process_inc_dir(&src.join(subd), "", dylib_exts);
            }
        }
        Ok(())
    }

    fn process_inc_dir(
        &mut self,
        src: &Path,
        name: &str,
        dylib_exts: &[String],
    ) -> PluginResult<()> {
        let content = self.fs.list_dir(src)?;
        for item in content {
            let path = src.join(&item);
            // let new_name = format!("{}.{}", name, item.split('.').next().unwrap());
            let new_name = if name.is_empty() {
                item.split('.').next().unwrap().to_string()
            } else {
                format!("{}.{}", name, item.split('.').next().unwrap())
            };
            match self.filetype(&path, dylib_exts) {
                FType::Lua => self.load_lua(&path, &new_name)?,
                FType::Bin => self.load_bin(&path, &new_name)?,
                FType::Dir => self.process_inc_dir(&path, &new_name, dylib_exts)?,
                FType::Unknown => continue,
            }
        }
        Ok(())
    }

    fn filetype(&self, path: &Path, dylib_exts: &[String]) -> FType {
        if path.is_dir() {
            return if path.to_str().map(|s| !s.contains('.')).unwrap_or(false) {
                FType::Dir
            } else {
                FType::Unknown
            };
        }
        let ext = match path.extension() {
            Some(val) => match val.to_str() {
                Some(val) => val,
                _ => return FType::Unknown,
            },
            _ => return FType::Unknown,
        };
        if ext == "lua" {
            return FType::Lua;
        }
        for dylib_ext in dylib_exts {
            if ext == dylib_ext {
                return FType::Bin;
            }
        }
        FType::Unknown
    }

    fn load_lua(&mut self, path: &Path, name: &str) -> PluginResult<()> {
        let plugin = self.lua_loader.load_plugin(path.to_str().unwrap(), name)?;
        self.plugins.push(plugin);
        Ok(())
    }

    fn load_bin(&mut self, path: &Path, name: &str) -> PluginResult<()> {
        let plugin = self.bin_loader.load_plugin(path.to_str().unwrap(), name)?;
        self.plugins.push(plugin);
        Ok(())
    }
}

impl<'a, 'b, 'c, FS: IFSManager, LPL: IPluginLoader, BPL: IPluginLoader> IPluginManager
    for PluginManager<'a, 'b, 'c, FS, LPL, BPL>
{
    fn get_plugins(self) -> Vec<Box<dyn IPlugin>> {
        self.plugins
    }
}
