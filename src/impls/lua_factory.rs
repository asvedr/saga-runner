use std::collections::HashMap;
use std::mem;

use mlua::prelude::{LuaError, LuaResult};
use mlua::{Lua, MultiValue, Table};

use crate::entities::errors::{PluginError, PluginResult};
use crate::entities::plugins::MODULE_PREFIX;
use crate::impls::lua_executor::LuaExecutor;
use crate::proto::lua::{ILuaFactory, IPlugin, IStdMod};

#[derive(Default)]
pub struct LuaFactory {
    pub stdlib: Vec<Box<dyn IStdMod>>,
    pub plugins: Vec<Box<dyn IPlugin>>,
}

type TableFun<'a, 'b> = Box<dyn Fn(&'a Lua, String) -> LuaResult<Table<'b>>>;

fn f_load_plugin(
    plugins: &HashMap<String, Box<dyn IPlugin>>,
    lua_id: usize,
    lua: &Lua,
    name: &String,
) -> PluginResult<()> {
    let plugin = match plugins.get(name) {
        Some(val) => val,
        None => return Err(PluginError::NotFound),
    };
    plugin.init(lua, lua_id)
}

#[allow(clippy::extra_unused_lifetimes)]
fn make_load_plugin_clos<'a>(
    plugins: HashMap<String, Box<dyn IPlugin>>,
    lua_id: usize,
) -> TableFun<'static, 'static> {
    let clos = move |lua: &'a Lua, name: String| -> LuaResult<Table<'a>> {
        f_load_plugin(&plugins, lua_id, lua, &name).map_err(|err| match err {
            PluginError::LuaError(err) => err,
            PluginError::NotFound => LuaError::RuntimeError(format!("plugin not found: {}", name)),
            _ => unreachable!(),
        })?;
        let t = lua.globals().get(format!("{}{}", MODULE_PREFIX, name))?;
        Ok(t)
    };
    let f: TableFun<'a, 'a> = Box::new(clos);
    unsafe { mem::transmute(f) }
}

fn save_load_fun(lua: &Lua, fun: TableFun<'static, 'static>) {
    let static_lua: &'static Lua = unsafe { mem::transmute(lua) };
    let obj = static_lua.create_function(fun).unwrap();
    static_lua.globals().set("load", obj).unwrap();
}

fn save_mod_tbl<'lua>(
    lua: &'lua Lua,
    parent: &Table<'lua>,
    mod_tbl: Table<'lua>,
    path: &[&str],
    name: &str,
) -> LuaResult<()> {
    if path.is_empty() {
        return parent.set(name, mod_tbl);
    }
    let p_name = path[0];
    let opt_table: Option<Table> = parent.get(p_name)?;
    if let Some(tbl) = opt_table {
        return save_mod_tbl(lua, &tbl, mod_tbl, &path[1..], name);
    }
    let tbl = lua.create_table()?;
    save_mod_tbl(lua, &tbl, mod_tbl, &path[1..], name)?;
    parent.set(p_name, tbl)
}

impl LuaFactory {
    fn init_stdlib(&self, id: usize, lua: &Lua) -> LuaResult<()> {
        let std_tbl = lua.create_table()?;
        for module in self.stdlib.iter() {
            let mod_tbl = lua.create_table()?;
            module.init(id, lua, &mod_tbl)?;
            let path = module.path();
            let p_len = path.len();
            let name = path[p_len - 1];
            save_mod_tbl(lua, &std_tbl, mod_tbl, &path[..p_len - 1], name)?;
        }
        lua.globals().set("std", std_tbl)
    }
}

impl ILuaFactory for LuaFactory {
    type Executor = LuaExecutor;

    fn produce(&self, lua_id: usize) -> LuaExecutor {
        let lua = Lua::new();
        let plugin_map = self
            .plugins
            .iter()
            .map(|p| (p.name().to_string(), p.clone_box()))
            .collect::<HashMap<_, _>>();
        let load_plugin_clos = make_load_plugin_clos(plugin_map, lua_id);
        save_load_fun(&lua, load_plugin_clos);
        let noop = lua
            .create_function(|_: &Lua, _: MultiValue| Ok(()))
            .unwrap();
        lua.globals().set("noop", noop).unwrap();

        self.init_stdlib(lua_id, &lua).unwrap();

        LuaExecutor::from(lua)
    }
}
