use std::ffi::c_int;
use std::io;
use std::sync::atomic::AtomicBool;
use std::sync::Arc;

use signal_hook::consts;
use signal_hook::flag::register;

use crate::proto::common::ITermSignalHandler;

pub struct SignalHandler;

const EXIT_SIGNALS: &[c_int] = &[consts::SIGTERM, consts::SIGALRM, consts::SIGINT];

impl ITermSignalHandler for SignalHandler {
    fn bind_to_flag(&self, flag: Arc<AtomicBool>) -> io::Result<()> {
        for signal in EXIT_SIGNALS {
            register(*signal, flag.clone())?;
        }
        Ok(())
    }
}
