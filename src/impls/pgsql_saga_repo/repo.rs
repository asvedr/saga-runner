use std::cell::RefCell;
use std::mem::{replace, transmute};
use std::str::FromStr;

use chrono::Utc;
use postgres::types::ToSql;
use postgres::{Client, NoTls, Row, Transaction};

use crate::entities::errors::{SagaError, SagaResult};
use crate::entities::saga::{NewSagaStep, Saga, SagaId, SagaLog, SagaStatus, SagaStep};
use crate::impls::pgsql_saga_repo::tables::{self, Setter};
use crate::proto::saga::{ISagaRepo, ITransaction};
use crate::utils::lt::to_static;

// One thread - one repo
pub struct DBSagaRepo {
    inner: RefCell<Inner>,
    ready_statuses: String,
    saga_setters: Vec<(&'static str, Setter<Saga>)>,
    step_setters: Vec<(&'static str, Setter<SagaStep>)>,
}

struct Inner {
    client: Client,
    trx: Option<Transaction<'static>>,
}

impl Drop for Inner {
    fn drop(&mut self) {
        self.trx = None
    }
}

pub struct Trx {
    repo: Option<&'static DBSagaRepo>,
}

impl ITransaction for Trx {
    fn commit(&self) -> SagaResult<()> {
        let repo = match self.repo {
            None => return Ok(()),
            Some(val) => val,
        };
        let mut inner = repo.inner.borrow_mut();
        let trx = match replace(&mut inner.trx, None) {
            None => panic!("expected transaction in repo but found none"),
            Some(val) => val,
        };
        trx.commit()?;
        Ok(())
    }

    fn rollback(&self) -> SagaResult<()> {
        let repo = match self.repo {
            None => return Ok(()),
            Some(val) => val,
        };
        let mut inner = repo.inner.borrow_mut();
        let trx = match replace(&mut inner.trx, None) {
            None => panic!("expected transaction in repo but found none"),
            Some(val) => val,
        };
        trx.rollback()?;
        Ok(())
    }
}

impl Trx {
    fn new(repo: &DBSagaRepo) -> SagaResult<Trx> {
        let mut inner = repo.inner.borrow_mut();
        if inner.trx.is_some() {
            return Ok(Self { repo: None });
        }
        let trx = inner.client.transaction()?;
        inner.trx = Some(unsafe { transmute(trx) });
        Ok(Self {
            repo: Some(unsafe { transmute(repo) }),
        })
    }
}

impl Drop for Trx {
    fn drop(&mut self) {
        let repo = match self.repo {
            Some(val) => val,
            _ => return,
        };
        let mut inner = repo.inner.borrow_mut();
        if inner.trx.is_none() {
            panic!("insufficent transaction drop");
        }
        inner.trx = None;
    }
}

fn map_err_field<T, E: std::fmt::Debug>(res: Result<T, E>, fld: &str) -> SagaResult<T> {
    res.map_err(|err| SagaError::DBError(format!("invalid db field '{}': {:?}", fld, err)))
}

impl DBSagaRepo {
    pub fn new(connection_string: &str) -> SagaResult<Self> {
        let mut client = Client::connect(connection_string, NoTls)?;
        client.execute(tables::CREATE_SAGA, &[])?;
        client.execute(tables::CREATE_SAGA_STEP, &[])?;
        client.execute(tables::CREATE_SAGA_LOG, &[])?;
        let inner = RefCell::new(Inner { client, trx: None });
        let ready_statuses = SagaStatus::ACTIVE
            .iter()
            .map(|s| s.as_i32().to_string())
            .collect::<Vec<_>>()
            .join(",");
        Ok(Self {
            inner,
            ready_statuses,
            saga_setters: tables::saga_setters(),
            step_setters: tables::step_setters(),
        })
    }
}

impl DBSagaRepo {
    pub fn execute(&self, query: &str, params: &[&(dyn ToSql + Sync)]) -> SagaResult<()> {
        let mut inner = self.inner.borrow_mut();
        match inner.trx {
            Some(ref mut trx) => trx.execute(query, params)?,
            _ => inner.client.execute(query, params)?,
        };
        Ok(())
    }

    fn get_one(&self, query: &str, params: &[&(dyn ToSql + Sync)]) -> SagaResult<Row> {
        let mut inner = self.inner.borrow_mut();
        let row = match inner.trx {
            Some(ref mut trx) => trx.query_one(query, params)?,
            _ => inner.client.query_one(query, params)?,
        };
        Ok(row)
    }

    fn get_many(&self, query: &str, params: &[&(dyn ToSql + Sync)]) -> SagaResult<Vec<Row>> {
        let mut inner = self.inner.borrow_mut();
        let rows = match inner.trx {
            Some(ref mut trx) => trx.query(query, params)?,
            _ => inner.client.query(query, params)?,
        };
        Ok(rows)
    }

    fn step_inset_values<'a>(
        &self,
        saga_id: &'a String,
        now: &'a i64,
        step: &'a NewSagaStep,
        params: &mut Vec<&'a (dyn ToSql + Sync)>,
    ) -> String {
        params.push(saga_id);
        let sg_id = params.len();
        params.push(&step.name);
        let name_id = params.len();
        params.push(&step.step_order);
        let step_order_id = params.len();
        params.push(step.status.as_i32_ptr());
        let status_id = params.len();
        params.push(now);
        let now_id = params.len();
        format!(
            "(${sg_id}, ${nm}, ${stdr}, ${stts}, ${now})",
            sg_id = sg_id,
            nm = name_id,
            stdr = step_order_id,
            stts = status_id,
            now = now_id,
        )
    }

    #[inline]
    fn des_status(src: i32) -> SagaResult<SagaStatus> {
        SagaStatus::from_i32(src)
            .map_err(|_| SagaError::DBError(format!("invalid status: {}", src)))
    }

    #[inline]
    fn des_uuid(src: String) -> SagaResult<SagaId> {
        SagaId::from_str(&src).map_err(|_| SagaError::DBError("invalid saga id".to_string()))
    }

    #[inline]
    fn des_saga(opt_id: Option<&SagaId>, row: &Row) -> SagaResult<Saga> {
        let id = if let Some(id) = opt_id {
            *id
        } else {
            Self::des_uuid(map_err_field(row.try_get("id"), "id")?)?
        };
        Ok(Saga {
            id,
            name: map_err_field(row.try_get("name"), "name")?,
            payload: map_err_field(row.try_get("payload"), "payload")?,
            step_name: map_err_field(row.try_get("step_name"), "step_name")?,
            status: Self::des_status(map_err_field(row.try_get("status"), "status")?)?,
            retried: map_err_field(row.try_get::<_, i32>("retried"), "retried")? as u32,
            execute_after: map_err_field(row.try_get("execute_after"), "execute_after")?,
            created_at: map_err_field(row.try_get("created_at"), "created_at")?,
            updated_at: map_err_field(row.try_get("updated_at"), "updated_at")?,
        })
    }

    #[inline]
    fn des_step(saga_id: &SagaId, row: &Row) -> SagaResult<SagaStep> {
        Ok(SagaStep {
            id: map_err_field(row.try_get::<_, i32>("id"), "id")? as u64,
            payload: map_err_field(row.try_get("payload"), "payload")?,
            updated_at: map_err_field(row.try_get("updated_at"), "updated_at")?,
            saga_id: *saga_id,
            name: map_err_field(row.try_get("name"), "name")?,
            step_order: map_err_field(row.try_get::<_, i32>("step_order"), "step_order")? as u32,
            status: Self::des_status(map_err_field(row.try_get("status"), "status")?)?,
        })
    }
}

impl ISagaRepo for DBSagaRepo {
    type Trx = Trx;

    fn transaction(&self) -> SagaResult<Trx> {
        Trx::new(self)
    }

    fn create(&self, saga: Saga, steps: Vec<NewSagaStep>) -> SagaResult<()> {
        let query = r#"
            INSERT INTO saga
            (id, name, payload, step_name, status,
             retried, created_at, execute_after)
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
        "#;
        let retried = 0;
        self.execute(
            query,
            &[
                &saga.id.to_string(),
                &saga.name,
                &saga.payload,
                &saga.step_name,
                SagaStatus::InProgress.as_i32_ptr(),
                &retried,
                &saga.created_at,
                &saga.execute_after,
            ],
        )?;
        let mut params: Vec<&(dyn ToSql + Sync)> = Vec::new();
        let saga_id = saga.id.to_string();
        let query = r#"
            INSERT INTO saga_step
            (saga_id, name, step_order, status, updated_at)
            VALUES
        "#;
        let now = Utc::now().timestamp();
        for step in steps.iter() {
            params.clear();
            let q = format!(
                "{}{}",
                query,
                self.step_inset_values(&saga_id, &now, step, &mut params)
            );
            self.execute(&q, &params)?;
        }
        Ok(())
    }

    fn get_ready_ids(&self, limit: usize) -> SagaResult<Vec<SagaId>> {
        let query = format!(
            r#"
                SELECT id from saga
                WHERE status IN ({statuses})
                  AND `execute_after` >= {now}
                ORDER BY `created_at`
                LIMIT {limit}
            "#,
            statuses = self.ready_statuses,
            now = Utc::now().timestamp(),
            limit = limit,
        );
        let rows = self.get_many(&query, &[])?;
        let mut result = Vec::new();
        for row in rows {
            let id = Self::des_uuid(map_err_field(row.try_get("id"), "id")?)?;
            result.push(id);
        }
        Ok(result)
    }

    fn get_saga(&self, id: &SagaId) -> SagaResult<Saga> {
        let query = r#"
            SELECT name, payload, step_name, status,
                retried, execute_after, created_at, updated_at
            FROM saga
            WHERE id = $1
        "#;
        let row = self.get_one(query, &[&id.to_string()])?;
        Self::des_saga(Some(id), &row)
    }

    fn get_last_saga_of_name(&self, name: &str) -> SagaResult<Saga> {
        let query = r#"
            SELECT name, payload, step_name, status,
                retried, execute_after, created_at, updated_at
            FROM saga
            WHERE name = $1
            ORDER BY created_at DESC
            LIMIT 1
        "#;
        let row = self.get_one(query, &[&name])?;
        Self::des_saga(None, &row)
    }

    fn update_saga(&self, saga: &Saga, columns: &[&str]) -> SagaResult<()> {
        let mut param_query = format!("updated_at = $1");
        let now = Utc::now().timestamp();
        let mut params: Vec<&(dyn ToSql + Sync)> = vec![to_static(&now)];
        for (key, fun) in self.saga_setters.iter() {
            if columns.contains(key) {
                let text = fun(saga, &mut params);
                param_query.push(',');
                param_query.push_str(&text);
            }
        }
        let query = format!(
            "UPDATE saga SET {params} WHERE id = ${n}",
            params = param_query,
            n = params.len() + 1,
        );
        let id_str = saga.id.to_string();
        params.push(to_static(&id_str));
        self.execute(&query, &params)?;
        Ok(())
    }

    fn get_count_of_status(&self, statuses: &[SagaStatus]) -> SagaResult<usize> {
        let mut template = Vec::new();
        let mut params: Vec<&(dyn ToSql + Sync)> = Vec::new();
        for status in statuses {
            params.push(status.as_i32_ptr());
            let len = template.len();
            template.push(format!("${}", len));
        }
        let query = format!(
            "SELECT COUNT(*) FROM saga WHERE status IN ({tl})",
            tl = template.join(","),
        );
        let row = self.get_one(&query, &params)?;
        let res = row.get::<_, i64>(0);
        Ok(res as usize)
    }

    fn get_saga_steps(&self, saga_id: &SagaId, keys: &[&str]) -> SagaResult<Vec<SagaStep>> {
        let q_keys = keys
            .iter()
            .enumerate()
            .map(|(i, _)| format!("${}", i + 2))
            .collect::<Vec<_>>()
            .join(",");
        let query = format!(
            r#"
                SELECT
                    id, payload, updated_at,
                    name, step_order, status
                FROM saga_step
                WHERE saga_id = $1 AND name IN ({keys})
                ORDER BY step_order
            "#,
            keys = q_keys,
        );
        let mut params = vec![saga_id.to_string()];
        for key in keys {
            params.push(key.to_string());
        }
        let l_params = params
            .iter()
            .map(|s| -> &(dyn ToSql + Sync) { s })
            .collect::<Vec<_>>();
        let rows = self.get_many(&query, &l_params)?;
        let mut steps = Vec::new();
        for row in rows {
            steps.push(Self::des_step(saga_id, &row)?);
        }
        Ok(steps)
    }

    fn get_all_saga_steps(&self, saga_id: &SagaId) -> SagaResult<Vec<SagaStep>> {
        let query = r#"
            SELECT
                id, payload, updated_at,
                name, step_order, status
            FROM saga_step
            WHERE saga_id = $1
            ORDER BY step_order
        "#;
        let saga_id_str = saga_id.to_string();
        let rows = self.get_many(query, &[&saga_id_str])?;
        let mut steps = Vec::new();
        for row in rows {
            steps.push(Self::des_step(saga_id, &row)?);
        }
        Ok(steps)
    }

    fn update_step(&self, step: &SagaStep, columns: &[&str]) -> SagaResult<()> {
        let mut param_query = format!("updated_at = {}", Utc::now().timestamp());
        let mut params = Vec::new();
        for (key, fun) in self.step_setters.iter() {
            if columns.contains(key) {
                param_query.push(',');
                param_query.push_str(&fun(step, &mut params));
            }
        }
        let query = format!(
            "UPDATE saga_step SET {pq} WHERE id = {id}",
            pq = param_query,
            id = step.id,
        );
        self.execute(&query, &params)?;
        Ok(())
    }

    fn log(&self, log: SagaLog) -> SagaResult<()> {
        let query = r#"
            INSERT INTO saga_log
            (saga_id, step_name, timestamp, status, error)
            VALUES
            ($1, $2, $3, $4, $5)
        "#;
        let saga_id = log.saga_id.to_string();
        let params: &[&(dyn ToSql + Sync)] = &[
            &saga_id,
            &log.step_name,
            &log.timestamp,
            log.status.as_i32_ptr(),
            &log.error,
        ];
        self.execute(query, params)
    }

    fn get_last_errors(&self, min_ts: i64, limit: usize) -> SagaResult<Vec<SagaLog>> {
        let query = r#"
            SELECT
                saga_id, step_name,
                "timestamp", status, "error"
            FROM saga_log
            WHERE NOT ("error" IS NULL) AND "timestamp" >= $1
            ORDER BY "timestamp" DESC
            LIMIT $2
        "#;
        let limit = limit as i64;
        let rows = self.get_many(query, &[&min_ts, &limit])?;
        let mut logs = Vec::new();
        for row in rows {
            let log = SagaLog {
                saga_id: Self::des_uuid(row.try_get("saga_id")?)?,
                step_name: row.try_get("step_name")?,
                timestamp: row.try_get("timestamp")?,
                status: Self::des_status(row.try_get("status")?)?,
                error: row.try_get("error")?,
            };
            logs.push(log);
        }
        Ok(logs)
    }
}
