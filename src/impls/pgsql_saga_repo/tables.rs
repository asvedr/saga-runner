use crate::entities::saga::{Saga, SagaStep};
use crate::utils::lt::to_static;
use postgres::types::ToSql;

pub const CREATE_SAGA: &str = r#"
    CREATE TABLE IF NOT EXISTS saga (
        id VARCHAR(36) PRIMARY KEY,
        name VARCHAR,
        payload VARCHAR,
        step_name VARCHAR,
        "status" INTEGER NOT NULL,
        retried INTEGER NOT NULL,
        execute_after BIGINT,
        created_at BIGINT NOT NULL,
        updated_at BIGINT NOT NULL
    )
"#;

pub const CREATE_SAGA_STEP: &str = r#"
    CREATE TABLE IF NOT EXISTS saga_step (
        id BIGSERIAL PRIMARY KEY,
        payload VARCHAR,
        updated_at BIGINT NOT NULL,
        saga_id VARCHAR(36) NOT NULL,
        name VARCHAR NOT NULL,
        step_order INTEGER,
        "status" INTEGER,
        CONSTRAINT fk_saga_step_saga_id
            FOREIGN KEY(saga_id)
	        REFERENCES saga(id)
	        ON DELETE CASCADE
    )
"#;

pub const CREATE_SAGA_LOG: &str = r#"
    CREATE TABLE IF NOT EXISTS saga_log (
        id SERIAL PRIMARY KEY,
        saga_id VARCHAR(36) NOT NULL,
        step_name VARCHAR NOT NULL,
        "timestamp" BIGINT NOT NULL,
        "status" INTEGER,
        error VARCHAR,
        CONSTRAINT fk_saga_log_saga_id
            FOREIGN KEY(saga_id)
	        REFERENCES saga(id)
	        ON DELETE CASCADE
    )
"#;

pub type Setter<T> = Box<dyn Fn(&T, &mut Vec<&'static (dyn ToSql + Sync)>) -> String>;

pub fn saga_setters() -> Vec<(&'static str, Setter<Saga>)> {
    vec![
        (
            "name",
            Box::new(|s, p| {
                p.push(to_static(&s.name));
                format!("name = ${}", p.len())
            }),
        ),
        (
            "payload",
            Box::new(|s, p| {
                p.push(to_static(&s.payload));
                format!("payload = ${}", p.len())
            }),
        ),
        (
            "step_name",
            Box::new(|s, p| {
                p.push(to_static(&s.step_name));
                format!("step_name = ${}", p.len())
            }),
        ),
        (
            "status",
            Box::new(|s, p| {
                p.push(to_static(&s.status.as_i32_ptr()));
                format!("status = ${}", p.len())
            }),
        ),
        (
            "retried",
            Box::new(|s, p| {
                p.push(to_static(&s.retried));
                format!("retried = ${}", p.len())
            }),
        ),
        (
            "execute_after",
            Box::new(|s, p| {
                p.push(to_static(&s.execute_after));
                format!("execute_after = ${}", p.len())
            }),
        ),
        (
            "created_at",
            Box::new(|s, p| {
                p.push(to_static(&s.created_at));
                format!("created_at = ${}", p.len())
            }),
        ),
    ]
}

pub fn step_setters() -> Vec<(&'static str, Setter<SagaStep>)> {
    vec![
        (
            "payload",
            Box::new(|s, p| {
                p.push(to_static(&s.payload));
                format!("payload = ${}", p.len())
            }),
        ),
        (
            "name",
            Box::new(|s, p| {
                p.push(to_static(&s.name));
                format!("name = ${}", p.len())
            }),
        ),
        (
            "step_order",
            Box::new(|s, p| {
                p.push(to_static(&s.step_order));
                format!("step_order = ${}", p.len())
            }),
        ),
        (
            "status",
            Box::new(|s, p| {
                p.push(to_static(s.status.as_i32_ptr()));
                format!("status = ${}", p.len())
            }),
        ),
    ]
}
