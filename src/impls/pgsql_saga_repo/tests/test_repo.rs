use crate::entities::errors::SagaError;
use crate::entities::saga::{NewSagaStep, Saga, SagaStatus};
use std::env;
use uuid::Uuid;

use crate::impls::pgsql_saga_repo::repo::DBSagaRepo;
use crate::proto::saga::ISagaRepo;

fn make_repo() -> DBSagaRepo {
    let cnn = format!(
        "host={h} port={po} user={u} password={pa} dbname={db}",
        h = env::var("POSTGRES_HOST").unwrap(),
        u = env::var("POSTGRES_USER").unwrap(),
        po = env::var("POSTGRES_PORT").unwrap(),
        pa = env::var("POSTGRES_PASSWORD").unwrap(),
        db = env::var("POSTGRES_DB").unwrap(),
    );
    DBSagaRepo::new(&cnn).unwrap()
}

fn make_saga_and_steps() -> (Saga, Vec<NewSagaStep>) {
    let saga = Saga {
        id: Uuid::new_v4(),
        name: "a".to_string(),
        step_name: "b".to_string(),
        ..Default::default()
    };
    let step_b = NewSagaStep {
        name: "b".to_string(),
        step_order: 0,
        status: Default::default(),
    };
    let step_c = NewSagaStep {
        name: "c".to_string(),
        step_order: 1,
        status: Default::default(),
    };
    (saga, vec![step_b, step_c])
}

#[test]
fn test_not_found() {
    let repo = make_repo();
    // repo.execute("DROP TABLE saga_log", &[]).unwrap();
    // repo.execute("DROP TABLE saga_step", &[]).unwrap();
    // repo.execute("DROP TABLE saga", &[]).unwrap();
    assert_eq!(repo.get_saga(&Uuid::new_v4()), Err(SagaError::NotFound))
}

#[test]
fn test_got_one() {
    let repo = make_repo();
    let (mut saga, steps) = make_saga_and_steps();
    repo.create(saga.clone(), steps).unwrap();
    let got = repo.get_saga(&saga.id).unwrap();
    saga.status = SagaStatus::InProgress;
    saga.execute_after = got.execute_after.clone();
    saga.created_at = got.created_at.clone();
    assert_eq!(got, saga);
}

#[test]
fn test_update_saga() {
    let repo = make_repo();
    let (mut saga, steps) = make_saga_and_steps();
    repo.create(saga.clone(), steps).unwrap();
    saga.name = "xxx".to_string();
    saga.payload = Some("eee".to_string());
    repo.update_saga(&saga, &["name"]).unwrap();
    let got = repo.get_saga(&saga.id).unwrap();
    assert_eq!(got.name, "xxx");
    assert_eq!(got.payload, None);
}

#[test]
fn test_get_saga_steps() {
    let repo = make_repo();
    let (saga, steps) = make_saga_and_steps();
    repo.create(saga.clone(), steps.clone()).unwrap();
    let steps = repo.get_saga_steps(&saga.id, &["b"]).unwrap();
    assert_eq!(steps.len(), 1);
    assert_eq!(steps[0].name, "b");
    let steps = repo.get_saga_steps(&saga.id, &["c", "b"]).unwrap();
    assert_eq!(steps.len(), 2);
    assert_eq!(steps[0].name, "b");
    assert_eq!(steps[1].name, "c");
}

#[test]
fn test_get_all_saga_steps() {
    let repo = make_repo();
    let (saga, steps) = make_saga_and_steps();
    repo.create(saga.clone(), steps.clone()).unwrap();
    let steps = repo.get_all_saga_steps(&saga.id).unwrap();
    assert_eq!(steps.len(), 2);
    assert_eq!(steps[0].name, "b");
    assert_eq!(steps[1].name, "c");
}

#[test]
fn test_update_saga_step() {
    let repo = make_repo();
    let (saga, steps) = make_saga_and_steps();
    repo.create(saga.clone(), steps.clone()).unwrap();
    let mut steps = repo.get_saga_steps(&saga.id, &["b"]).unwrap();
    assert_eq!(steps[0].name, "b");
    assert_eq!(steps[0].payload, None);
    steps[0].payload = Some("abc".to_string());
    repo.update_step(&steps[0], &["payload"]).unwrap();
    let steps = repo.get_saga_steps(&saga.id, &["b"]).unwrap();
    assert_eq!(steps[0].name, "b");
    assert_eq!(steps[0].payload, Some("abc".to_string()));
}
