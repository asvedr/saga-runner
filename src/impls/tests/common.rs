use std::path::PathBuf;

use crate::entities::config::MainConfig;
use crate::impls::fs_manager::FSManager;
use crate::impls::lua_executor::LuaExecutor;
use crate::impls::lua_factory::LuaFactory;
use crate::impls::saga_executor::SagaExecutor;
use crate::impls::saga_schema_manager::SagaSchemaManager;
use crate::impls::saga_schema_parser::SagaSchemaParser;
use crate::impls::sqlite_saga_repo::repo::DBSagaRepo;
use crate::impls::stdlib::all_mods;
use crate::proto::lua::ILuaFactory;

#[allow(dead_code)]
pub struct Ctx {
    pub db_repo: Box<DBSagaRepo>,
    // pub fs_man: Box<FSManager>,
    // pub parser: Box<SagaSchemaParser>,
    // pub schema_man: Box<SagaSchemaManager<FSManager, SagaSchemaParser>>,
    // pub lua: Box<LuaExecutor>,
    pub executor: SagaExecutor<
        &'static DBSagaRepo,
        SagaSchemaManager<FSManager, SagaSchemaParser>,
        LuaExecutor,
    >,
}

fn to_s<T>(link: &T) -> &'static T {
    unsafe { std::mem::transmute(link) }
}

impl Ctx {
    pub fn new() -> Self {
        use mddd::traits::IConfig;

        let mut config = MainConfig::build().unwrap();
        config.schema_root = PathBuf::from("test_data/saga_root");
        config.include = Vec::new();
        config.max_threads = 1;
        let db_repo = Box::new(DBSagaRepo::new(":memory:").unwrap());
        let fs_man = FSManager;
        let parser = SagaSchemaParser;
        let schema_man = SagaSchemaManager::new(fs_man, parser, &config).unwrap();
        let lua_factory = LuaFactory {
            stdlib: all_mods(&config),
            ..Default::default()
        };
        let lua = lua_factory.produce(1);
        let executor = SagaExecutor::new(to_s(&*db_repo), schema_man, lua);
        Self {
            db_repo,
            // fs_man,
            // parser,
            // schema_man,
            // lua,
            executor,
        }
    }
}
