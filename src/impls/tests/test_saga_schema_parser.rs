use std::fs;
use std::io::Read;
use std::path::{Path, PathBuf};

use crate::entities::errors::SagaSchemaError;
use crate::entities::saga::{
    SagaSchema, SagaStepSchema, DEFAULT_MAX_RETRIES, DEFAULT_RETRY_TIMEOUT, DEFAULT_WAIT_TIMEOUT,
};
use crate::impls::saga_schema_parser::SagaSchemaParser;
use crate::proto::saga::ISagaSchemaParser;

fn make_parser() -> SagaSchemaParser {
    SagaSchemaParser
}

const DIR: &str = "test_data/saga_config";

fn source(name: &str) -> String {
    let mut src = fs::File::open(format!("{}/{}.toml", DIR, name)).unwrap();
    let mut buf = String::new();
    src.read_to_string(&mut buf).unwrap();
    buf
}

#[test]
fn test_min_schema() {
    let expected = SagaSchema {
        max_retries: DEFAULT_MAX_RETRIES,
        retry_timeout: DEFAULT_RETRY_TIMEOUT,
        wait_timeout: DEFAULT_WAIT_TIMEOUT,
        steps: vec![
            SagaStepSchema {
                name: "get_values".to_string(),
                step_order: 0,
                script_forward: "print('hoho')".to_string(),
                ..Default::default()
            },
            SagaStepSchema {
                name: "calculate".to_string(),
                step_order: 1,
                script_forward: "print('calc')".to_string(),
                ..Default::default()
            },
            SagaStepSchema {
                name: "send_results".to_string(),
                step_order: 2,
                script_forward: "print('send')".to_string(),
                ..Default::default()
            },
        ],
        periodic: None,
    };
    let got = make_parser().parse_schema(Path::new(DIR), &source("min"));
    assert_eq!(got, Ok(expected))
}

#[test]
fn test_max_schema() {
    let expected = SagaSchema {
        max_retries: 1,
        retry_timeout: 2.5,
        wait_timeout: 3.0,
        steps: vec![SagaStepSchema {
            name: "get_values".to_string(),
            script_backward: Some("print('send')".to_string()),
            step_order: 2,
            required_payloads: vec!["a".to_string(), "b".to_string(), "c".to_string()],
            script_forward: "print('get')".to_string(),
            max_retries: Some(10),
            retry_timeout: Some(11.0),
            wait_timeout: Some(12.0),
        }],
        periodic: None,
    };
    let got = make_parser().parse_schema(Path::new(DIR), &source("max"));
    assert_eq!(got, Ok(expected))
}

#[test]
fn test_empty_steps() {
    let got = make_parser().parse_schema(Path::new(DIR), &source("empty_steps"));
    assert_eq!(got, Err(SagaSchemaError::SagaMustHaveAtLeastOneStep),)
}

#[test]
fn test_non_uniq_order() {
    let got = make_parser().parse_schema(Path::new(DIR), &source("non_uniq_order"));
    assert_eq!(got, Err(SagaSchemaError::StepOrdersMustBeUnique))
}

#[test]
fn test_rollback_after_retry() {
    let got = make_parser().parse_schema(Path::new(DIR), &source("rollback_after_retry"));
    assert_eq!(
        got,
        Err(SagaSchemaError::StepWithRollbackCanNotBeAfterStepWithNoRollback)
    )
}
