use crate::entities::saga::SagaId;
use crate::entities::saga::SagaStatus as SS;
use crate::impls::tests::common::Ctx;
use crate::proto::saga::{ISagaExecutor, ISagaRepo};

fn prepare(err_t: &str, err_d: &str) -> (Ctx, SagaId) {
    let ctx = Ctx::new();
    let js = format!(
        "{}\"err_type\": \"{}\", \"err_data\": {}{}",
        '{', err_t, err_d, '}'
    );
    let saga_id = ctx
        .executor
        .create_saga("type_c".to_string(), Some(js))
        .unwrap();
    (ctx, saga_id)
}

fn update_payload(ctx: &Ctx, saga_id: &SagaId, err_t: &str, err_d: &str) {
    let mut saga = ctx.db_repo.get_saga(saga_id).unwrap();
    let js = format!(
        "{}\"err_type\": \"{}\", \"err_data\": {}{}",
        '{', err_t, err_d, '}'
    );
    saga.payload = Some(js);
    ctx.db_repo.update_saga(&saga, &["payload"]).unwrap()
}

macro_rules! validate_statuses {
    (
        $ctx:expr, $saga_id:expr,
        $saga_status:expr, $active_step:expr,
        $saga_retried:expr, $step_statuses:expr
    ) => {{
        let saga = $ctx.db_repo.get_saga(&$saga_id).unwrap();
        assert_eq!(saga.status, $saga_status);
        assert_eq!(saga.retried, $saga_retried);
        assert_eq!(saga.step_name, $active_step);
        let steps = $ctx
            .db_repo
            .get_all_saga_steps(&$saga_id)
            .unwrap()
            .into_iter()
            .map(|s| s.status)
            .collect::<Vec<_>>();
        assert_eq!(steps, $step_statuses);
    }};
}

#[test]
fn test_success_rollback_after_term_err() {
    let (ctx, saga_id) = prepare("", "null");
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step1",
        0,
        &[SS::InProgress, SS::InProgress]
    );
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step2",
        0,
        &[SS::Completed, SS::InProgress]
    );
    update_payload(&ctx, &saga_id, "T", "{}");
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::RollingBack,
        "step1",
        0,
        &[SS::Completed, SS::RolledBack]
    );
    update_payload(&ctx, &saga_id, "", "{}");
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::RolledBack,
        "step1",
        0,
        &[SS::RolledBack, SS::RolledBack]
    );
}

#[test]
fn test_break_on_rollback_after_term_err() {
    let (ctx, saga_id) = prepare("", "null");
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step1",
        0,
        &[SS::InProgress, SS::InProgress]
    );
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step2",
        0,
        &[SS::Completed, SS::InProgress]
    );
    update_payload(&ctx, &saga_id, "T", "{}");
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::RollingBack,
        "step1",
        0,
        &[SS::Completed, SS::RolledBack]
    );
    update_payload(&ctx, &saga_id, "T", "{}");
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::Broken,
        "step1",
        0,
        &[SS::Broken, SS::RolledBack]
    );
}

#[test]
fn test_start_rollback_after_retry() {
    let (ctx, saga_id) = prepare("", "null");
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step1",
        0,
        &[SS::InProgress, SS::InProgress]
    );
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step2",
        0,
        &[SS::Completed, SS::InProgress]
    );
    update_payload(&ctx, &saga_id, "R", "{}");
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step2",
        1,
        &[SS::Completed, SS::InProgress]
    );
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::RollingBack,
        "step1",
        0,
        &[SS::Completed, SS::RolledBack]
    );
}

#[test]
fn test_break_on_rollback_after_retry() {
    let (ctx, saga_id) = prepare("", "null");
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step1",
        0,
        &[SS::InProgress, SS::InProgress]
    );
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step2",
        0,
        &[SS::Completed, SS::InProgress]
    );
    update_payload(&ctx, &saga_id, "R", "{}");
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step2",
        1,
        &[SS::Completed, SS::InProgress]
    );
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::RollingBack,
        "step1",
        0,
        &[SS::Completed, SS::RolledBack]
    );
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::RollingBack,
        "step1",
        1,
        &[SS::Completed, SS::RolledBack]
    );
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::Broken,
        "step1",
        1,
        &[SS::Broken, SS::RolledBack]
    );
}

#[test]
fn test_reset_retry_counter_after_need_to_wait() {
    let (ctx, saga_id) = prepare("", "null");
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step1",
        0,
        &[SS::InProgress, SS::InProgress]
    );
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step2",
        0,
        &[SS::Completed, SS::InProgress]
    );
    update_payload(&ctx, &saga_id, "R", "{}");
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step2",
        1,
        &[SS::Completed, SS::InProgress]
    );
    update_payload(&ctx, &saga_id, "N", "{}");
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step2",
        0,
        &[SS::Completed, SS::InProgress]
    );
}

#[test]
fn test_reset_retry_counter_after_move_forward() {
    let (ctx, saga_id) = prepare("R", "null");
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step1",
        0,
        &[SS::InProgress, SS::InProgress]
    );
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step1",
        1,
        &[SS::InProgress, SS::InProgress]
    );
    update_payload(&ctx, &saga_id, "", "{}");
    ctx.executor.execute_saga(&saga_id).unwrap();
    validate_statuses!(
        ctx,
        saga_id,
        SS::InProgress,
        "step2",
        0,
        &[SS::Completed, SS::InProgress]
    );
}
