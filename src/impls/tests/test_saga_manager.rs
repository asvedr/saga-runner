use mddd::traits::IConfig;
use std::path::PathBuf;

use crate::entities::config::MainConfig;
use crate::impls::fs_manager::FSManager;
use crate::impls::saga_schema_manager::SagaSchemaManager;
use crate::impls::saga_schema_parser::SagaSchemaParser;
use crate::proto::saga::ISagaSchemaManager;

#[test]
fn test_build_saga() {
    let fs = FSManager;
    let p = SagaSchemaParser;
    let mut config = MainConfig::build().unwrap();
    config.schema_root = PathBuf::from("test_data/saga_root");
    config.include = vec![];
    let man = SagaSchemaManager::new(fs, p, &config).unwrap();
    let mut schemas = man.available_schemas();
    schemas.sort();
    assert_eq!(schemas, ["type_a", "type_b", "type_c"]);
    let schema = man.get_schema("type_b").unwrap();
    assert_eq!(schema.steps.len(), 2);
    assert_eq!(schema.steps[0].script_forward, "print('step 1')");
    assert_eq!(
        schema.steps[1].script_forward,
        "print('step 2')\nresult = {a=1, b=2}"
    );
    let schema = man.get_schema("type_a").unwrap();
    assert_eq!(schema.steps.len(), 1);
    assert_eq!(
        schema.steps[0].script_backward,
        Some("print('bye bye world')".to_string())
    );
}
