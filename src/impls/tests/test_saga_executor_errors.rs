use crate::entities::errors::LuaCustomError as LCE;
use crate::entities::errors::SagaError as SE;
use crate::entities::errors::SagaResult;
use crate::entities::saga::SagaId;
use crate::impls::tests::common::Ctx;
use crate::proto::saga::{ISagaExecutor, ISagaRepo};

fn get_result(ctx: Ctx, id: SagaId) -> SagaResult<()> {
    let saga = ctx.db_repo.get_saga(&id)?;
    let (_, step_schema) = ctx.executor.get_schema(&saga)?;
    let mut steps = ctx.executor.get_saga_steps(&saga, step_schema)?;
    let active_step = steps.pop().unwrap();
    let (name, code) = ctx
        .executor
        .prepare_lua(&saga, &steps, &active_step, step_schema)?;
    ctx.executor.execute_script(&name, code)?;
    Ok(())
}

fn test_body(js: &str, expected_error: SE) {
    let ctx = Ctx::new();
    let saga_id = ctx
        .executor
        .create_saga("type_c".to_string(), Some(js.to_string()))
        .unwrap();
    assert_eq!(get_result(ctx, saga_id), Err(expected_error))
}

#[test]
fn test_no_raise() {
    let ctx = Ctx::new();
    let saga_id = ctx
        .executor
        .create_saga("type_c".to_string(), Some("{}".to_string()))
        .unwrap();
    assert!(get_result(ctx, saga_id).is_ok())
}

#[test]
fn test_raise_ntw_no_params() {
    let js = r#"{"err_type": "N"}"#;
    test_body(js, SE::LuaCustomError(LCE::NeedToWait(None)))
}

#[test]
fn test_raise_ntw_no_data() {
    let js = r#"{"err_type": "N", "err_data": {}}"#;
    test_body(js, SE::LuaCustomError(LCE::NeedToWait(None)))
}

#[test]
fn test_raise_ntw_timeout() {
    let js = r#"{"err_type": "N", "err_data": {"timeout": 5}}"#;
    test_body(js, SE::LuaCustomError(LCE::NeedToWait(Some(5.0))))
}

#[test]
fn test_raise_retry_no_params() {
    let js = r#"{"err_type": "R"}"#;
    test_body(js, SE::LuaCustomError(LCE::Retry(None, None)))
}

#[test]
fn test_raise_retry_no_data() {
    let js = r#"{"err_type": "R", "err_data": {}}"#;
    test_body(js, SE::LuaCustomError(LCE::Retry(None, None)))
}

#[test]
fn test_raise_retry_only_msg() {
    let js = r#"{"err_type": "R", "err_data": {"msg": "a"}}"#;
    test_body(
        js,
        SE::LuaCustomError(LCE::Retry(Some("a".to_string()), None)),
    )
}

#[test]
fn test_raise_retry_only_tm() {
    let js = r#"{"err_type": "R", "err_data": {"timeout": 1}}"#;
    test_body(js, SE::LuaCustomError(LCE::Retry(None, Some(1.0))))
}

#[test]
fn test_raise_retry_full_data() {
    let js = r#"{"err_type": "R", "err_data": {"msg": "a", "timeout": 1}}"#;
    let lce = LCE::Retry(Some("a".to_string()), Some(1.0));
    test_body(js, SE::LuaCustomError(lce))
}

#[test]
fn test_raise_term_no_params() {
    let js = r#"{"err_type": "T"}"#;
    test_body(js, SE::LuaCustomError(LCE::Terminal(None)))
}

#[test]
fn test_raise_term_no_data() {
    let js = r#"{"err_type": "T", "err_data": {}}"#;
    test_body(js, SE::LuaCustomError(LCE::Terminal(None)))
}

#[test]
fn test_raise_term_msg() {
    let js = r#"{"err_type": "T", "err_data": {"msg": "5"}}"#;
    test_body(js, SE::LuaCustomError(LCE::Terminal(Some("5".to_string()))))
}
