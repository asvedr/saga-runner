use std::env::var;

use mddd::traits::IConfig;
use mlua::prelude::LuaResult;
use mlua::{Lua, Table};

use crate::entities::config::MainConfig;
use crate::impls::lua_factory::LuaFactory;
use crate::impls::plugin_loader::dylib;
use crate::impls::plugin_loader::lua;
use crate::impls::stdlib::all_mods;
use crate::proto::lua::{ILuaExecutor, IPlugin, IStdMod};
use crate::proto::lua::{ILuaFactory, IPluginLoader};
use crate::utils::lua::format_lua_error;

fn factory(plugins: Vec<Box<dyn IPlugin>>) -> LuaFactory {
    LuaFactory {
        stdlib: vec![],
        plugins,
    }
}

#[test]
fn test_plain_lua() {
    let lua = factory(vec![]).produce(1);
    let val = lua
        .exec_n_get_js("code", "result = 1 + 2", "result")
        .unwrap();
    assert_eq!(val.to_string(), "3")
}

#[test]
fn test_text_plugin() {
    let plugin = lua::PluginLoader
        .load_plugin("test_data/math_module.lua", "math")
        .unwrap();
    let lua = factory(vec![plugin]).produce(1);
    let val = lua
        .exec_n_get_js("code", "m = load('math'); result = m.gcd(6, 10)", "result")
        .unwrap();
    assert_eq!(val.to_string(), "2")
}

#[test]
fn test_invalid_text_plugin() {
    let plugin = lua::PluginLoader
        .load_plugin("test_data/invalid_syn_module.lua", "im")
        .unwrap();
    let lua = factory(vec![plugin]).produce(1);
    let res = lua.exec("code", "im = load('im')");
    let err = match res {
        Err(err) => format_lua_error(&err),
        _ => panic!(),
    };
    let expected = concat!(
        "-- CallbackError --:\n",
        "test_data/invalid_syn_module.lua:1: unexpected symbol near '{'\n",
    );
    assert_eq!(err, expected)
}

#[test]
fn test_runtime_error_on_load() {
    let plugin = lua::PluginLoader
        .load_plugin("test_data/runtime_err_on_load_module.lua", "im")
        .unwrap();
    let lua = factory(vec![plugin]).produce(1);
    let err = match lua.exec("code", "im = load('im')") {
        Ok(_) => panic!(),
        Err(err) => format_lua_error(&err),
    };
    let expected = concat!(
        "-- CallbackError --:\n",
        "test_data/runtime_err_on_load_module.lua:6: attempt to index a nil value (local 'x')\n",
        "stack traceback:\n",
        "\t[C]: in metamethod 'index'\n",
        "\ttest_data/runtime_err_on_load_module.lua:6\n",
        "\t[C]: in function 'load'\n",
        "\tcode:1: in main chunk\n"
    );
    assert_eq!(err, expected)
}

#[test]
fn test_depends() {
    let plugin_a = lua::PluginLoader
        .load_plugin("test_data/depends_on_math.lua", "dm")
        .unwrap();
    let plugin_b = lua::PluginLoader
        .load_plugin("test_data/math_module.lua", "math")
        .unwrap();
    let lua = factory(vec![plugin_a, plugin_b]).produce(1);
    let val = lua
        .exec_n_get_js("code", "dm = load('dm'); result = dm.gte(1, 2)", "result")
        .unwrap();
    assert_eq!(val.to_string(), "false");
}

#[test]
fn test_bin_math() {
    let loader = dylib::PluginLoader::default();
    let path = var("PLUGIN_MATH").unwrap();
    let plugin = loader.load_plugin(&path, "math").unwrap();
    let lua = factory(vec![plugin]).produce(1);
    let val = lua
        .exec_n_get_js("code", "m = load('math'); result = m.add(3,4)", "result")
        .unwrap();
    assert_eq!(val.to_string(), "7");
}

#[test]
fn test_bin_str() {
    let loader = dylib::PluginLoader::default();
    let path = var("PLUGIN_STR").unwrap();
    let plugin = loader.load_plugin(&path, "str").unwrap();
    let lua = factory(vec![plugin]).produce(1);
    let val = lua
        .exec_n_get_js("code", "m = load('str'); result = m.add('a','b')", "result")
        .unwrap();
    assert_eq!(val.to_string(), "ab");
}

#[test]
fn test_bin_str_err() {
    let loader = dylib::PluginLoader::default();
    let path = var("PLUGIN_STR").unwrap();
    let plugin = loader.load_plugin(&path, "str").unwrap();
    let lua = factory(vec![plugin]).produce(1);
    let err = lua
        .exec("code", "m = load('str'); m.raise_err()")
        .unwrap_err();
    let expected = concat!("-- CallbackError --:\n", "oh no!\n",);
    assert_eq!(format_lua_error(&err), expected);
}

#[test]
fn test_multimodule() {
    let plugin_a = lua::PluginLoader
        .load_plugin("test_data/math_module.lua", "lm")
        .unwrap();
    let loader = dylib::PluginLoader::default();
    let plugin_b = loader
        .load_plugin(&var("PLUGIN_MATH").unwrap(), "bm")
        .unwrap();
    let plugin_c = loader
        .load_plugin(&var("PLUGIN_STR").unwrap(), "bs")
        .unwrap();
    let lua = factory(vec![plugin_a, plugin_b, plugin_c]).produce(1);
    let res = lua
        .exec_n_get_js(
            "code",
            r#"
            lm = load('lm')
            bm = load('bm')
            bs = load('bs')
            res_a = lm.add(1, 2)
            res_b = bm.add(1, 2)
            res_c = bs.add('a', 'b')
            result = {res_a, res_b, res_c}
            "#,
            "result",
        )
        .unwrap();
    assert_eq!(res.to_string(), "[4,3,\"ab\"]")
}

struct StdMod;

impl IStdMod for StdMod {
    fn path(&self) -> &[&str] {
        &["mods", "hello"]
    }

    fn init<'lua>(&self, id: usize, lua: &'lua Lua, table: &'lua Table) -> LuaResult<()> {
        let fun = lua.create_function(move |_, _: ()| Ok(format!("hello from {}", id)))?;
        table.set("say_hello", fun)
    }
}

#[test]
fn test_std_lib_mod() {
    let factory = LuaFactory {
        stdlib: vec![Box::new(StdMod)],
        plugins: vec![],
    };
    let lua = factory.produce(3);
    let res = lua
        .exec_n_get_js("code", "res = std.mods.hello.say_hello()", "res")
        .unwrap();
    assert_eq!(res.to_string(), "hello from 3");
}

#[test]
fn test_std_lib_proc() {
    let config = MainConfig::build().unwrap();
    let factory = LuaFactory {
        stdlib: all_mods(&config),
        plugins: vec![],
    };
    let lua = factory.produce(3);
    let res = lua
        .exec_n_get_js("code", "res = std.proc.call('echo hi').stdout", "res")
        .unwrap();
    assert_eq!(res.to_string(), "hi\n");
}
