use chrono::Utc;
use std::thread;
use std::time::Duration;

use crate::entities::saga::SagaStatus;
use crate::impls::tests::common::Ctx;
use crate::proto::saga::{ISagaExecutor, ISagaRepo};

#[test]
fn test_create_saga() {
    let ctx = Ctx::new();
    let saga_id = ctx
        .executor
        .create_saga("type_b".to_string(), Some("1".to_string()))
        .unwrap();
    let saga = ctx.db_repo.get_saga(&saga_id).unwrap();
    let steps = ctx.db_repo.get_all_saga_steps(&saga.id).unwrap();
    assert_eq!(saga.name, "type_b");
    assert_eq!(saga.payload, Some("1".to_string()));
    assert_eq!(saga.retried, 0);
    assert_eq!(saga.step_name, "step1");
    assert_eq!(saga.status, SagaStatus::InProgress);

    assert_eq!(steps[0].name, "step1");
    assert_eq!(steps[0].payload, None);
    assert_eq!(steps[0].step_order, 1);
    assert_eq!(steps[0].status, SagaStatus::InProgress);

    assert_eq!(steps[1].name, "step2");
    assert_eq!(steps[1].payload, None);
    assert_eq!(steps[1].step_order, 2);
    assert_eq!(steps[1].status, SagaStatus::InProgress);
}

#[test]
fn test_execute_saga() {
    let ctx = Ctx::new();
    let saga_id = ctx
        .executor
        .create_saga("type_b".to_string(), Some("1".to_string()))
        .unwrap();
    ctx.executor.execute_saga(&saga_id).unwrap();
    let saga = ctx.db_repo.get_saga(&saga_id).unwrap();
    let steps = ctx.db_repo.get_all_saga_steps(&saga.id).unwrap();

    assert_eq!(saga.name, "type_b");
    assert_eq!(saga.retried, 0);
    assert_eq!(saga.step_name, "step2");
    assert_eq!(saga.status, SagaStatus::InProgress);

    assert_eq!(steps[0].name, "step1");
    assert_eq!(steps[0].payload, None);
    assert_eq!(steps[0].step_order, 1);
    assert_eq!(steps[0].status, SagaStatus::Completed);

    assert_eq!(steps[1].name, "step2");
    assert_eq!(steps[1].payload, None);
    assert_eq!(steps[1].step_order, 2);
    assert_eq!(steps[1].status, SagaStatus::InProgress);
}

#[test]
fn test_complete_saga() {
    let ctx = Ctx::new();
    let saga_id = ctx
        .executor
        .create_saga("type_b".to_string(), Some("1".to_string()))
        .unwrap();
    ctx.executor.execute_saga(&saga_id).unwrap();
    ctx.executor.execute_saga(&saga_id).unwrap();
    let saga = ctx.db_repo.get_saga(&saga_id).unwrap();
    let steps = ctx.db_repo.get_all_saga_steps(&saga.id).unwrap();

    assert_eq!(saga.name, "type_b");
    assert_eq!(saga.retried, 0);
    assert_eq!(saga.step_name, "step2");
    assert_eq!(saga.status, SagaStatus::Completed);

    assert_eq!(steps[0].name, "step1");
    assert_eq!(steps[0].payload, None);
    assert_eq!(steps[0].step_order, 1);
    assert_eq!(steps[0].status, SagaStatus::Completed);

    assert_eq!(steps[1].name, "step2");
    let got = json::parse(steps[1].payload.as_ref().unwrap()).unwrap();
    let expected = json::parse(r#"{"a":1,"b":2}"#).unwrap();
    assert_eq!(expected, got);
    assert_eq!(steps[1].step_order, 2);
    assert_eq!(steps[1].status, SagaStatus::Completed);
}

#[test]
fn test_need_to_wait_timeout() {
    let ctx = Ctx::new();
    let payload = r#"{"err_type": "N", "err_data": {"timeout": 5}}"#.to_string();
    let saga_id = ctx
        .executor
        .create_saga("type_c".to_string(), Some(payload))
        .unwrap();
    let saga = ctx.db_repo.get_saga(&saga_id).unwrap();
    let updated_ver_a = saga.updated_at;
    let execute_after_a = saga.execute_after;
    thread::sleep(Duration::from_secs(1));
    ctx.executor.execute_saga(&saga_id).unwrap();
    let saga = ctx.db_repo.get_saga(&saga_id).unwrap();
    let updated_ver_b = saga.updated_at;
    let execute_after_b = saga.execute_after;
    assert_ne!(updated_ver_a, updated_ver_b);
    assert!(execute_after_a.is_some());
    assert!(execute_after_b.is_some());
    assert_ne!(execute_after_a, execute_after_b);
    assert!(execute_after_b.unwrap() > Utc::now().timestamp());
    // let steps = ctx.db_repo.get_all_saga_steps(&saga.id).unwrap();
}
