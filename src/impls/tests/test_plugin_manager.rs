use std::path::PathBuf;

use mddd::traits::IConfig;

use crate::entities::config::MainConfig;
use crate::impls::fs_manager::FSManager;
use crate::impls::plugin_loader::{dylib, lua};
use crate::impls::plugin_manager::PluginManager;
use crate::proto::lua::IPluginManager;

#[test]
fn test_process_dir() {
    let fs = FSManager;
    let lpl = lua::PluginLoader;
    let bpl = dylib::PluginLoader::default();
    let mut config = MainConfig::build().unwrap();
    config.schema_root = PathBuf::from("test_data/saga_root");
    config.include = vec!["test_data/include_dir".to_string()];
    let manager_res = PluginManager::new(&fs, &lpl, &bpl, &config).unwrap();
    let plugins = manager_res.get_plugins();
    let mut names = plugins.iter().map(|p| p.name()).collect::<Vec<_>>();
    names.sort();
    assert_eq!(
        names,
        &["plugin_a", "super_plugin.plugin_b", "super_plugin.plugin_c"]
    );
}
