use std::io::{self, Read, Write};
use std::net::{TcpListener, TcpStream};
use std::str::FromStr;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::Duration;

use chrono::NaiveDateTime;
use json::object::Object;
use json::JsonValue;

use crate::entities::errors::SagaError;
use crate::entities::io::Cmd;
use crate::entities::saga::{SagaId, SagaStatus};
use crate::proto::saga::{ISagaRepo, ISagaRepoPool, ISagaRunner};

pub struct TcpIOInner<RP, SR> {
    listener: TcpListener,
    term_flag: Arc<AtomicBool>,
    saga_repo_pool: RP,
    runner: SR,
}

unsafe impl<R, S> Send for TcpIOInner<R, S> {}
unsafe impl<R, S> Sync for TcpIOInner<R, S> {}

fn read_int(stream: &mut TcpStream) -> io::Result<usize> {
    let mut buf = 0_u32.to_le_bytes();
    stream.read_exact(&mut buf)?;
    Ok(u32::from_le_bytes(buf) as usize)
}

fn read_string(stream: &mut TcpStream) -> io::Result<String> {
    let len = read_int(stream)?;
    if len == 0 {
        return Ok(Default::default());
    }
    let mut buf = vec![0_u8; len];
    stream.read_exact(&mut buf)?;
    String::from_utf8(buf).map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "not utf8"))
}

fn get_key<T, F: Fn(&JsonValue) -> Option<T>>(
    js: &Object,
    key: &str,
    cast: F,
) -> io::Result<T> {
    let val = match js.get(key) {
        None => return Err(
            io::Error::new(
                io::ErrorKind::InvalidInput,
                format!("key '{}' not found", key),
            )
        ),
        Some(val) => val,
    };
    match cast(val) {
        None => Err(
            io::Error::new(
                io::ErrorKind::InvalidInput,
                format!("key '{}': invalid value", key),
            )
        ),
        Some(val) => Ok(val)
    }
}

fn write_string(stream: &mut TcpStream, txt: &str) -> io::Result<()> {
    let bts = txt.as_bytes();
    let len = bts.len() as u32;
    stream.write_all(&len.to_le_bytes())?;
    stream.write_all(bts)
}

impl<RP: ISagaRepoPool + 'static, SR: ISagaRunner + 'static> TcpIOInner<RP, SR> {
    pub fn new(
        port: u16,
        saga_repo_pool: RP,
        runner: SR,
        term_flag: Arc<AtomicBool>,
    ) -> io::Result<Self> {
        let listener = TcpListener::bind(("0.0.0.0", port))?;
        Ok(Self {
            listener,
            term_flag,
            saga_repo_pool,
            runner,
        })
    }

    pub fn run(&self) -> io::Result<()> {
        for res_stream in self.listener.incoming() {
            if self.term_flag.load(Ordering::Relaxed) {
                break;
            }
            if let Err(err) = self.process_stream(res_stream?) {
                eprintln!("Process stream error: {:?}", err);
            }
        }
        Ok(())
    }

    fn process_stream(&self, mut stream: TcpStream) -> io::Result<()> {
        stream.set_read_timeout(Some(Duration::from_secs(1)))?;
        let cmd_text = match read_string(&mut stream) {
            Ok(val) => val,
            Err(_) => return write_string(&mut stream, "Err: can not read stream"),
        };
        let (cmd, js) = match self.parse_cmd(cmd_text) {
            Ok(val) => val,
            Err(_) => return write_string(&mut stream, "Err: can not parse cmd"),
        };
        let result = match cmd {
            Cmd::CreateSaga => self.cmd_create_saga(js),
            Cmd::SagaStatus => self.cmd_saga_status(js),
            Cmd::StepStatus => self.cmd_step_status(js),
            Cmd::GetStats => self.cmd_get_stats(),
            Cmd::GetErrs => self.cmd_get_errs(js),
        };
        let msg = match result {
            Ok(val) => val,
            Err(err) => format!("Err: {:?}", err),
        };
        write_string(&mut stream, &msg)
    }

    fn parse_cmd(&self, src: String) -> io::Result<(Cmd, Object)> {
        let js = json::parse(&src)
            .map_err(|_|io::Error::new(io::ErrorKind::InvalidInput, "invalid json"))?;
        let obj = match js {
            JsonValue::Object(val) => val,
            _ => return Err(io::Error::new(io::ErrorKind::InvalidInput, "invalid json")),
        };
        let cmd = get_key(&obj, "cmd", |js| {
            Cmd::from_str(js.as_str()?)
        })?;
        Ok((cmd, obj))
    }

    fn cmd_create_saga(&self, ctx: Object) -> io::Result<String> {
        let name = get_key(&ctx, "name", |js|
            Some(js.as_str()?.to_string())
        )?;
        let body = ctx.get("body")
            .map(|js|js.to_string());
        let saga_id = self
            .runner
            .create_saga(name, body)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, format!("{:?}", err)))?;
        let mut obj = Object::new();
        obj.insert("id", saga_id.to_string().into());
        Ok(JsonValue::Object(obj).to_string())
    }

    fn cmd_saga_status(&self, ctx: Object) -> io::Result<String> {
        let id = get_key(&ctx, "id", |js| {
            SagaId::from_str(js.as_str()?).ok()
        })?;
        let saga = self
            .get_repo()?
            .get_saga(&id)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, format!("{:?}", err)))?;
        let execute_after = match saga.execute_after {
            Some(val) => match NaiveDateTime::from_timestamp_opt(val, 0) {
                Some(val) => val.to_string(),
                _ => "n/a".to_string(),
            },
            _ => "None".to_string(),
        };
        let created = match NaiveDateTime::from_timestamp_opt(saga.created_at, 0) {
            Some(val) => val.to_string(),
            _ => "n/a".to_string(),
        };
        let mut obj = Object::new();
        obj.insert("name", saga.name.into());
        obj.insert("step", saga.step_name.into());
        obj.insert("status", format!("{:?}", saga.status).into());
        obj.insert("retried", saga.retried.into());
        obj.insert("execute_after", execute_after.into());
        obj.insert("created", created.into());
        Ok(JsonValue::Object(obj).to_string())
    }

    fn cmd_step_status(&self, ctx: Object) -> io::Result<String> {
        use std::fmt::Write;

        let id = get_key(&ctx, "id", |js| {
            SagaId::from_str(js.as_str()?).ok()
        })?;
        let result = self
            .get_repo()?
            .get_all_saga_steps(&id)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, format!("{:?}", err)))?
            .into_iter()
            .map(|step| {
                let mut obj = Object::new();
                obj.insert("name", step.name.into());
                obj.insert("order", step.step_order.into());
                obj.insert("status", format!("{:?}", step.status).into());
                JsonValue::Object(obj)
            })
            .collect::<Vec<_>>();
        Ok(JsonValue::Array(result).to_string())
    }

    fn cmd_get_stats(&self) -> io::Result<String> {
        let repo = self.get_repo()?;
        fn err(_: SagaError) -> io::Error {
            io::Error::new(io::ErrorKind::Other, "Can not get saga count")
        }
        let active = repo
            .get_count_of_status(&[SagaStatus::InProgress, SagaStatus::RollingBack])
            .map_err(err)?;
        let rolled_back = repo
            .get_count_of_status(&[SagaStatus::RolledBack])
            .map_err(err)?;
        let broken = repo
            .get_count_of_status(&[SagaStatus::Broken])
            .map_err(err)?;
        let mut obj = Object::new();
        obj.insert("active", active.into());
        obj.insert("rolled_back", rolled_back.into());
        obj.insert("broken", broken.into());
        Ok(JsonValue::Object(obj).to_string())
    }

    fn cmd_get_errs(&self, ctx: Object) -> io::Result<String> {
        let repo = self.get_repo()?;
        let ts = get_key(&ctx, "timestamp", |js|js.as_i64())?;
        let limit = get_key(&ctx, "limit", |js|js.as_usize())?;
        let rows = repo
            .get_last_errors(ts, limit)
            .map_err(|err| {
                let msg = format!("{:?}", err);
                io::Error::new(io::ErrorKind::Other, msg)
            })?
            .into_iter()
            .map(|row| {
                let mut obj = Object::new();
                obj.insert("saga_id", row.saga_id.to_string().into());
                obj.insert("step", row.step_name.into());
                obj.insert("timestamp", row.timestamp.into());
                obj.insert("error", row.error.unwrap().into());
                JsonValue::Object(obj)
            })
            .collect::<Vec<_>>();
        Ok(JsonValue::Array(rows).to_string())
    }

    fn get_repo(&self) -> io::Result<RP::Repo> {
        match self.saga_repo_pool.acquire_repo() {
            None => Err(io::Error::new(
                io::ErrorKind::Other,
                "Can not acquire repo".to_string(),
            )),
            Some(repo) => Ok(repo),
        }
    }
}
