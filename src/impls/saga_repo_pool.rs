use std::mem::replace;
use std::sync::{Arc, Mutex};

use crate::entities::errors::SagaResult;
use crate::entities::saga::{NewSagaStep, Saga, SagaId, SagaLog, SagaStatus, SagaStep};
use crate::proto::saga::{ISagaRepo, ISagaRepoPool};
use crate::utils::lt::to_static;

pub struct SagaRepoPool<R: ISagaRepo> {
    #[allow(dead_code)]
    factory: Box<dyn Fn() -> SagaResult<R>>,
    repos: Vec<R>,
    acquired: Arc<Mutex<Vec<bool>>>,
    single_mode: bool,
}

struct ReleaseCtx {
    acquired: Arc<Mutex<Vec<bool>>>,
    index: usize,
}

pub struct RepoWrapper<R: 'static> {
    repo: &'static R,
    release_ctx: Option<ReleaseCtx>,
}

impl<R: ISagaRepo> ISagaRepo for RepoWrapper<R> {
    type Trx = R::Trx;

    fn transaction(&self) -> SagaResult<Self::Trx> {
        self.repo.transaction()
    }

    fn create(&self, saga: Saga, steps: Vec<NewSagaStep>) -> SagaResult<()> {
        self.repo.create(saga, steps)
    }

    fn get_ready_ids(&self, limit: usize) -> SagaResult<Vec<SagaId>> {
        self.repo.get_ready_ids(limit)
    }

    fn get_saga(&self, id: &SagaId) -> SagaResult<Saga> {
        self.repo.get_saga(id)
    }

    fn get_last_saga_of_name(&self, name: &str) -> SagaResult<Saga> {
        self.repo.get_last_saga_of_name(name)
    }

    fn update_saga(&self, saga: &Saga, columns: &[&str]) -> SagaResult<()> {
        self.repo.update_saga(saga, columns)
    }

    fn get_count_of_status(&self, status: &[SagaStatus]) -> SagaResult<usize> {
        self.repo.get_count_of_status(status)
    }

    fn get_saga_steps(&self, saga_id: &SagaId, keys: &[&str]) -> SagaResult<Vec<SagaStep>> {
        self.repo.get_saga_steps(saga_id, keys)
    }

    fn get_all_saga_steps(&self, saga_id: &SagaId) -> SagaResult<Vec<SagaStep>> {
        self.repo.get_all_saga_steps(saga_id)
    }

    fn update_step(&self, step: &SagaStep, columns: &[&str]) -> SagaResult<()> {
        self.repo.update_step(step, columns)
    }

    fn log(&self, log: SagaLog) -> SagaResult<()> {
        self.repo.log(log)
    }

    fn get_last_errors(&self, min_ts: i64, limit: usize) -> SagaResult<Vec<SagaLog>> {
        self.repo.get_last_errors(min_ts, limit)
    }
}

fn release_repo(acquired: Arc<Mutex<Vec<bool>>>, index: usize) {
    let mut acquired = match acquired.lock() {
        Ok(val) => val,
        _ => panic!("repo pool mutex is broken"),
    };
    acquired[index] = false;
}

impl<R> Drop for RepoWrapper<R> {
    fn drop(&mut self) {
        let ctx = match replace(&mut self.release_ctx, None) {
            Some(val) => val,
            _ => return,
        };
        release_repo(ctx.acquired, ctx.index)
    }
}

impl<R: ISagaRepo> SagaRepoPool<R> {
    /// Use None for single mode
    pub fn new(
        factory: Box<dyn Fn() -> SagaResult<R>>,
        opt_count: Option<usize>,
    ) -> SagaResult<Self> {
        if let Some(count) = opt_count {
            let mut repos = Vec::new();
            for _ in 0..count {
                repos.push(factory()?);
            }
            Ok(Self {
                factory,
                repos,
                acquired: Arc::new(Mutex::new(vec![false; count])),
                single_mode: true,
            })
        } else {
            let repo = factory()?;
            Ok(Self {
                factory,
                repos: vec![repo],
                acquired: Arc::new(Mutex::new(vec![])),
                single_mode: true,
            })
        }
    }
}

impl<R: ISagaRepo + 'static> ISagaRepoPool for SagaRepoPool<R> {
    type Repo = RepoWrapper<R>;

    fn acquire_repo(&self) -> Option<Self::Repo> {
        if self.single_mode {
            let rw = RepoWrapper {
                repo: to_static(&self.repos[0]),
                release_ctx: None,
            };
            return Some(rw);
        }
        let mut acquired = match self.acquired.lock() {
            Ok(val) => val,
            _ => panic!("repo pool mutex is broken"),
        };
        for i in 0..acquired.len() {
            if !acquired[i] {
                acquired[i] = true;
                let rw = RepoWrapper {
                    repo: to_static(&self.repos[i]),
                    release_ctx: Some(ReleaseCtx {
                        acquired: self.acquired.clone(),
                        index: i,
                    }),
                };
                return Some(rw);
            }
        }
        None
    }
}
