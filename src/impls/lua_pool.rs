use json::JsonValue;
use mlua::prelude::LuaResult;
use std::mem::replace;
use std::sync::{Arc, Mutex};

use crate::proto::lua::{ILuaExecutor, ILuaFactory, ILuaPool};
use crate::utils::lt::to_static;

pub struct LuaPool<E: ILuaExecutor> {
    lua: Vec<E>,
    acquired: Arc<Mutex<Vec<bool>>>,
}

struct ReleaseCtx {
    acquired: Arc<Mutex<Vec<bool>>>,
    index: usize,
}

pub struct LuaWrapper<E: 'static> {
    lua: &'static E,
    release_ctx: Option<ReleaseCtx>,
}

impl<E: ILuaExecutor> ILuaExecutor for LuaWrapper<E> {
    fn set_globals(&self, values: &[(&str, JsonValue)]) -> LuaResult<()> {
        self.lua.set_globals(values)
    }

    fn exec(&self, name: &str, code: &str) -> LuaResult<()> {
        self.lua.exec(name, code)
    }

    fn exec_n_get_js(&self, name: &str, code: &str, result: &str) -> LuaResult<JsonValue> {
        self.lua.exec_n_get_js(name, code, result)
    }
}

fn release_lua(acquired: Arc<Mutex<Vec<bool>>>, index: usize) {
    let mut acquired = match acquired.lock() {
        Ok(val) => val,
        _ => panic!("lua pool mutex is broken"),
    };
    acquired[index] = false;
}

impl<L> Drop for LuaWrapper<L> {
    fn drop(&mut self) {
        let ctx = match replace(&mut self.release_ctx, None) {
            Some(val) => val,
            _ => return,
        };
        release_lua(ctx.acquired, ctx.index)
    }
}

impl<L: ILuaExecutor> LuaPool<L> {
    /// Use None for single mode
    pub fn new<LF: ILuaFactory<Executor = L>>(factory: &LF, count: usize) -> Self {
        let mut pool = Vec::new();
        for id in 0..count {
            pool.push(factory.produce(id));
        }
        Self {
            lua: pool,
            acquired: Arc::new(Mutex::new(vec![false; count])),
        }
    }
}

impl<E: ILuaExecutor + 'static> ILuaPool for LuaPool<E> {
    type Executor = LuaWrapper<E>;

    fn acquire_lua(&self) -> Option<Self::Executor> {
        let mut acquired = match self.acquired.lock() {
            Ok(val) => val,
            _ => panic!("repo pool mutex is broken"),
        };
        for i in 0..acquired.len() {
            if !acquired[i] {
                acquired[i] = true;
                let rw = LuaWrapper {
                    lua: to_static(&self.lua[i]),
                    release_ctx: Some(ReleaseCtx {
                        acquired: self.acquired.clone(),
                        index: i,
                    }),
                };
                return Some(rw);
            }
        }
        None
    }
}
