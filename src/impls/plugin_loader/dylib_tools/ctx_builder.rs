use std::ffi::{c_uint, CStr};
use std::mem;
use std::os::raw::c_char;
use std::str::FromStr;

use libloading::{Library, Symbol};

use crate::entities::dylib::{DyLibCtx, DyValType, FuncSchema};
use crate::entities::errors::{PluginError, PluginResult};

fn get_func<'a, T>(lib: &'a Library, libname: &str, name: &str) -> PluginResult<Symbol<'a, T>> {
    unsafe {
        lib.get(name.as_bytes()).map_err(|_| {
            let msg = format!("{}: {}", libname, name);
            PluginError::BinaryPluginMethodNotFound(msg)
        })
    }
}

fn load(name: &str, path: &str) -> PluginResult<Box<Library>> {
    unsafe {
        let lib =
            Library::new(path).map_err(|_| PluginError::BinaryPluginIsInvalid(name.to_string()))?;
        Ok(Box::new(lib))
    }
}

fn parse_type(raw: &str, lib: &str, line_id: usize) -> PluginResult<DyValType> {
    if let Ok(val) = DyValType::from_str(raw) {
        return Ok(val);
    }
    let msg = format!("{}::schema(), line {}", lib, line_id);
    Err(PluginError::BinaryPluginIsInvalid(msg))
}

fn parse_func(raw: &str, lib: &str, line_id: usize) -> PluginResult<FuncSchema> {
    let split = raw.split('(').collect::<Vec<_>>();
    if split.len() != 2 {
        let msg = format!("{}::schema(), line {}", lib, line_id);
        return Err(PluginError::BinaryPluginIsInvalid(msg));
    }
    let name = split[0].to_string();
    let split = split[1].split(')').collect::<Vec<_>>();
    if split.len() != 2 {
        let msg = format!("{}::schema(), line {}", lib, line_id);
        return Err(PluginError::BinaryPluginIsInvalid(msg));
    }
    let raw_params = split[0];
    let split = split[1].split("->").collect::<Vec<_>>();
    if split.len() != 2 {
        let msg = format!("{}::schema(), line {}", lib, line_id);
        return Err(PluginError::BinaryPluginIsInvalid(msg));
    }
    let result = parse_type(split[1].trim(), lib, line_id)?;
    let mut params = Vec::new();
    if !raw_params.trim().is_empty() {
        for token in raw_params.split(',') {
            params.push(parse_type(token.trim(), lib, line_id)?);
        }
    }
    Ok(FuncSchema {
        name,
        params,
        result,
    })
}

fn parse_schema(libname: &str, raw: *const c_char) -> PluginResult<Vec<FuncSchema>> {
    let cstr = unsafe { CStr::from_ptr(raw) };
    let text = cstr.to_str().map_err(|_| {
        let msg = format!("{}::schema()", libname);
        PluginError::BinaryPluginIsInvalid(msg)
    })?;
    let mut result = Vec::new();
    for (num, line) in text.split('\n').enumerate() {
        let stripped = line.trim();
        if stripped.is_empty() {
            continue;
        }
        result.push(parse_func(stripped, libname, num)?);
    }
    Ok(result)
}

#[allow(clippy::redundant_closure)]
fn convert_callback(sym: Symbol<extern "C" fn(c_uint)>) -> Box<dyn Fn(c_uint)> {
    let changed: Symbol<'static, extern "C" fn(c_uint)> = unsafe { mem::transmute(sym) };
    Box::new(move |id| changed(id))
}

pub fn load_ctx(name: &str, path: &str) -> PluginResult<DyLibCtx> {
    let lib = load(name, path)?;
    let schema_f: Symbol<extern "C" fn() -> *const c_char> = get_func(&lib, name, "schema")?;
    let init_f: Symbol<extern "C" fn(c_uint)> = get_func(&lib, name, "init")?;
    let finalize_f: Symbol<extern "C" fn(c_uint)> = get_func(&lib, name, "finalize")?;
    let funcs = parse_schema(name, schema_f())?;
    let init = convert_callback(init_f);
    let finalize = convert_callback(finalize_f);
    Ok(DyLibCtx {
        lib,
        name: name.to_string(),
        funcs,
        init,
        finalize,
    })
}
