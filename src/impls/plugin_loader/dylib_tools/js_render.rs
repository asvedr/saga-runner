use json::object::Object;
use json::JsonValue;
use mlua::prelude::LuaResult;
use mlua::{Error, Table, Value};

fn is_arr(tbl: &Table) -> bool {
    if tbl.get_metatable().is_some() {
        return false;
    }
    if let Some(pair) = tbl.clone().pairs().next() {
        let res: Result<(isize, Value), _> = pair;
        return res.is_ok();
    }
    true
}

fn to_arr(tbl: Table) -> LuaResult<Vec<JsonValue>> {
    let mut result = Vec::new();
    for pair in tbl.pairs() {
        let (_, val): (isize, Value) = match pair {
            Ok(val) => val,
            _ => unreachable!(),
        };
        result.push(lua_to_js(val)?);
    }
    Ok(result)
}

fn to_dict(tbl: Table) -> LuaResult<Object> {
    let mut result = Object::new();
    for pair in tbl.pairs() {
        let (key, val): (String, Value) = match pair {
            Ok(val) => val,
            _ => return Err(Error::RuntimeError("can not convert to json".to_string())),
        };
        result.insert(&key, lua_to_js(val)?);
    }
    Ok(result)
}

pub fn lua_to_js(value: Value) -> LuaResult<JsonValue> {
    let js = match value {
        Value::Nil => JsonValue::Null,
        Value::Boolean(val) => JsonValue::Boolean(val),
        Value::Integer(val) => JsonValue::Number(val.into()),
        Value::Number(val) => JsonValue::Number(val.into()),
        Value::String(val) => JsonValue::String(val.to_str()?.to_string()),
        Value::Table(tbl) => {
            if is_arr(&tbl) {
                JsonValue::Array(to_arr(tbl)?)
            } else {
                JsonValue::Object(to_dict(tbl)?)
            }
        }
        _ => return Err(Error::RuntimeError("can not convert to json".to_string())),
    };
    Ok(js)
}

#[inline]
pub fn render_json(value: Value) -> LuaResult<String> {
    Ok(lua_to_js(value)?.to_string())
}
