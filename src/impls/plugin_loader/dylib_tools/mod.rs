// every fun is
// Fn(
//      c_uint, -- lua id
//      *const c_void, -- array of args
//      *const u8, -- result ptr,
//      *const u8, -- error ptr,
// ) -> c_uint -- 1 ok, 0 - err

pub mod ctx_builder;
pub mod ffi_wrapper;
pub mod js_parser;
pub mod js_render;
