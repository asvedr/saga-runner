use std::str::FromStr;

use json::{parse, JsonValue};

use mlua::prelude::LuaResult;
use mlua::{Error, Lua, ToLua, Value};

pub fn js_to_lua<'a>(lua: &'a Lua, js: &JsonValue) -> LuaResult<Value<'a>> {
    match js {
        JsonValue::Null => Ok(Value::Nil),
        JsonValue::Short(val) => val.as_str().to_lua(lua),
        JsonValue::String(val) => val.as_str().to_lua(lua),
        JsonValue::Number(val) => {
            let str_val = val.to_string();
            if str_val.contains('.') {
                f64::from_str(&str_val).unwrap().to_lua(lua)
            } else {
                i64::from_str(&str_val).unwrap().to_lua(lua)
            }
        }
        JsonValue::Boolean(val) => Ok(Value::Boolean(*val)),
        JsonValue::Object(dict) => {
            let tbl = lua.create_table()?;
            for (key, item) in dict.iter() {
                let val = js_to_lua(lua, item)?;
                tbl.set(key, val)?;
            }
            Ok(Value::Table(tbl))
        }
        JsonValue::Array(arr) => {
            let tbl = lua.create_table()?;
            for (i, item) in arr.iter().enumerate() {
                let key = i + 1;
                let val = js_to_lua(lua, item)?;
                tbl.set(key, val)?;
            }
            Ok(Value::Table(tbl))
        }
    }
}

#[inline]
pub fn parse_json<'a>(lua: &'a Lua, js: &str) -> LuaResult<Value<'a>> {
    let js = parse(js).map_err(|_| Error::RuntimeError("invalid json".to_string()))?;
    js_to_lua(lua, &js)
}
