use std::ffi::{c_char, c_double, c_int, c_uint, c_void, CStr, CString};
use std::mem;
use std::ptr::null;
use std::sync::Arc;

use libloading::Symbol;
use mlua::prelude::{LuaError, LuaResult, LuaValue};
use mlua::{Error, FromLua, Lua, ToLua, Value};

use crate::entities::dylib::{DyLibCtx, DyValType, ExFun, FuncSchema, WrappedExFun};
use crate::entities::errors::{PluginError, PluginResult};
use crate::impls::plugin_loader::dylib_tools::js_parser::parse_json;
use crate::impls::plugin_loader::dylib_tools::js_render::render_json;

enum DyArg {
    Int(c_int),
    Real(c_double),
    Str(CString),
}

struct DyCallCtx {
    values: Vec<DyArg>,
    ptrs: Vec<*const c_void>,
}

impl DyCallCtx {
    fn prepare(
        lua: &Lua,
        schema: &[DyValType],
        raw_args: Vec<Value>,
        fun_name: &str,
    ) -> LuaResult<Self> {
        if schema.len() != raw_args.len() {
            let msg = format!(
                "{}: expected {} args, got {}",
                fun_name,
                schema.len(),
                raw_args.len()
            );
            return Err(LuaError::RuntimeError(msg));
        }
        let mut values = Vec::with_capacity(schema.len());
        for (i, value) in raw_args.into_iter().enumerate() {
            let value = render_value(lua, value, schema[i])?;
            values.push(value);
        }
        Ok(Self {
            values,
            ptrs: vec![],
        })
    }

    fn args_ptr(&mut self) -> *const c_void {
        if self.values.is_empty() {
            return null();
        }
        self.ptrs.clear();
        for value in self.values.iter() {
            match value {
                DyArg::Int(raw) => self.ptrs.push(raw as *const c_int as *const c_void),
                DyArg::Real(raw) => self.ptrs.push(raw as *const c_double as *const c_void),
                DyArg::Str(raw) => self.ptrs.push(raw.as_ptr() as *const c_void),
            }
        }
        let ptr: *const *const c_void = &self.ptrs[0];
        ptr as *const c_void
    }
}

fn render_value(lua: &Lua, raw: LuaValue, tp: DyValType) -> LuaResult<DyArg> {
    let result = match tp {
        DyValType::Void => DyArg::Int(0),
        DyValType::Bool => DyArg::Int(bool::from_lua(raw, lua)? as c_int),
        DyValType::Int => DyArg::Int(c_int::from_lua(raw, lua)?),
        DyValType::Real => DyArg::Real(c_double::from_lua(raw, lua)?),
        DyValType::Str => DyArg::Str(CString::from_lua(raw, lua)?),
        DyValType::Json => {
            let js = render_json(raw)?.into_bytes();
            DyArg::Str(unsafe { CString::from_vec_unchecked(js) })
        }
    };
    Ok(result)
}

fn get_symbol<'a>(ctx: &'a DyLibCtx, name: &str) -> PluginResult<Symbol<'static, ExFun>> {
    let lib = &ctx.lib;
    let libname = &ctx.name;
    let sym: Symbol<'a, ExFun> = unsafe {
        lib.get(name.as_bytes()).map_err(|_| {
            let msg = format!("{}::{}", libname, name);
            PluginError::BinaryPluginMethodNotFound(msg)
        })?
    };
    unsafe { Ok(mem::transmute(sym)) }
}

fn parse_err(ptr: *const c_void) -> String {
    if ptr.is_null() {
        return String::new();
    }
    let cstr = unsafe { CStr::from_ptr(ptr as *const c_char) };
    match cstr.to_str() {
        Ok(val) => val.to_string(),
        Err(_) => panic!("can not parse str from error"),
    }
}

fn parse_result(lua: &Lua, schema: DyValType, ptr: *const c_void) -> LuaResult<Value> {
    match schema {
        DyValType::Void => Ok(Value::Nil),
        DyValType::Bool => {
            let val = unsafe { *(ptr as *const c_int) != 0 };
            Ok(Value::Boolean(val))
        }
        DyValType::Int => {
            let val = unsafe { *(ptr as *const c_int) };
            val.to_lua(lua)
        }
        DyValType::Real => {
            let val = unsafe { *(ptr as *const c_double) };
            Ok(Value::Number(val))
        }
        DyValType::Str => {
            if ptr.is_null() {
                return String::new().to_lua(lua);
            }
            let cstr = unsafe { CStr::from_ptr(ptr as *const c_char) };
            cstr.to_lua(lua)
        }
        DyValType::Json => {
            if ptr.is_null() {
                return Ok(Value::Nil);
            }
            let cstr = unsafe { CStr::from_ptr(ptr as *const c_char) };
            let str = cstr
                .to_str()
                .map_err(|_| Error::RuntimeError("ext string is not utf8".to_string()))?;
            parse_json(lua, str)
        }
    }
}

pub fn compile_func(
    ctx: Arc<DyLibCtx>,
    lua_id: c_uint,
    schema: &FuncSchema,
) -> PluginResult<WrappedExFun<'static>> {
    let fun_name = schema.name.clone();
    let symbol = get_symbol(&ctx, &fun_name)?;
    let params_schema = schema.params.clone();
    let result_schema = schema.result;
    let result: WrappedExFun = Box::new(
        move |lua: &'static Lua, raw_args| -> LuaResult<Value<'static>> {
            let mut call_ctx =
                DyCallCtx::prepare(lua, &params_schema, raw_args.into_vec(), &fun_name)?;
            let mut result: *const c_void = null();
            let status = symbol(
                lua_id,
                call_ctx.args_ptr() as *const *const c_void,
                &mut result,
            );
            if status == 1 {
                parse_result(lua, result_schema, result)
            } else {
                let msg = parse_err(result);
                Err(LuaError::RuntimeError(msg))
            }
        },
    );
    Ok(result)
}
