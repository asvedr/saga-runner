use std::cell::RefCell;
use std::fs;
use std::io::Read;
use std::path::Path;

use mlua::Lua;

use crate::entities::errors::{PluginError, PluginResult};
use crate::entities::plugins::{CodeBlock, MODULE_PREFIX};
use crate::proto::lua::{IPlugin, IPluginLoader};

pub struct PluginLoader;

struct Plugin {
    name: String,
    path: String,
    inited: RefCell<bool>,
}

impl IPlugin for Plugin {
    fn clone_box(&self) -> Box<dyn IPlugin> {
        Box::new(Self {
            name: self.name.clone(),
            path: self.path.clone(),
            inited: RefCell::new(false),
        })
    }

    fn name(&self) -> &str {
        &self.name
    }

    fn init(&self, lua: &Lua, _: usize) -> PluginResult<()> {
        if *self.inited.borrow() {
            return Ok(());
        }
        let mut file = fs::File::open(&self.path).map_err(|_| PluginError::CanNotReadFile)?;
        let mut code = String::new();
        file.read_to_string(&mut code)
            .map_err(|_| PluginError::CanNotReadFile)?;
        let code = format!(
            "{prefix}{name} = (function()\nlocal module = {empty}\n{code}return module\nend)()\n",
            prefix = MODULE_PREFIX,
            empty = "{}",
            name = self.name,
            code = code,
        );
        let block = CodeBlock {
            source: &code,
            name: &self.path,
            shift: -2,
        };
        lua.load(&block).exec().map_err(PluginError::LuaError)?;
        *self.inited.borrow_mut() = true;
        Ok(())
    }
}

impl IPluginLoader for PluginLoader {
    fn load_plugin(&self, path: &str, name: &str) -> PluginResult<Box<dyn IPlugin>> {
        if !Path::new(path).is_file() {
            return Err(PluginError::FileNotFound(path.to_string()));
        }
        let plugin = Plugin {
            name: name.to_string(),
            path: path.to_string(),
            inited: RefCell::new(false),
        };
        Ok(Box::new(plugin))
    }
}
