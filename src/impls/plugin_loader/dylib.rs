use std::collections::HashMap;
use std::ffi::c_uint;
use std::mem;
use std::sync::{Arc, Mutex};

use mlua::Lua;

use crate::entities::dylib::{DyLibCtx, WrappedExFun};
use crate::entities::errors::{PluginError, PluginResult};
use crate::entities::plugins::MODULE_PREFIX;
use crate::impls::plugin_loader::dylib_tools::ctx_builder::load_ctx;
use crate::impls::plugin_loader::dylib_tools::ffi_wrapper::compile_func;
use crate::proto::lua::{IPlugin, IPluginLoader};

pub struct PluginLoader {
    plugins: Mutex<HashMap<String, Plugin>>,
}

impl Default for PluginLoader {
    fn default() -> Self {
        Self {
            plugins: Mutex::new(Default::default()),
        }
    }
}

#[derive(Clone)]
struct Plugin {
    inner: Arc<PluginInner>,
}

struct PluginInner {
    ctx: Arc<DyLibCtx>,
    inited_for: Mutex<Vec<usize>>,
}

fn register_methods(lua: &Lua, lua_id: c_uint, ctx: &Arc<DyLibCtx>) -> PluginResult<()> {
    let lua: &'static Lua = unsafe { mem::transmute(lua) };
    let table = lua.create_table()?;
    for schema in ctx.funcs.iter() {
        let ctx_copy: Arc<DyLibCtx> = ctx.clone();
        let func: WrappedExFun<'static> = compile_func(ctx_copy, lua_id, schema)?;
        let lua_func = lua.create_function(func)?;
        table.set(schema.name.clone(), lua_func)?;
    }
    let mod_name = format!("{}{}", MODULE_PREFIX, ctx.name);
    lua.globals().set(mod_name, table)?;
    Ok(())
}

impl IPlugin for Plugin {
    fn clone_box(&self) -> Box<dyn IPlugin> {
        Box::new(self.clone())
    }

    fn name(&self) -> &str {
        &self.inner.ctx.name
    }

    fn init(&self, lua: &Lua, lua_id: usize) -> PluginResult<()> {
        let mut inited_for = self.inner.inited_for.lock().map_err(|_| {
            let msg = format!("plugin init: {}", self.inner.ctx.name);
            PluginError::BrokenMutex(msg)
        })?;
        if inited_for.contains(&lua_id) {
            return Ok(());
        }
        (self.inner.ctx.init)(lua_id as c_uint);
        register_methods(lua, lua_id as c_uint, &self.inner.ctx)?;
        inited_for.push(lua_id);
        Ok(())
    }
}

impl IPluginLoader for PluginLoader {
    fn load_plugin(&self, path: &str, name: &str) -> PluginResult<Box<dyn IPlugin>> {
        let mut plugins = self
            .plugins
            .lock()
            .map_err(|_| PluginError::BrokenMutex("PluginLoader.plugins".to_string()))?;
        if let Some(plugin) = plugins.get(name) {
            return Ok(Box::new(plugin.clone()));
        }
        let res = load_ctx(name, path);
        let ctx = Arc::new(res?);
        let inner = PluginInner {
            ctx,
            inited_for: Mutex::new(vec![]),
        };
        let plugin = Plugin {
            inner: Arc::new(inner),
        };
        let result = Box::new(plugin.clone());
        plugins.insert(name.to_string(), plugin);
        Ok(result)
    }
}
