use json::JsonValue;
use mlua::prelude::LuaResult;
use mlua::{Error, Lua, Value};
use std::str::FromStr;
use std::sync::Arc;

use crate::entities::plugins::CodeBlock;
use crate::impls::plugin_loader::dylib_tools::js_parser::js_to_lua;
use crate::impls::plugin_loader::dylib_tools::js_render::lua_to_js;
use crate::proto::lua::ILuaExecutor;

const FORMAT_PREFIX: &str = "[[SHIFT ";
const FORMAT_SUFFIX: &str = "]]";

pub struct LuaExecutor {
    lua: Lua,
}

impl From<Lua> for LuaExecutor {
    fn from(lua: Lua) -> Self {
        Self { lua }
    }
}

#[inline(always)]
fn format_msg_line_with_shift(line: &str, is_first: bool) -> String {
    let mut split = line.split(FORMAT_PREFIX);
    // empty text
    split.next();
    let line = split.next().unwrap();
    let mut split = line.split(FORMAT_SUFFIX);
    let shift_src = split.next().unwrap();
    let shift = match isize::from_str(shift_src) {
        Ok(val) => val,
        Err(_) => return line.to_string(),
    };
    let rest_of_line = split.collect::<Vec<_>>().join(FORMAT_SUFFIX);
    let mut split = rest_of_line.split(':');
    let filename = split.next().unwrap();
    let line_num_src = match split.next() {
        None => return rest_of_line,
        Some(val) => val,
    };
    let line_num = match isize::from_str(line_num_src) {
        Ok(val) => val,
        Err(_) => return rest_of_line,
    };
    let shifted = line_num + shift;
    if shifted < 0 {
        return String::new();
    }
    if is_first {
        let suff = split.collect::<Vec<_>>().join(":");
        format!("{}:{}:{}", filename, shifted, suff)
    } else {
        format!("{}:{}", filename, shifted)
    }
}

#[inline(always)]
fn add_prefix(line: &str) -> String {
    format!("\t{}", line)
}

fn format_msg_line(has_prefix: bool, line: &str) -> String {
    if line.starts_with(FORMAT_PREFIX) {
        let line = format_msg_line_with_shift(line, !has_prefix);
        if has_prefix && !line.is_empty() {
            add_prefix(&line)
        } else {
            line
        }
    } else {
        let line = line.split(FORMAT_PREFIX).next().unwrap();
        if has_prefix && !line.is_empty() {
            add_prefix(line)
        } else {
            line.to_string()
        }
    }
}

#[allow(clippy::ptr_arg)]
fn format_msg(msg: &String) -> String {
    let mut result = String::new();
    for line in msg.split('\n') {
        let has_prefix = line.starts_with('\t');
        let formatted = format_msg_line(has_prefix, line.trim());
        if !formatted.is_empty() {
            result.push_str(&formatted);
            result.push('\n');
        }
    }
    result
}

fn format_error(err: &Error) -> Error {
    match err {
        Error::SyntaxError {
            message,
            incomplete_input,
        } => Error::SyntaxError {
            message: format_msg(message),
            incomplete_input: *incomplete_input,
        },
        Error::RuntimeError(msg) => Error::RuntimeError(format_msg(msg)),
        Error::MemoryError(msg) => Error::MemoryError(format_msg(msg)),
        Error::SafetyError(msg) => Error::SafetyError(format_msg(msg)),
        Error::ToLuaConversionError { from, to, message } => Error::ToLuaConversionError {
            from,
            to,
            message: message.as_ref().map(format_msg),
        },
        Error::FromLuaConversionError { from, to, message } => Error::FromLuaConversionError {
            from,
            to,
            message: message.as_ref().map(format_msg),
        },
        Error::MetaMethodRestricted(msg) => Error::MetaMethodRestricted(format_msg(msg)),
        Error::MetaMethodTypeError {
            method,
            type_name,
            message,
        } => Error::MetaMethodTypeError {
            method: method.clone(),
            type_name,
            message: message.as_ref().map(format_msg),
        },
        Error::CallbackError { traceback, cause } => Error::CallbackError {
            traceback: format_msg(traceback),
            cause: Arc::new(format_error(cause)),
        },
        other => other.clone(),
    }
}

impl ILuaExecutor for LuaExecutor {
    fn set_globals(&self, values: &[(&str, JsonValue)]) -> LuaResult<()> {
        let globals = self.lua.globals();
        for (key, val) in values {
            globals.set(*key, js_to_lua(&self.lua, val)?)?;
        }
        Ok(())
    }

    fn exec(&self, name: &str, code: &str) -> LuaResult<()> {
        let block = CodeBlock {
            source: code,
            name,
            shift: 0,
        };
        self.lua.load(&block).exec().map_err(|e| format_error(&e))
    }

    fn exec_n_get_js(&self, name: &str, code: &str, result: &str) -> LuaResult<JsonValue> {
        let block = CodeBlock {
            source: code,
            name,
            shift: 0,
        };
        self.lua.load(&block).exec().map_err(|e| format_error(&e))?;
        let value: Value = self
            .lua
            .globals()
            .get(result)
            .map_err(|e| format_error(&e))?;
        lua_to_js(value)
    }
}
