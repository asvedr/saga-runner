use std::collections::HashMap;
use std::path::Path;

use crate::entities::config::MainConfig;
use crate::entities::errors::{SagaBuilderError, SagaBuilderResult};
use crate::entities::saga::{PeriodicSchema, SagaSchema, SagaStepSchema};
use crate::proto::common::IFSManager;
use crate::proto::saga::{ISagaSchemaManager, ISagaSchemaParser};

const SAGAS_DIR: &str = "sagas";
const SCHEMA_FILE: &str = "saga.toml";

pub struct SagaSchemaManager<FS: IFSManager, P: ISagaSchemaParser> {
    fs: FS,
    parser: P,
    schemas: HashMap<String, SagaSchema>,
}

impl<FS: IFSManager, P: ISagaSchemaParser> SagaSchemaManager<FS, P> {
    pub fn new(fs: FS, parser: P, config: &MainConfig) -> SagaBuilderResult<Self> {
        let mut man = Self {
            fs,
            schemas: Default::default(),
            parser,
        };
        man.init(config)?;
        Ok(man)
    }

    fn init(&mut self, config: &MainConfig) -> SagaBuilderResult<()> {
        let content = self.fs.list_dir(&config.schema_root)?;
        if !content.iter().any(|p| *p == SAGAS_DIR) {
            return Err(SagaBuilderError::SagaDirNotFound);
        }
        let saga_root_path = config.schema_root.join(SAGAS_DIR);
        let saga_dirs = self.fs.list_dir(&saga_root_path)?;
        for saga in saga_dirs {
            let path = saga_root_path.join(&saga);
            self.init_saga(saga, &path)?;
        }
        Ok(())
    }

    fn init_saga(&mut self, name: String, dir: &Path) -> SagaBuilderResult<()> {
        let src = self.fs.read_text(&dir.join(SCHEMA_FILE))?;
        let schema = self.parser.parse_schema(dir, &src)?;
        self.schemas.insert(name, schema);
        Ok(())
    }
}

impl<FS: IFSManager, P: ISagaSchemaParser> ISagaSchemaManager for SagaSchemaManager<FS, P> {
    fn get_schema(&self, saga_name: &str) -> Option<&SagaSchema> {
        self.schemas.get(saga_name)
    }

    fn available_schemas(&self) -> Vec<&str> {
        self.schemas.keys().map(|s| -> &str { s }).collect()
    }

    fn get_periodics(&self) -> Vec<(String, PeriodicSchema)> {
        self.schemas
            .iter()
            .filter_map(|(key, val)| -> Option<(String, PeriodicSchema)> {
                let schema = val.periodic.as_ref()?;
                Some((key.to_string(), schema.clone()))
            })
            .collect::<Vec<_>>()
    }
}
