use std::str::FromStr;

use chrono::Utc;
use easy_sqlite::traits::repo::{IDbRepo, INewDbRepo};
use easy_sqlite::traits::table::ITable;
use easy_sqlite::{deescape, escape, MSQLConnection, TableManager};
use rusqlite::types::Type;
use uuid::Uuid;

use crate::entities::errors::SagaResult;
use crate::entities::saga::{NewSagaStep, Saga, SagaId, SagaLog, SagaStatus, SagaStep};
use crate::impls::sqlite_saga_repo::tables::{SagaLogTable, SagaStepTable, SagaTable, Setter};
use crate::proto::saga::{ISagaRepo, ITransaction};

type Connection = MSQLConnection;

pub struct DBSagaRepo {
    // cnn: Connection,
    saga_tbl: TableManager<Connection, SagaTable>,
    step_tbl: TableManager<Connection, SagaStepTable>,
    log_tbl: TableManager<Connection, SagaLogTable>,
    saga_setters: Vec<(&'static str, Setter<Saga>)>,
    step_setters: Vec<(&'static str, Setter<SagaStep>)>,
    ready_statuses: String,
    null: String,
}

pub struct Trx;

impl ITransaction for Trx {
    fn commit(&self) -> SagaResult<()> {
        Ok(())
    }

    fn rollback(&self) -> SagaResult<()> {
        Ok(())
    }
}

impl DBSagaRepo {
    pub fn new(connect_string: &str) -> SagaResult<Self> {
        let cnn = MSQLConnection::new(connect_string)?;
        let saga_tbl = TableManager::create(cnn.clone());
        let step_tbl = TableManager::create(cnn.clone());
        let log_tbl = TableManager::create(cnn);
        saga_tbl.init()?;
        step_tbl.init()?;
        log_tbl.init()?;
        let null = "NULL".to_string();
        let saga_setters = SagaTable::setters();
        let step_setters = SagaStepTable::setters();
        let ready_statuses = SagaStatus::ACTIVE
            .iter()
            .map(|s| s.as_i32().to_string())
            .collect::<Vec<_>>()
            .join(",");
        Ok(Self {
            saga_tbl,
            step_tbl,
            log_tbl,
            null,
            saga_setters,
            step_setters,
            ready_statuses,
        })
    }

    fn create_saga(&self, saga: &Saga, payload: &str, step_name: &str) -> SagaResult<()> {
        let now = Utc::now().timestamp();
        let execute_after = saga.execute_after.unwrap_or(now);
        let query = format!(
            r#"INSERT INTO `{tbl}` (
                `id`, `name`, `payload`,
                `step_name`, `status`,
                `retried`, `created_at`,
                `execute_after`, `updated_at`
            )
            VALUES (
                '{id}', {name}, {payload},
                {step_name}, {status},
                0, {now}, {execute_after}, {now}
            )"#,
            tbl = SagaTable::NAME,
            id = saga.id,
            name = escape(&saga.name),
            payload = payload,
            step_name = step_name,
            status = SagaStatus::NotReady.as_i32(),
            now = now,
            execute_after = execute_after,
        );
        self.saga_tbl.execute(&query)?;
        Ok(())
    }

    fn create_step_values(&self, step: &NewSagaStep, saga_id: &str, now: i64) -> String {
        format!(
            r#"({upd}, '{saga_id}', {name}, {order}, {status})"#,
            upd = now,
            saga_id = saga_id,
            name = escape(&step.name),
            order = step.step_order,
            status = SagaStatus::InProgress.as_i32(),
        )
    }

    fn save_log(&self, log: &SagaLog, err: &str) -> SagaResult<()> {
        let query = format!(
            r#"
                INSERT INTO {tbl}
                (saga_id, step_name, timestamp, status, error)
                VALUES
                ('{saga_id}', {step_nm}, {ts}, {status}, {err})
            "#,
            tbl = SagaLogTable::NAME,
            saga_id = log.saga_id,
            step_nm = escape(&log.step_name),
            ts = log.timestamp,
            status = log.status.as_i32(),
            err = err,
        );
        self.log_tbl.execute(&query)?;
        Ok(())
    }

    #[inline]
    fn des_uuid(src: String) -> rusqlite::Result<Uuid> {
        Uuid::from_str(&src)
            .map_err(|err| rusqlite::Error::FromSqlConversionFailure(1, Type::Text, Box::new(err)))
    }

    #[inline]
    fn des_status(src: i32) -> rusqlite::Result<SagaStatus> {
        SagaStatus::from_i32(src).map_err(|err| {
            rusqlite::Error::FromSqlConversionFailure(1, Type::Integer, Box::new(err))
        })
    }

    #[inline]
    fn des_saga(id_opt: Option<&SagaId>, row: &rusqlite::Row) -> rusqlite::Result<Saga> {
        let id = if let Some(val) = id_opt {
            *val
        } else {
            Self::des_uuid(row.get::<_, String>("id")?)?
        };
        Ok(Saga {
            id,
            name: row.get("name")?,
            payload: row.get::<_, Option<String>>("payload")?.map(deescape),
            step_name: row.get("step_name")?,
            status: Self::des_status(row.get("status")?)?,
            retried: row.get("retried")?,
            execute_after: row.get("execute_after")?,
            created_at: row.get("created_at")?,
            updated_at: row.get("updated_at")?,
        })
    }

    fn des_saga_step(saga_id: &Uuid, row: &rusqlite::Row) -> rusqlite::Result<SagaStep> {
        Ok(SagaStep {
            id: row.get(0)?,
            payload: row.get::<_, Option<String>>(1)?.map(deescape),
            updated_at: row.get(2)?,
            saga_id: *saga_id,
            name: deescape(row.get(3)?),
            step_order: row.get(4)?,
            status: Self::des_status(row.get(5)?)?,
        })
    }
}

impl ISagaRepo for DBSagaRepo {
    type Trx = Trx;

    fn transaction(&self) -> SagaResult<Self::Trx> {
        Ok(Trx)
    }

    fn create(&self, saga: Saga, steps: Vec<NewSagaStep>) -> SagaResult<()> {
        let step_name = escape(&steps[0].name);
        if let Some(ref payload) = saga.payload {
            self.create_saga(&saga, &escape(payload), &step_name)?
        } else {
            self.create_saga(&saga, &self.null, &step_name)?
        }
        let mut query = format!(
            r#"INSERT INTO {tbl} (
                `updated_at`, `saga_id`, `name`,
                `step_order`, `status`
            ) VALUES"#,
            tbl = SagaStepTable::NAME,
        );
        let saga_id = saga.id.to_string();
        let now = Utc::now().timestamp();
        query.push_str(&self.create_step_values(&steps[0], &saga_id, now));
        for step in steps.iter().skip(1) {
            query.push(',');
            query.push_str(&self.create_step_values(step, &saga_id, now));
        }
        self.step_tbl.execute(&query)?;
        let query = format!(
            "UPDATE {tbl} SET `status` = {status}",
            tbl = SagaTable::NAME,
            status = SagaStatus::InProgress.as_i32(),
        );
        self.saga_tbl.execute(&query)?;
        Ok(())
    }

    fn get_ready_ids(&self, limit: usize) -> SagaResult<Vec<SagaId>> {
        let now = Utc::now().timestamp();
        let query = format!(
            r#"
                SELECT {pk} from {tbl}
                WHERE `status` IN ({statuses})
                  AND `execute_after` <= {now}
                ORDER BY `created_at`
                LIMIT {limit}
            "#,
            pk = SagaTable::PK,
            tbl = SagaTable::NAME,
            statuses = self.ready_statuses,
            now = now,
            limit = limit
        );
        let result = self
            .saga_tbl
            .get_many(&query, |row| Self::des_uuid(row.get(0)?))?;
        Ok(result)
    }

    fn get_saga(&self, id: &SagaId) -> SagaResult<Saga> {
        let query = format!(
            "SELECT {rows} FROM {tbl} WHERE {pk} = '{id}'",
            rows = SagaTable::get_columns_as_str(&[SagaTable::PK]),
            tbl = SagaTable::NAME,
            pk = SagaTable::PK,
            id = id,
        );
        let saga = self
            .saga_tbl
            .get_one(&query, |row| Self::des_saga(Some(id), row))?;
        Ok(saga)
    }

    fn get_last_saga_of_name(&self, name: &str) -> SagaResult<Saga> {
        let query = format!(
            r#"
                SELECT {rows} FROM {tbl}
                WHERE `name` = {name}
                ORDER BY `created_at` DESC
                LIMIT 1
            "#,
            rows = SagaTable::get_columns_as_str(&[]),
            tbl = SagaTable::NAME,
            name = escape(name),
        );
        let saga = self
            .saga_tbl
            .get_one(&query, |row| Self::des_saga(None, row))?;
        Ok(saga)
    }

    fn update_saga(&self, saga: &Saga, columns: &[&str]) -> SagaResult<()> {
        let mut setters = format!("updated_at = {}", Utc::now().timestamp());
        for (key, fun) in self.saga_setters.iter() {
            if columns.contains(key) {
                let text = fun(saga);
                setters.push(',');
                setters.push_str(&format!("`{}` = {}", key, text));
            }
        }
        let query = format!(
            "UPDATE {tbl} SET {setters} WHERE {pk} = '{id}'",
            tbl = SagaTable::NAME,
            setters = setters,
            pk = SagaTable::PK,
            id = saga.id,
        );
        self.saga_tbl.execute(&query)?;
        Ok(())
    }

    fn get_count_of_status(&self, status: &[SagaStatus]) -> SagaResult<usize> {
        let status_list = status
            .iter()
            .map(|s| s.as_i32().to_string())
            .collect::<Vec<_>>()
            .join(",");
        let query = format!(
            "SELECT COUNT(*) FROM {tbl} WHERE `status` IN ({sl})",
            tbl = SagaTable::NAME,
            sl = status_list,
        );
        let res = self.saga_tbl.get_one(&query, |row| row.get(0))?;
        Ok(res)
    }

    fn get_saga_steps(&self, saga_id: &SagaId, keys: &[&str]) -> SagaResult<Vec<SagaStep>> {
        let mut q_keys = format!("({}", escape(keys[0]));
        for key in keys.iter().skip(1) {
            q_keys.push(',');
            q_keys.push_str(&escape(key));
        }
        q_keys.push(')');
        let query = format!(
            r#"
                SELECT {columns} FROM {tbl}
                WHERE `saga_id` = '{id}' AND `name` IN {keys}
                ORDER BY `step_order`
            "#,
            columns = SagaStepTable::get_columns_as_str(&["saga_id"]),
            tbl = SagaStepTable::NAME,
            id = saga_id,
            keys = q_keys,
        );
        let steps = self
            .step_tbl
            .get_many(&query, |row| Self::des_saga_step(saga_id, row))?;
        Ok(steps)
    }

    fn get_all_saga_steps(&self, saga_id: &SagaId) -> SagaResult<Vec<SagaStep>> {
        let query = format!(
            "SELECT {columns} FROM {tbl} WHERE `saga_id` = '{id}' ORDER BY `step_order`",
            columns = SagaStepTable::get_columns_as_str(&["saga_id"]),
            tbl = SagaStepTable::NAME,
            id = saga_id,
        );
        let steps = self
            .step_tbl
            .get_many(&query, |row| Self::des_saga_step(saga_id, row))?;
        Ok(steps)
    }

    fn update_step(&self, step: &SagaStep, columns: &[&str]) -> SagaResult<()> {
        let mut setters = format!("updated_at = {}", Utc::now().timestamp());
        for (key, fun) in self.step_setters.iter() {
            if columns.contains(key) {
                let text = fun(step);
                setters.push(',');
                setters.push_str(&format!("`{}` = {}", key, text));
            }
        }
        let query = format!(
            "UPDATE {tbl} SET {setters} WHERE {pk} = {id}",
            tbl = SagaStepTable::NAME,
            setters = setters,
            pk = SagaStepTable::PK,
            id = step.id,
        );
        self.step_tbl.execute(&query)?;
        Ok(())
    }

    fn log(&self, log: SagaLog) -> SagaResult<()> {
        if let Some(ref val) = log.error {
            self.save_log(&log, &escape(val))
        } else {
            self.save_log(&log, &self.null)
        }
    }

    fn get_last_errors(&self, min_ts: i64, limit: usize) -> SagaResult<Vec<SagaLog>> {
        let query = format!(
            r#"
                SELECT {fields} FROM {tbl}
                WHERE NOT (`error` IS NULL) AND `timestamp` >= {ts}
                ORDER BY `timestamp` DESC
                LIMIT {limit}
            "#,
            fields = SagaLogTable::get_columns_as_str(&[]),
            tbl = SagaLogTable::NAME,
            ts = min_ts,
            limit = limit,
        );
        let rows = self.log_tbl.get_many(&query, |row| {
            Ok(SagaLog {
                saga_id: Self::des_uuid(row.get("saga_id")?)?,
                step_name: row.get("step_name")?,
                timestamp: row.get("timestamp")?,
                status: Self::des_status(row.get("status")?)?,
                error: row.get("error")?,
            })
        })?;
        Ok(rows)
    }
}
