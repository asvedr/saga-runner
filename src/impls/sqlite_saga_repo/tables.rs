use easy_sqlite::traits::table::ITable;
use easy_sqlite::{escape, SStr, SVec};

use crate::entities::saga::{Saga, SagaStep};

pub struct SagaTable;

pub struct SagaStepTable;

pub struct SagaLogTable;

impl ITable for SagaTable {
    const PK: SStr = "id";
    const NAME: SStr = "saga";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "TEXT PRIMARY KEY"),
        ("name", "TEXT NOT NULL"),
        ("payload", "TEXT"),
        ("step_name", "TEXT"),
        ("status", "INTEGER NOT NULL"),
        ("retried", "INTEGER NOT NULL"),
        ("execute_after", "INTEGER"),
        ("created_at", "INTEGER NOT NULL"),
        ("updated_at", "INTEGER NOT NULL"),
    ];
    const INDEXES: SVec<SStr> = &["name", "step_name", "status", "execute_after"];
}

impl ITable for SagaStepTable {
    const PK: SStr = "id";
    const NAME: SStr = "saga_step";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER PRIMARY KEY"),
        ("payload", "TEXT"),
        ("updated_at", "INTEGER NOT NULL"),
        ("saga_id", "TEXT NOT NULL"),
        ("name", "TEXT NOT NULL"),
        ("step_order", "INTEGER"),
        ("status", "INTEGER"),
    ];
    const INDEXES: SVec<SStr> = &["saga_id", "name", "step_order", "status"];
    const UNIQUE: SVec<SStr> = &["saga_id,name"];
    const FOREIGN_KEYS: SVec<(SStr, SStr, SStr)> = &[("saga_id", SagaTable::NAME, SagaTable::PK)];
}

impl ITable for SagaLogTable {
    const PK: SStr = "id";
    const NAME: SStr = "saga_log";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER PRIMARY KEY"),
        ("saga_id", "INTEGER NOT NULL"),
        ("step_name", "TEXT NOT NULL"),
        ("timestamp", "INTEGER"),
        ("status", "INTEGER"),
        ("error", "TEXT"),
    ];
    const INDEXES: SVec<SStr> = &["saga_id"];
    const FOREIGN_KEYS: SVec<(SStr, SStr, SStr)> = &[("saga_id", SagaTable::NAME, SagaTable::PK)];
}

pub type Setter<T> = Box<dyn Fn(&T) -> String>;

impl SagaTable {
    pub fn setters() -> Vec<(&'static str, Setter<Saga>)> {
        vec![
            ("name", Box::new(|s| escape(&s.name))),
            (
                "payload",
                Box::new(|s| {
                    if let Some(ref val) = s.payload {
                        escape(val)
                    } else {
                        "NULL".to_string()
                    }
                }),
            ),
            ("step_name", Box::new(|s| escape(&s.step_name))),
            ("status", Box::new(|s| s.status.as_i32().to_string())),
            ("retried", Box::new(|s| s.retried.to_string())),
            (
                "execute_after",
                Box::new(|s| {
                    if let Some(ref val) = s.execute_after {
                        val.to_string()
                    } else {
                        "NULL".to_string()
                    }
                }),
            ),
            ("created_at", Box::new(|s| s.created_at.to_string())),
        ]
    }
}

impl SagaStepTable {
    pub fn setters() -> Vec<(&'static str, Setter<SagaStep>)> {
        vec![
            (
                "payload",
                Box::new(|s| {
                    if let Some(ref val) = s.payload {
                        escape(val)
                    } else {
                        "NULL".to_string()
                    }
                }),
            ),
            // ("updated_at", Box::new(|s| s.updated_at.to_string())),
            ("name", Box::new(|s| escape(&s.name))),
            ("step_order", Box::new(|s| s.step_order.to_string())),
            ("status", Box::new(|s| s.status.as_i32().to_string())),
        ]
    }
}
