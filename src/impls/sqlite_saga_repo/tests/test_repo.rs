use uuid::Uuid;

use crate::entities::errors::SagaError;
use crate::entities::saga::{NewSagaStep, Saga, SagaStatus};
use crate::impls::sqlite_saga_repo::repo::DBSagaRepo;
use crate::proto::saga::ISagaRepo;

fn make_repo() -> DBSagaRepo {
    DBSagaRepo::new(":memory:").unwrap()
}

fn make_saga_and_steps() -> (Saga, Vec<NewSagaStep>) {
    let saga = Saga {
        id: Uuid::new_v4(),
        name: "a".to_string(),
        step_name: "b".to_string(),
        ..Default::default()
    };
    let step_b = NewSagaStep {
        name: "b".to_string(),
        step_order: 0,
        status: Default::default(),
    };
    let step_c = NewSagaStep {
        name: "c".to_string(),
        step_order: 1,
        status: Default::default(),
    };
    (saga, vec![step_b, step_c])
}

#[test]
fn test_not_found() {
    let repo = make_repo();
    assert_eq!(repo.get_saga(&Uuid::new_v4()), Err(SagaError::NotFound))
}

#[test]
fn test_got_one() {
    let repo = make_repo();
    let (mut saga, steps) = make_saga_and_steps();
    repo.create(saga.clone(), steps).unwrap();
    saga.status = SagaStatus::InProgress;
    let got = repo.get_saga(&saga.id).unwrap();
    saga.execute_after = got.execute_after.clone();
    saga.created_at = got.created_at.clone();
    saga.updated_at = got.updated_at;
    assert_eq!(got, saga);
}

#[test]
fn test_update_saga() {
    let repo = make_repo();
    let (mut saga, steps) = make_saga_and_steps();
    repo.create(saga.clone(), steps).unwrap();
    saga.name = "xxx".to_string();
    saga.payload = Some("eee".to_string());
    repo.update_saga(&saga, &["name"]).unwrap();
    let got = repo.get_saga(&saga.id).unwrap();
    assert_eq!(got.name, "xxx");
    assert_eq!(got.payload, None);
}

#[test]
fn test_get_ready() {
    let repo = make_repo();

    assert_eq!(repo.get_ready_ids(10).unwrap(), vec![]);

    let (mut saga_1, steps) = make_saga_and_steps();
    let id_1 = saga_1.id.clone();
    repo.create(saga_1.clone(), steps).unwrap();
    let (saga_2, steps) = make_saga_and_steps();
    let id_2 = saga_2.id.clone();
    repo.create(saga_2, steps).unwrap();

    assert_eq!(
        repo.get_ready_ids(10).unwrap(),
        vec![id_1.clone(), id_2.clone()]
    );

    assert_eq!(repo.get_ready_ids(1).unwrap(), vec![id_1.clone()]);

    saga_1.status = SagaStatus::Broken;
    repo.update_saga(&saga_1, &["status"]).unwrap();

    assert_eq!(repo.get_ready_ids(10).unwrap(), vec![id_2]);
}

#[test]
fn test_get_saga_steps() {
    let repo = make_repo();
    let (saga, steps) = make_saga_and_steps();
    repo.create(saga.clone(), steps.clone()).unwrap();
    let steps = repo.get_saga_steps(&saga.id, &["b"]).unwrap();
    assert_eq!(steps.len(), 1);
    assert_eq!(steps[0].name, "b");
    let steps = repo.get_saga_steps(&saga.id, &["c", "b"]).unwrap();
    assert_eq!(steps.len(), 2);
    assert_eq!(steps[0].name, "b");
    assert_eq!(steps[1].name, "c");
}

#[test]
fn test_get_all_saga_steps() {
    let repo = make_repo();
    let (saga, steps) = make_saga_and_steps();
    repo.create(saga.clone(), steps.clone()).unwrap();
    let steps = repo.get_all_saga_steps(&saga.id).unwrap();
    assert_eq!(steps.len(), 2);
    assert_eq!(steps[0].name, "b");
    assert_eq!(steps[1].name, "c");
}

#[test]
fn test_update_saga_step() {
    let repo = make_repo();
    let (saga, steps) = make_saga_and_steps();
    repo.create(saga.clone(), steps.clone()).unwrap();
    let mut steps = repo.get_saga_steps(&saga.id, &["b"]).unwrap();
    assert_eq!(steps[0].name, "b");
    assert_eq!(steps[0].payload, None);
    steps[0].payload = Some("abc".to_string());
    repo.update_step(&steps[0], &["payload"]).unwrap();
    let steps = repo.get_saga_steps(&saga.id, &["b"]).unwrap();
    assert_eq!(steps[0].name, "b");
    assert_eq!(steps[0].payload, Some("abc".to_string()));
}

#[test]
fn test_get_count_of_status() {
    let repo = make_repo();
    let (saga, steps) = make_saga_and_steps();
    let count = repo
        .get_count_of_status(&[SagaStatus::InProgress, SagaStatus::RollingBack])
        .unwrap();
    assert_eq!(count, 0);
    repo.create(saga.clone(), steps.clone()).unwrap();
    let count = repo
        .get_count_of_status(&[SagaStatus::InProgress, SagaStatus::RollingBack])
        .unwrap();
    assert_eq!(count, 1)
}
