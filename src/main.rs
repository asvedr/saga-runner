mod app;
mod entities;
mod impls;
mod proto;
mod use_cases;
mod utils;

fn main() {
    use crate::app::entrypoints;
    use mddd::std::{LeafRunner, NodeRunner};

    NodeRunner::new("saga runner")
        .add("lua", LeafRunner::new(entrypoints::lua_shell))
        .add("run", LeafRunner::new(entrypoints::run))
        .add("config", LeafRunner::new(entrypoints::config))
        .add("version", LeafRunner::new(entrypoints::version))
        .run()
}
