use mlua::Error;
use std::str::FromStr;

use crate::entities::errors::LuaCustomError;

fn format_ext_error(txt: String) -> String {
    let err = match LuaCustomError::from_str(&txt) {
        Ok(val) => val,
        Err(_) => return txt,
    };
    match err {
        LuaCustomError::NeedToWait(_) => "std::NeedToWaitError",
        LuaCustomError::Retry(_, _) => "std::RetryError",
        LuaCustomError::Terminal(_) => "std::TerminalError",
    }
    .to_string()
}

fn format_lua_error_inner(error: &Error) -> (Result<&'static str, String>, String) {
    match error {
        Error::SyntaxError { message, .. } => (Ok("SyntaxError"), message.clone()),
        Error::RuntimeError(msg) => (Ok("RuntimeError"), msg.clone()),
        Error::MemoryError(msg) => (Ok("MemoryError"), msg.clone()),
        Error::SafetyError(msg) => (Ok("SafetyError"), msg.clone()),
        Error::ToLuaConversionError { message, .. } => (
            Ok("ToLuaConversionError"),
            message.clone().unwrap_or_default(),
        ),
        Error::FromLuaConversionError { message, .. } => (
            Ok("FromLuaConversionError"),
            message.clone().unwrap_or_default(),
        ),
        Error::MetaMethodRestricted(msg) => (Ok("MetaMethodRestricted"), msg.clone()),
        Error::MetaMethodTypeError { message, .. } => (
            Ok("MetaMethodTypeError"),
            message.clone().unwrap_or_default(),
        ),
        Error::CallbackError { cause, .. } => {
            let (_, tail) = format_lua_error_inner(cause);
            (Ok("CallbackError"), tail)
        }
        Error::ExternalError(msg) => (Ok("ExternalError"), format_ext_error(msg.to_string())),
        other => (Err(other.to_string()), String::new()),
    }
}

pub fn format_lua_error(error: &Error) -> String {
    let (head, tb) = format_lua_error_inner(error);
    match head {
        Ok(h) => format!("-- {} --:\n{}", h, tb),
        Err(h) => format!("-- {} --:\n{}", h, tb),
    }
}
