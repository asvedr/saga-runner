use std::mem::transmute;

#[inline(always)]
pub fn to_static<T: ?Sized>(obj: &T) -> &'static T {
    unsafe { transmute(obj) }
}
