use std::process::exit;

use mddd::macros::singleton_simple;

use crate::app::components::main_config;
use crate::impls::pgsql_saga_repo::repo::DBSagaRepo;
use crate::impls::saga_repo_pool::SagaRepoPool;

pub type DBSagaRepoPool = SagaRepoPool<DBSagaRepo>;

singleton_simple!(
    pub fn repo_pool() -> DBSagaRepoPool {
        let factory = || DBSagaRepo::new(&main_config().db);
        let count = Some(main_config().max_threads + 2);
        match SagaRepoPool::new(Box::new(factory), count) {
            Ok(val) => val,
            Err(err) => {
                eprintln!("Can not init saga repo pool: {:?}", err);
                exit(1)
            }
        }
    }
);
