use crate::app::components;
use crate::app::saga;
use crate::impls::lua_executor::LuaExecutor;
use crate::impls::lua_factory::LuaFactory;
use crate::impls::signal_handler::SignalHandler;
use crate::use_cases::config::ConfigUC;
use crate::use_cases::lua_shell::LuaShellUC;
use crate::use_cases::run::RunUC;
use crate::use_cases::version::VersionUC;

pub fn lua_shell() -> LuaShellUC<LuaExecutor, &'static LuaFactory> {
    LuaShellUC {
        lua_factory: components::lua_factory(),
    }
}

pub fn config() -> ConfigUC {
    ConfigUC
}

pub fn version() -> VersionUC {
    VersionUC
}

#[cfg(test)]
pub fn run() -> ConfigUC {
    ConfigUC
}

#[cfg(feature = "sqlite")]
#[cfg(not(test))]
pub fn run() -> RunUC<&'static saga::TSagaRunner, &'static SignalHandler, saga::TTcpIO> {
    let config = components::main_config();
    let io = config.tcp_port.map(|_| saga::tcp_io());
    RunUC {
        runner: saga::saga_runner(),
        signal_handler: components::signal_handler(),
        io,
    }
}

#[cfg(feature = "pgsql")]
#[cfg(not(test))]
pub fn run() -> RunUC<&'static saga::TSagaRunner, &'static SignalHandler, saga::TTcpIO> {
    let config = components::main_config();
    let io = config.tcp_port.map(|_| saga::tcp_io());
    RunUC {
        runner: saga::saga_runner(),
        signal_handler: components::signal_handler(),
        io,
    }
}
