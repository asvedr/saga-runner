use std::process::exit;

use mddd::macros::singleton_simple;
use mddd::traits::IConfig;

use crate::entities::config::MainConfig;
use crate::impls::fs_manager::FSManager;
use crate::impls::lua_executor::LuaExecutor;
use crate::impls::lua_factory::LuaFactory;
use crate::impls::lua_pool::LuaPool;
use crate::impls::plugin_loader::dylib::PluginLoader as BPL;
use crate::impls::plugin_loader::lua::PluginLoader as LPL;
use crate::impls::plugin_manager::PluginManager;
use crate::impls::saga_schema_manager::SagaSchemaManager;
use crate::impls::saga_schema_parser::SagaSchemaParser;
use crate::impls::signal_handler::SignalHandler;
use crate::impls::stdlib::all_mods;

singleton_simple!(
    pub fn main_config() -> MainConfig {
        match MainConfig::build() {
            Ok(val) => val,
            Err(err) => {
                eprintln!("Config parser failed: {}", err);
                exit(1)
            }
        }
    }
);

singleton_simple!(
    pub fn script_plugin_loader() -> LPL {
        LPL
    }
);

singleton_simple!(
    pub fn bin_plugin_loader() -> BPL {
        BPL::default()
    }
);

singleton_simple!(
    pub fn fs_manager() -> FSManager {
        FSManager
    }
);

singleton_simple!(
    fn saga_schema_parser() -> SagaSchemaParser {
        SagaSchemaParser
    }
);

#[allow(dead_code)]
pub type TSagaSchemaManager = SagaSchemaManager<&'static FSManager, &'static SagaSchemaParser>;

singleton_simple!(
    pub fn saga_schema_manager() -> TSagaSchemaManager {
        match SagaSchemaManager::new(fs_manager(), saga_schema_parser(), main_config()) {
            Ok(val) => val,
            Err(err) => {
                eprintln!("Schema manager failed: {:?}", err);
                exit(1)
            }
        }
    }
);

singleton_simple!(
    pub fn lua_factory() -> LuaFactory {
        use crate::proto::lua::IPluginManager;

        let manager_res = PluginManager::new(
            fs_manager(),
            script_plugin_loader(),
            bin_plugin_loader(),
            main_config(),
        );
        let manager = match manager_res {
            Ok(val) => val,
            Err(err) => {
                eprintln!("Plugins can not be processed: {:?}", err);
                exit(1)
            }
        };
        let plugins = manager.get_plugins();
        LuaFactory {
            stdlib: all_mods(main_config()),
            plugins,
        }
    }
);

singleton_simple!(
    pub fn signal_handler() -> SignalHandler {
        SignalHandler
    }
);

#[allow(dead_code)]
pub type TLuaPool = LuaPool<LuaExecutor>;

singleton_simple!(
    pub fn lua_pool() -> TLuaPool {
        LuaPool::new(lua_factory(), main_config().max_threads + 1)
    }
);
