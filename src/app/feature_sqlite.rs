use std::process::exit;

use mddd::macros::singleton_simple;

use crate::app::components::main_config;
use crate::impls::saga_repo_pool::SagaRepoPool;
use crate::impls::sqlite_saga_repo::repo::DBSagaRepo;

pub type DBSagaRepoPool = SagaRepoPool<DBSagaRepo>;

singleton_simple!(
    pub fn repo_pool() -> DBSagaRepoPool {
        let factory = || DBSagaRepo::new(&main_config().db);
        match SagaRepoPool::new(Box::new(factory), None) {
            Ok(val) => val,
            Err(err) => {
                eprintln!("Can not init saga repo pool: {:?}", err);
                exit(1)
            }
        }
    }
);
