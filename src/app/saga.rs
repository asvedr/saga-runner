use mddd::macros::singleton_simple;

use crate::app::components::{self, TLuaPool, TSagaSchemaManager};
#[cfg(feature = "pgsql")]
use crate::app::feature_pgsql::{repo_pool, DBSagaRepoPool};
#[cfg(feature = "sqlite")]
use crate::app::feature_sqlite::{repo_pool, DBSagaRepoPool};
use crate::impls::saga_executor_factory::SagaExecutorFactory;
use crate::impls::saga_runner::SagaRunner;
use crate::impls::tcp_io::TcpIO;

pub type TSagaExecutorFactory =
    SagaExecutorFactory<&'static DBSagaRepoPool, &'static TSagaSchemaManager, &'static TLuaPool>;

singleton_simple!(
    pub fn saga_executor_factory() -> TSagaExecutorFactory {
        SagaExecutorFactory {
            repo_pool: repo_pool(),
            lua_pool: components::lua_pool(),
            schema_manager: components::saga_schema_manager(),
        }
    }
);

pub type TSagaRunner = SagaRunner<&'static TSagaExecutorFactory, &'static DBSagaRepoPool>;

singleton_simple!(
    pub fn saga_runner() -> TSagaRunner {
        SagaRunner::new(
            saga_executor_factory(),
            repo_pool(),
            components::saga_schema_manager(),
            components::main_config(),
        )
    }
);

pub type TTcpIO = TcpIO<&'static DBSagaRepoPool, &'static TSagaRunner>;

pub fn tcp_io() -> TTcpIO {
    TcpIO::new(repo_pool(), saga_runner(), components::main_config())
}
