use json::JsonValue;
use mlua::prelude::LuaResult;
use mlua::{Lua, Table};

use crate::entities::errors::PluginResult;

pub trait IPlugin {
    fn clone_box(&self) -> Box<dyn IPlugin>;
    fn name(&self) -> &str;
    fn init(&self, lua: &Lua, lua_id: usize) -> PluginResult<()>;
}

pub trait IPluginLoader {
    fn load_plugin(&self, path: &str, name: &str) -> PluginResult<Box<dyn IPlugin>>;
}

pub trait ILuaExecutor {
    fn set_globals(&self, values: &[(&str, JsonValue)]) -> LuaResult<()>;

    fn exec(&self, name: &str, code: &str) -> LuaResult<()>;

    fn exec_n_get_js(&self, name: &str, code: &str, result: &str) -> LuaResult<JsonValue>;
}

pub trait IStdMod {
    fn path(&self) -> &[&str];
    fn init<'lua>(&self, id: usize, lua: &'lua Lua, table: &'lua Table) -> LuaResult<()>;
}

pub trait IPluginManager {
    fn get_plugins(self) -> Vec<Box<dyn IPlugin>>;
}

pub trait ILuaFactory {
    type Executor: ILuaExecutor;
    fn produce(&self, id: usize) -> Self::Executor;
}

pub trait ILuaPool {
    type Executor: ILuaExecutor;
    fn acquire_lua(&self) -> Option<Self::Executor>;
}

// --------------------------------
// IMPLS
// --------------------------------

impl<T: ILuaPool> ILuaPool for &'static T {
    type Executor = T::Executor;

    fn acquire_lua(&self) -> Option<Self::Executor> {
        (**self).acquire_lua()
    }
}

impl<T: IPluginLoader> IPluginLoader for &'static T {
    fn load_plugin(&self, path: &str, name: &str) -> PluginResult<Box<dyn IPlugin>> {
        (**self).load_plugin(path, name)
    }
}

impl<T: ILuaFactory> ILuaFactory for &'static T {
    type Executor = T::Executor;
    fn produce(&self, id: usize) -> Self::Executor {
        (**self).produce(id)
    }
}

impl<T: ILuaExecutor> ILuaExecutor for &T {
    fn set_globals(&self, values: &[(&str, JsonValue)]) -> LuaResult<()> {
        (**self).set_globals(values)
    }

    fn exec(&self, name: &str, code: &str) -> LuaResult<()> {
        (**self).exec(name, code)
    }

    fn exec_n_get_js(&self, name: &str, code: &str, result: &str) -> LuaResult<JsonValue> {
        (**self).exec_n_get_js(name, code, result)
    }
}
