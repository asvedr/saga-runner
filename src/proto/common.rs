use std::io;
use std::path::Path;
use std::sync::atomic::AtomicBool;
use std::sync::Arc;

pub trait IFSManager {
    fn read_text(&self, path: &Path) -> io::Result<String>;
    fn write_text(&self, path: &Path, text: &str) -> io::Result<()>;
    fn list_dir(&self, path: &Path) -> io::Result<Vec<String>>;
    fn read_bytes(&self, path: &Path) -> io::Result<Vec<u8>>;
    fn write_bytes(&self, path: &Path, bts: &[u8]) -> io::Result<()>;
}

pub trait ITermSignalHandler {
    fn bind_to_flag(&self, term_flag: Arc<AtomicBool>) -> io::Result<()>;
}

#[allow(clippy::upper_case_acronyms)]
pub trait IIO {
    fn run(&self) -> io::Result<()>;
    fn stop(&self) -> io::Result<()>;
}

// --------------------------------
// IMPLS
// --------------------------------

impl<T: IFSManager> IFSManager for &'static T {
    fn read_text(&self, path: &Path) -> io::Result<String> {
        (**self).read_text(path)
    }
    fn write_text(&self, path: &Path, text: &str) -> io::Result<()> {
        (**self).write_text(path, text)
    }
    fn list_dir(&self, path: &Path) -> io::Result<Vec<String>> {
        (**self).list_dir(path)
    }
    fn read_bytes(&self, path: &Path) -> io::Result<Vec<u8>> {
        (**self).read_bytes(path)
    }
    fn write_bytes(&self, path: &Path, bts: &[u8]) -> io::Result<()> {
        (**self).write_bytes(path, bts)
    }
}

impl<T: ITermSignalHandler> ITermSignalHandler for &'static T {
    fn bind_to_flag(&self, term_flag: Arc<AtomicBool>) -> io::Result<()> {
        (**self).bind_to_flag(term_flag)
    }
}
