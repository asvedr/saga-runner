use std::path::Path;
use std::sync::atomic::AtomicBool;
use std::sync::Arc;

use crate::entities::errors::{SagaResult, SagaSchemaResult};
use crate::entities::saga::{
    NewSagaStep, PeriodicSchema, Saga, SagaId, SagaLog, SagaSchema, SagaStatus, SagaStep,
};

pub trait ITransaction {
    fn commit(&self) -> SagaResult<()>;
    fn rollback(&self) -> SagaResult<()>;
    fn process_result_dbg<T: std::fmt::Debug>(&self, res: SagaResult<T>) -> SagaResult<T> {
        let loc_res = if res.is_ok() {
            self.commit()
        } else {
            self.rollback()
        };
        if loc_res.is_err() {
            eprintln!("Transaction failed on process {:?}", res)
        }
        loc_res?;
        res
    }
}

pub trait ISagaRepoPool {
    type Repo: ISagaRepo;
    fn acquire_repo(&self) -> Option<Self::Repo>;
}

pub trait ISagaRepo {
    type Trx: ITransaction;

    fn transaction(&self) -> SagaResult<Self::Trx>;

    fn create(&self, saga: Saga, steps: Vec<NewSagaStep>) -> SagaResult<()>;
    fn get_ready_ids(&self, limit: usize) -> SagaResult<Vec<SagaId>>;
    fn get_saga(&self, id: &SagaId) -> SagaResult<Saga>;
    fn get_last_saga_of_name(&self, name: &str) -> SagaResult<Saga>;
    fn update_saga(&self, saga: &Saga, columns: &[&str]) -> SagaResult<()>;
    fn get_count_of_status(&self, status: &[SagaStatus]) -> SagaResult<usize>;

    fn get_saga_steps(&self, saga_id: &SagaId, keys: &[&str]) -> SagaResult<Vec<SagaStep>>;
    fn get_all_saga_steps(&self, saga_id: &SagaId) -> SagaResult<Vec<SagaStep>>;
    fn update_step(&self, step: &SagaStep, columns: &[&str]) -> SagaResult<()>;

    fn log(&self, log: SagaLog) -> SagaResult<()>;
    fn get_last_errors(&self, min_ts: i64, limit: usize) -> SagaResult<Vec<SagaLog>>;
}

pub trait ISagaSchemaParser {
    fn parse_schema(&self, path: &Path, src: &str) -> SagaSchemaResult<SagaSchema>;
}

pub trait ISagaSchemaManager {
    fn get_schema(&self, saga_name: &str) -> Option<&SagaSchema>;
    fn available_schemas(&self) -> Vec<&str>;
    fn get_periodics(&self) -> Vec<(String, PeriodicSchema)>;
}

pub trait ISagaExecutor {
    fn create_saga(&self, name: String, payload: Option<String>) -> SagaResult<SagaId>;
    fn execute_saga(&self, saga_id: &SagaId) -> SagaResult<()>;
}

pub trait ISagaExecutorFactory {
    type Executor: ISagaExecutor;
    fn produce(&self) -> SagaResult<Self::Executor>;
}

pub trait ISagaRunner {
    fn create_saga(&self, name: String, payload: Option<String>) -> SagaResult<SagaId>;
    fn run(&self, term_flag: Arc<AtomicBool>) -> SagaResult<()>;
}

// --------------------------------
// IMPLS
// --------------------------------

impl<T: ISagaRepoPool> ISagaRepoPool for &'static T {
    type Repo = T::Repo;

    fn acquire_repo(&self) -> Option<Self::Repo> {
        (**self).acquire_repo()
    }
}

impl<T: ISagaRepo> ISagaRepo for &'static T {
    type Trx = T::Trx;

    fn transaction(&self) -> SagaResult<Self::Trx> {
        (**self).transaction()
    }

    fn create(&self, saga: Saga, steps: Vec<NewSagaStep>) -> SagaResult<()> {
        (**self).create(saga, steps)
    }

    fn get_ready_ids(&self, limit: usize) -> SagaResult<Vec<SagaId>> {
        (**self).get_ready_ids(limit)
    }

    fn get_saga(&self, id: &SagaId) -> SagaResult<Saga> {
        (**self).get_saga(id)
    }

    fn get_last_saga_of_name(&self, name: &str) -> SagaResult<Saga> {
        (**self).get_last_saga_of_name(name)
    }

    fn update_saga(&self, saga: &Saga, columns: &[&str]) -> SagaResult<()> {
        (**self).update_saga(saga, columns)
    }

    fn get_count_of_status(&self, status: &[SagaStatus]) -> SagaResult<usize> {
        (**self).get_count_of_status(status)
    }

    fn get_saga_steps(&self, saga_id: &SagaId, keys: &[&str]) -> SagaResult<Vec<SagaStep>> {
        (**self).get_saga_steps(saga_id, keys)
    }

    fn get_all_saga_steps(&self, saga_id: &SagaId) -> SagaResult<Vec<SagaStep>> {
        (**self).get_all_saga_steps(saga_id)
    }

    fn update_step(&self, step: &SagaStep, columns: &[&str]) -> SagaResult<()> {
        (**self).update_step(step, columns)
    }

    fn log(&self, log: SagaLog) -> SagaResult<()> {
        (**self).log(log)
    }

    fn get_last_errors(&self, min_ts: i64, limit: usize) -> SagaResult<Vec<SagaLog>> {
        (**self).get_last_errors(min_ts, limit)
    }
}

impl<T: ISagaSchemaParser> ISagaSchemaParser for &'static T {
    fn parse_schema(&self, path: &Path, src: &str) -> SagaSchemaResult<SagaSchema> {
        (**self).parse_schema(path, src)
    }
}

impl<T: ISagaSchemaManager> ISagaSchemaManager for &'static T {
    fn get_schema(&self, saga_name: &str) -> Option<&SagaSchema> {
        (**self).get_schema(saga_name)
    }

    fn available_schemas(&self) -> Vec<&str> {
        (**self).available_schemas()
    }

    fn get_periodics(&self) -> Vec<(String, PeriodicSchema)> {
        (**self).get_periodics()
    }
}

impl<T: ISagaExecutor> ISagaExecutor for &'static T {
    fn create_saga(&self, name: String, payload: Option<String>) -> SagaResult<SagaId> {
        (**self).create_saga(name, payload)
    }
    fn execute_saga(&self, saga_id: &SagaId) -> SagaResult<()> {
        (**self).execute_saga(saga_id)
    }
}

impl<T: ISagaExecutorFactory> ISagaExecutorFactory for &'static T {
    type Executor = T::Executor;

    fn produce(&self) -> SagaResult<Self::Executor> {
        (**self).produce()
    }
}

impl<T: ISagaRunner> ISagaRunner for &'static T {
    fn create_saga(&self, name: String, payload: Option<String>) -> SagaResult<SagaId> {
        (**self).create_saga(name, payload)
    }
    fn run(&self, term_flag: Arc<AtomicBool>) -> SagaResult<()> {
        (**self).run(term_flag)
    }
}
