use mddd::traits::IUseCase;

pub struct VersionUC;

#[allow(unreachable_code)]
#[allow(clippy::needless_return)]
fn db() -> &'static str {
    #[cfg(feature = "sqlite")]
    return "sqlite";
    #[cfg(feature = "pgsql")]
    return "postgres";
    return "<none>";
}

impl IUseCase for VersionUC {
    type Request = ();
    type Error = ();

    fn short_description() -> String {
        "show version and available features".to_string()
    }

    fn execute(&mut self, _: ()) -> Result<(), ()> {
        println!("version: {}", env!("CARGO_PKG_VERSION"));
        println!("db: {}", db());
        Ok(())
    }
}
