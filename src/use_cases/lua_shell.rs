use std::fs;
use std::io;
use std::io::Read;

use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCase;

use crate::proto::lua::{ILuaExecutor, ILuaFactory};
use crate::utils::lua::format_lua_error;

pub struct LuaShellUC<LE: ILuaExecutor, LF: ILuaFactory<Executor = LE>> {
    pub lua_factory: LF,
}

#[derive(IUseCaseRequest, Debug)]
pub struct Request {
    #[description("Main script. If none open interactive shell")]
    main: Option<String>,
}

fn run_interactive(lua: &dyn ILuaExecutor) -> Result<(), String> {
    let stdin = io::stdin();
    let mut buf = String::new();
    loop {
        buf.clear();
        stdin
            .read_line(&mut buf)
            .map_err(|err| format!("IO error: {:?}", err))?;
        let stripped = buf.trim();
        if stripped == "exit" || stripped == ":q" {
            break;
        }
        if let Err(e) = lua.exec("interactive", &buf) {
            println!("{}", format_lua_error(&e));
        }
    }
    Ok(())
}

fn run_script(lua: &dyn ILuaExecutor, path: String) -> Result<(), String> {
    let mut file = fs::File::open(path).map_err(|_| "Can not open main script".to_string())?;
    let mut text = String::new();
    file.read_to_string(&mut text)
        .map_err(|_| "Can not read main script".to_string())?;
    lua.exec("main", &text).map_err(|e| format_lua_error(&e))
}

impl<LE: ILuaExecutor, LF: ILuaFactory<Executor = LE>> IUseCase for LuaShellUC<LE, LF> {
    type Request = Request;
    type Error = String;

    fn short_description() -> String {
        "Run lua shell".to_string()
    }

    fn description(&self) -> String {
        "Lua shell. exit commands: \"exit\", \":q\"".to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Self::Error> {
        let lua = self.lua_factory.produce(1);
        match request.main {
            None => run_interactive(&lua),
            Some(path) => run_script(&lua, path),
        }
    }
}
