use mddd::traits::{IConfig, IUseCase};

use crate::entities::config::MainConfig;

pub struct ConfigUC;

impl IUseCase for ConfigUC {
    type Request = ();
    type Error = ();

    fn short_description() -> String {
        "show config params".to_string()
    }

    fn execute(&mut self, _: ()) -> Result<(), ()> {
        println!("Config schema:\n{}", MainConfig::help());
        match MainConfig::build() {
            Ok(config) => println!("Config values:\n{}", config),
            Err(err) => println!("Build error: {}", err),
        }
        Ok(())
    }
}
