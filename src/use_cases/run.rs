use mddd::traits::IUseCase;
use std::process::exit;
use std::sync::atomic::AtomicBool;
use std::sync::Arc;

use crate::entities::errors::SagaError;
use crate::proto::common::{ITermSignalHandler, IIO};
use crate::proto::saga::ISagaRunner;

pub struct RunUC<SR: ISagaRunner, TSH: ITermSignalHandler, IO: IIO + 'static> {
    pub runner: SR,
    pub signal_handler: TSH,
    pub io: Option<IO>,
}

impl<SR: ISagaRunner, TSH: ITermSignalHandler, IO: IIO + 'static> IUseCase for RunUC<SR, TSH, IO> {
    type Request = ();
    type Error = SagaError;

    fn short_description() -> String {
        "process sagas in infinite loop".to_string()
    }

    fn execute(&mut self, _: Self::Request) -> Result<(), Self::Error> {
        let flag = Arc::new(AtomicBool::new(false));
        if let Some(ref io) = self.io {
            if let Err(err) = io.run() {
                println!("io failed: {:?}", err);
                exit(1)
            };
        }
        if let Err(err) = self.signal_handler.bind_to_flag(flag.clone()) {
            println!("Can not bind handler to unix signals: {:?}", err);
            exit(1)
        };
        self.runner.run(flag)?;

        if let Some(ref io) = self.io {
            let _ = io.stop();
        }
        println!("Terminated by signal");
        Ok(())
    }
}
